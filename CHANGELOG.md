# Changelog

## v0.7.3
 - Compat with Julia 1.11. Lower compat bound was raised to Julia 1.9 (which it already was de facto)
 - *bugfix*: The `MemoryExport` module was fixed, and its backend switched to `OrderedDict`s
 - *new feature*: `BufferedSaveCallback` accumulates snapshots using `MemoryExport` and flushes to disk at lower rate


## v0.7.2
 - Improve flexibility when contructing BoxGrids
 - More performant `findbyid`
 - Some internal cleanup and house keeping
 - Doc string improvements

## v0.7.1
 - particles are now always saved with their concrete type, making reconstruction safer if the simulation particle type is non-concrete. Old files are still readable.
 - pairwise interaction computation is now skipped for empty boxes. This improves performance for sparse systems without affecting results
 - *new feature*: new API function `legacytypeparameters(::Type)` which maps non-concrete (e.g., `UnionAll`) types to concrete types they should be assumed to correspond to. Currently used for particle types to be able to load old files. Can be extended in the future for newly introduced type parameters.

## v0.7.0
 - *bugfix*, *breaking*: Multi-threading with an odd number of boxes along the first dimension in periodic systems is now safe. In previous versions of InPartS, computing interactions across the system boundaries may have caused race conditions when writing to the particle structs. The fix *changes the order of force computations* for all systems with an odd number of boxes in the first dimension.
 - *bugfix*: BoxGrid now behaves correctly on small periodic domains. In previous versions of InPartS, simulating with a BoxGrid of less than 3 boxes in a periodic dimension may have caused forces between boxes to be computed twice.
 - *bugfix*: automatic ParamType detection is more robust

## v0.6.0
 - *breaking*: Generated expressions for model import have changed and become more robust.
  It will no longer error for missing fields in the files. This breaks hypothetical workflows
  where one might have relied on messing with the expressions to implement compatibility.

## v0.5.2
 - *bugfix*: `hasobstacles` has been resurrected, because completely deprecating it was breaking other things
 - *deprecation*: the old name `hasobstacles` is deprecated in favor of the new `supportsobstacles`
 - *bugfix*: having been broken since its introduction `supportsobstacles(sim)` (former `hasobstacles`) now works correctly
 - *new feature*: `supportsobstacles` can now be called on particle containers as well as their types

## v0.5.1
 - *new feature*: `LineageCoordRegistrar` keeps track of the position and time of particle creation
 - *new feature*: `obstacles(sim)` and `obstacles(sim.particles)` now always returns a vector of `<:AbstractObstacle`, which is empty if there are no obstacles.
 - *new feature*: when `finalize`ing an empty/uninitialized output file, `SaveCallback` now throws a warning and initializes the file instead of throwing an error
 - *export format change*: instead of the old `registrarType`, the key for registrar types is now the standard `_type`. Old files are still readable
 - *bugfix*: the definition of the finalizer for `SignalHandlerCallback` has been fixed
 - *deprecation*: the old registrar names `SimpleSeqRegistrar` and `TrackingSeqRegistrar` have been deprecated in favour of `SimpleRegistrar` and `LineageRegistrar` respectively. Old names will be removed in a future release.
 - *deprecation*: the `hasobstacles` function has been deprecated, as `obstacles(sim)` is now always defined, and will be removed in a future release

## v0.5.0
 - *new feature*: All information available within an `evolve!` call is now managed in a `DynamicState` structure. This enables full state dumps for seamless continuation of simulation runs.
 - *new feature*: The boxgrid pair finding logic has been wrapped into a `pairwise_interactions` higher-order function that applies a given function to all possible pairs of particles. This is now used by default to compute external interactions, but can also be used in data analysis (e.g. to compute pair correlation functions of nearest neighbours)
 - *new feature*: The callback API has been extended by two new optional functions
    - `freeze!(::AbstractCallback, ::DynamicState)` is called on a callback instead of `finalize` when the simulation is prematurely interrupted
    - `defrost!(::AbstractCallback)` can be used when restoring callbacks from a dumped `DynamicState`
 - *new feature*: Three new callbacks are included with this version of InPartS
    - `SignalHandlerCallback` captures POSIX signals for graceful termination of `evolve!` calls. This callback lives in an extension and depends on [InterProcessCommunication.jl](https://github.com/emmt/InterProcessCommunication.jl) being loaded
    - `BinaryDumpCallback(trigger, filename)`: write a `Serialization.serialize` dump of the `DynamicState` when triggered and upon `finalize!` (and `freeze!`).
    - `ExponentialBackoffCallback`: A generic callback that triggers at first execution, then after `max(max_interval, start_interval*factor^n)` with `n` the amount of previous triggers. Starts back at `n=0` after `defrost!`
 - *new feature*: the behaviour of the `SaveCallback` on encountering an already existing snapshot can now be specified
 - *new feature*: InPartS export functions can now operate on in-memory dictionaries as "data files".

 - *breaking*: The callback API has been overhauled, introducing several *breaking changes*:
    - the meaning of callback return values has been changed: the only return values that do *not* terminate the simulation are `nothing` and a zero-value integer. All non-zero integers terminate the simulation, and will be returned by `evolve!`. No return values other than Integers and `nothing` are permitted.
    - the `(pre|post)propagate` and `finalize` hooks have been renamed `(pre|post)propagate!` and `finalize!` respectively and now operate on `DynamicState`s instead of `Simulation`s
        - this does not apply to the user-provided functions in generic callbacks (e.g. PeriodicCallback), which generally still operate on `Simulation`s
 - *breaking*: `evolve!` now returns an Integer return code (`0` on regular `tmax` termination or any non-zero callback return code) and a `DynamicState`
 - *breaking*: the execution order in single-threaded force computation is changed to the striping pattern using in multi-threading, thus simulation results should now be consistent independent of threading
 - *breaking*: The `offset` parameter in `RealTimeCallback`s is now given in seconds relative to the first `prepropagate!` call.\

 - *bugfix*: Access to `sim.rng` is now type-stable, which should speed up simulations that rely on frequent random number generation.
 - *bugfix*: `InPartS.isgroup` for `JLD2Files` now correctly looks for `JLD2.Group`s instead of (fictional) `JLD2Group`s

 - *deprecation*: `CallbackSet` is deprecated in favour of `CallbackList` which has a semantic ordering to the callback execution and uses early termination when a callback has a non-nothing return value.

## v0.4.4
 - *bugfix*: the IO extensions are now backwards compatible with Julia 1.8.5 and older

## v0.4.3
 - *bugfix*: fixed bug in HDF5 backend
 - *bugfix*: fixed constructors for BackupCallback

## v0.4.2
 - *deprecation*: `forcetuple` is deprecated in favour of the new improved `forcecontribution`
 - *bugfix*: automatic ParamType now can deal with UnionAll particle types as long as `(::ParticleType).params` is concrete
 - various IO improvements
    - HDF5 and JLD2 IO backends have been moved to extensions on Julia versions that support them
    - improvements to type reconstruction, speeding up simulation loading
    - `readsim` now loads the last full snapshot when no snapshot is explicitely specified
 - utility functions `current_time`, `current_step`, `particletype`, `hasobstacles` are now exported
 - added SleeperCallback for external simulation control

## v0.4.1
 - *bugfix*: PyPlot plotting has been repaired
 - *new feature*: new convenience syntax for reading data files with the `Snapshots` accessor
 - *new feature*: IO warnings can be globally disabled with `INPARTS_IOWARN` environment variable
 - added `particletype(sim)` definition

## v0.4.0
 - *breaking*: remove the obstacle definitions from InPartS
 - reexport StaticArrays

## v0.3.14
 - *bugfix*: fixed two bugs in the export system introduced in the previous version

## v0.3.13
 - *bugfix*: Fixed critical bugs for threaded simulations (see #31) that could cause race conditions in force computation and particle addition
 - *deprecation*: the kwargs for SimpleAdaptiveStepper are now called `dx`, `dtmin` and `dtmax`
 - `SimpleAdaptiveSteppers` now allow specifying a custom `dtmin` (see #30)
 - if `ParamType(::ParticleType)` isn't explicitely defined, InPartS tries to infer it from the fieldtype of `(::ParticleType).params`
 - reworked snapshot loading and writing
    - new function `InPartS.setstate!`
    - added support for POLOContainers

## v0.3.12
 - bugfix: particletype for POLOContainers fixed
 - bugfix: ParticleContainers can now safely return distinct objects on succesive calls of alldynamicobjects (as originally intended)
 - bugfix: a default full serialization condition interval for the global RNG has been added (the condition is `false`)
 - added convenience functions for data file handling

## v0.3.11
 - bugfix: boxing for 3D inverted DiskObstacles has been fixed
 - fallback constructors for obstacles with `inverted` field now work without having to specify type parameters
 - performance enhancement: `isoutside` no longer allocates

## v0.3.10
 - Xoshiro RNGs are now serialized in every snapshot
 - export system improvements for custom/external types. In particular, how a type is reconstructed (other than through the keyword-only constructor) can now be controlled through the new `@importconstruct` macro or an additional argument to `@genexport`.
 - more accurate module context for type name stringification and type reconstruction
 - Makie plotting pipeline moved to new package
 - bugfix in type dissection for legacy data import

## v0.3.9
 - behaviour change: BoxGrids now throw an error if no suitable box can be found for a particle (e.g. when it is outside of the domain) instead of simply ignoring the particle. This should never happen for correctly written models.
 - behaviour change: `ParticleObstacleContainer`s and `POLOContainers` now expect the `SO` type parameter to be `<: AbstractObstacle{N}`
 - new export system convenience functions for dealing with custom types other than particles and parameters
 - bugfix: new particles added from the `step!` function are now folded back into the domain before the next force computation. This may affect simulation results in periodic domains.
 - bugfix: the function `isperiodic` now actually returns correct values
 - bugfix: `readdict` for the JLD2 export backend now produces nested dictionaries instead of a single, flat dictionary with slash-separated paths, consistent with the `HDF5` version
 - minor fixes removing noisy depreciation warnings and debug output

## v0.3.8
 - general cleanup for first public release

## v0.3.7
- dropped support for Julia versions < 1.7
- added optional argument to block execution of finalizers in `evolve!`
- added new TimedCallback
- added export support for Xoshiro RNG
- added exportrecursive macro for recursive use of generated serialization functions
- various improvements in the export system

## v0.3.6
- added a convenience function to save simulationas
- fixed link to changelog in README
- `BoxGrid` now gracefully handles system sizes smaller than the `interactionrange`
- improved improved Makie plotting support
- first steps towards an improved callback interface — callbacks can now have an additional `postpropagate` hook

## v0.3.5
- fixed import of parametric types
- added support for inverted obstacles
- improved Makie plotting support

## v0.3.4
- particles are now `reset!` before the main loop starts
- introduced `autogrid!` function (even more convenient than `addgrid!`!)
- various bugs fixed

## v0.3.3
- import for v0.2 files fixed again

## v0.3.2
- every (generic) export function now has a `warn` flag, which can be used to disable all warnings
- `oldestancestor(id, registrar)` no longer considers IDs with `iszero(id) == true` as valid ancestors.
    This is useful because all cells added by `addparticle!` without a specified parent get registered as
    descendants of `UInt64(0)`
- added `addparticles!` and `addgrid!` function to add multiple particles
- `SimpleParticleContainer` and `ParticleObstacleContainer` can now be used without `BoxGrid`s. Simply add `InPartS.NoBoxing` as a third (fourth) type parameter
- more bugs fixed


## v0.3.1
- various bugs fixed in the export system (thanks to Dorian!)
- reintroduced Particle-type Simulation constructor

## v0.3.0
### Breaking changes
3D systems are now supported! This comes with a bunch of API changes to make things less awkward, to wit
- many basic InPartS types (including abstract types for models) now have a type parameter `D`, indicating their dimensions.
- the World struct has been dissolved:
    - Time is now kept directly in the simulation and can be accessed using the `current_time` and `current_step` functions.
    - Domain geometry is now handled by Domain structs, subtypes of AbstractDomain. InPartS comes with two default domain structs, `Domain2D` and `Domain3D` which do pretty much what you expect them to do. The old `rem` and `fold_back` functions still work, but keep in mind that the field in the simulation struct has been renamed to `sim.domain`.
- Some obstacle definitions (`AxisAlignedWall`, `DiskObstacle`) have been extended to work in 3D. In the case of `DiskObstacle`s, the first type parameter now indicates the dimensions of the Obstacle. Note that for `AxisAlignedWall`s, type parameters have not changed since the memory layout is independent of system dimension.
- `BoxGrid` has been extended to 3D (working with 26-neighbourhoods). The first type parameter now indicates the dimensions of the box grid.
- `AbstractParticleContainer` now has a type parameter `N` which indicates the supported system dimension. `SimpleParticleContainer` and `ParticleObstacleContainer` have also gained a new first type parameter `N`, and now extend `AbstractParticleContainer{N}`.
- the function `Base.ndims` can now be used to determine the supported dimension for `AbstractParticle`s, `AbstractParticleContainer`s, `BoxGrid`s and `AbstractDomain`s without resorting to type parameters.

Aside from the 3D-related stuff, there are some other API changes
- `InPartS.ParticleType(::AbstractParams)` has been removed because
    1. it wasn't actually used anywhere
    2. it enforces a 1-to-1-relation between params and particles, which isn't necessary and/or practical
### Non-breaking changes & new features
 - 3D models are now supported (see https://gitlab.gwdg.de/eDLS/InPartS-models/colonygrowth-models for a working model)
 - callbacks are now called before a new `dt` is computed to make interacting with system dynamics safer and prevent explosions
 - `@exportadd` now actually works, so if you relied on it being broken, you shouldn't have
 - `@exportcollect` has been extended to AdditionalExport fields

## v0.2.4
 - added multithreading (!66)
 - changed behaviour of `serialize!(group, object)` to return `group` (1ad689e5)

## v0.2.3
 - minor bugfixes

## v0.2.2
 - Make use of custom group size in `JLD2` backend
 - Fix type-instability in `closestpoints`. MOAR PERFORMANCE
