export Simulation, current_time, current_step, particles, obstacles, particletype, supportsobstacles


mutable struct Simulation{
   PTC<:AbstractParticleContainer,
   PT <: AbstractParticle,
   PM <: AbstractParams,
   D <: AbstractDomain,
   R  <:AbstractRegistrar{UInt64},
   THREADED,
   RNG <: AbstractRNG # rand(::AbstractRNG, ::Type{Float64}) is NOT type stable
}
    particles::PTC
    params::Vector{PM}
    domain::D

    # time
    t::Float64
    step::UInt64

    registrar::R

    _addition_buffer::Vector{PT}
    _deletion_buffer::Set{UInt64}
    _buffer_lock::ReentrantLock

    rng::RNG

    function Simulation(pc::PTC, PT::Type, PM::Type, domain::W, t, step, reg::R, rng::RNG, threaded::Bool = false) where {PTC, W, R, RNG}
      @assert ndims(PTC) == ndims(W) "Dimensions of particle container ($(ndims(PTC))) and domain ($(ndims(W))) must match!"
      new{PTC, PT, PM, W, R, threaded, RNG}(pc, PM[], domain, t, step, reg, PT[], Set{UInt64}(), Threads.ReentrantLock(), rng)
   end

   function Simulation(sim::Simulation{PTC, PT, PM, W, R, THREADED, RNG}; threaded::Bool) where {PTC,PT,PM,W,R,THREADED, RNG}
      new{PTC, PT, PM, W, R, threaded, RNG}(
          sim.particles,
          sim.params,
          sim.domain,
          sim.t,
          sim.step,
          sim.registrar,
          sim._addition_buffer,
          sim._deletion_buffer,
          sim._buffer_lock,
          sim.rng
      )
   end

end


"""
   Simulation(PT::Type{<:AbstractParticle}; kwargs)
   Simulation(ptc<:AbstractParticleContainer{N}; domainsize::NTuple{N}; kwargs...) where {N}
Create a Simulation with an `N`-dimensional cuboid domain of size `domainsize`.

First argument is either a particle type `PT<:AbstractParticle{N}`, in which case the simulation is initialised with
a `SimpleParticleContainer{N, PT}`, or a particle container.

# Arguments

 * `boundaries`: Specifiy the boundary conditions of the domain. Defaults to all [`Boundary`](@ref)
 * `time`: Set the initial value of the simulation clock. Defaults to `0.0`
 * `step`: Set the initial value of the timestep counter. Defaults to `0x0000000000000000`
 * `registrar`: Set the registrar. Defaults to [`SimpleRegistrar()`](@ref).
 * `rng`: Set the random number generator. Defaults to [`Random.Xoshiro`](@ref) with a randomly chosen seed
 * `threaded`: Enable multi-threaded force computation. Defaults to `false`.

!!! warning

    Multi-threaded force computation may not be supported by all particle containers

"""
function Simulation(ptc::AbstractParticleContainer{N};
   domainsize::NTuple{N},
   particletype::Type{<:AbstractParticle{N}} = particletype(ptc), # these are undocumented on purpose
   paramtype = ParamType(particletype), # you shouldn't need to mess with them
   boundaries::NTuple{N} = Tuple(fill(Boundary, N)),
   time = 0.0,
   step = zero(UInt64),
   registrar = SimpleRegistrar(),
   rng = Random.Xoshiro(rand(UInt64)),
   threaded::Bool = false
) where {N}

   if N == 2
      domaintype = Domain2D
   elseif  N == 3
      domaintype = Domain3D
   else
      error("This constructor does not know any domain types with $N dimensions.")
   end

   domain = domaintype{boundaries...}(SVector{N, Float64}(domainsize))

   return Simulation(ptc, particletype, paramtype, domain, time, step, registrar, rng, threaded)
end


Simulation(T::Type{<:AbstractParticle{N}}; kwargs...) where N = Simulation(SimpleParticleContainer{N, T}(); kwargs...)

## optional multi-threading
"""
   isthreaded(sim::Simulation)
Returns true if the simulation is set up for multi- force computation
"""
isthreaded(::Simulation{PTC, PT, PM, W, R, THREADED}) where {PTC,PT,PM,W,R,THREADED} = THREADED

"""
   enable_threading(sim)
Create a copy of `sim` set up for multi-threaded force computation

!!! warning

Multi-threaded force computation may nor be supported by all particle containers
"""
enable_threading(sim) =   isthreaded(sim) ? sim : toggle_threading(sim)

"""
   disable_threading(sim)
Create a copy of `sim` set up for single-threaded force computation
"""
disable_threading(sim) = !isthreaded(sim) ? sim : toggle_threading(sim)

"""
   toggle_threading(sim)
Create a copy of `sim` with `isthreaded(toggle_threading(sim)) == !isthreaded(sim)``

!!! warning

Multi-threaded force computation may nor be supported by all particle containers
"""
toggle_threading(sim::Simulation{PTC, PT, PM, W, R, THREADED}) where {PTC,PT,PM,W,R,THREADED} =
    Simulation(sim; threaded=!THREADED)


## accessors
"""
   domainsize(sim)
Returns the domain size of sim.
"""
@inline domainsize(sim::Simulation) = domainsize(sim.domain)

"""
   current_time(sim)
Returns the current simulation time.
"""
@inline current_time(sim::Simulation) = sim.t

"""
   current_time(sim)
Returns the current simulation time step number.
"""
@inline current_step(sim::Simulation) = sim.step


"""
   particles(sim)
Returns an iterator of particles in the simulation
"""
@inline particles(sim::Simulation) = particles(sim.particles)

"""
   obstacles(sim)
Returns an iterator of obstacles in the simulation
"""
@inline obstacles(sim::Simulation) = obstacles(sim.particles)

"""
    particletype(sim)
Returns the particle type of the simulation
"""
particletype(sim::Simulation) = particletype(sim.particles)

"""
    supportsobstacles(sim)
Returns true if the simulation can contain obstacles
"""
supportsobstacles(sim::Simulation) = supportsobstacles(sim.particles)
supportsobstacles(::PC) where {PC<:AbstractParticleContainer} = supportsobstacles(PC)

"""
   reset_clock!(sim::Simulation)
Set simulation time to zero
"""
function reset_clock!(sim::Simulation)
   sim.t = 0.0
   sim.step = zero(UInt64)
   return
end

## Param vector management

"""
   addparams!(sim, param)
Add the parameter struct `param` to the simulation.

Returns `false` if `param` is already part of the simulation.
"""
function addparams!(sim::Simulation{PTC, PT, PM}, params::PM) where {PTC, PT, PM}
   if params ∉ sim.params
      push!(sim.params, params)
      return true
   end
   return false
end


Base.show(io::IO, sim::Simulation) = print(io, "Simulation with $(typeof(sim.particles)) in a $(typeof(sim.domain))")

# fancy multi-line output
function Base.show(io::IO, ::MIME"text/plain", sim::Simulation)
   print(io,
"""InPartS Simulation
   $(typeof(sim.particles))
   $(typeof(sim.domain)) of size $(join(domainsize(sim), "×"))
   Time: $(round(current_time(sim), digits=3)) (Step: $(current_step(sim)))
   """)
   show(io, MIME"text/plain"(), sim.particles)
end
