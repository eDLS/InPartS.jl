export Domain2D, Domain3D, Boundary, PeriodicBoundary, domainsize


"""
Boundary
Abstract type representing a boundary condition.

!!! note
All periodic boundary conditions should inherit from [`PeriodicBoundary`](@ref)
"""
abstract type Boundary end
# TODO: don't like that Boundary is both the abstract supertype and the "conrete" representation of nonperiodic boundaries

"""    PeriodicBoundary
PeriodicBoundary is a subtype of [`Boundary`], used to specify periodic boundary conditions.
"""
abstract type PeriodicBoundary <: Boundary end




# TODO: should cuboid domains get another abstract type?
abstract type AbstractDomain{N} end
Base.ndims(::Type{<:AbstractDomain{N}}) where N = N
Base.ndims(::AbstractDomain{N}) where N = N



"""
    Domain2D{BCX, BCY, BCZ}(size::SVector{2, Float64})
    Domain2D{BCX, BCY, BCZ}(w, h)
    Domain2D(args; [boundaries = (BCX, BCY, BCZ)])
Create a two-dimensional rectangular domain of dimensions `SA[w, h]`

`Domain2D`s are parametrized by two [`Boundary`](@ref) types that represent the boundary
conditions in the two dimensions.

See also: [`Domain3D`]
"""
struct Domain2D{BCX<:Boundary, BCY<:Boundary} <: AbstractDomain{2}
    size::SV{Float64}
end

Domain2D(args...; boundaries = (Boundary, Boundary), kwargs...) =
    Domain2D{boundaries...}(args...; kwargs...)

Domain2D{BCX, BCY}(width, height) where {BCX, BCY} =
    Domain2D{BCX, BCY}(SA[width, height])

@inline domainsize(w::Domain2D) = w.size


"""
    isperiodic(w::Domain2D)
Checks whether the Domain2D has periodic boundary conditions. Returns a tuple of
booleans that are true if the corresponding boundary parameter is a subtype of
[`PeriodicBoundary`](@ref).
"""
isperiodic(w::Domain2D{BX, BY}) where {BX, BY} = (BX <: PeriodicBoundary, BY <: PeriodicBoundary)


"""
    getboundaries(w::Domain2D)
Returns the boundary parameters of `w` as a tuple. If you use this in
performance-critical functions, you are probably doing something wrong.
"""
getboundaries(w::Domain2D{BX, BY}) where {BX, BY} = (BX, BY)



## 3D


"""
    Domain3D{BCX, BCY, BCZ}(size::SVector{3, Float64})
    Domain3D{BCX, BCY, BCZ}(w, h, d)
    Domain3D(args; [boundaries = (BCX, BCY, BCZ)])
Create a three-dimensional cuboid domain of dimensions `SA[w, h, d]`

`Domain3D`s are parametrized by three [`Boundary`](@ref) types that represent the boundary
conditions in the three dimensions.

See also: [`Domain2D`]
"""
struct Domain3D{BCX<:Boundary, BCY<:Boundary, BCZ<:Boundary} <: AbstractDomain{3}
    size::SVector{3, Float64}
end

Domain3D(args...; boundaries = (Boundary, Boundary, Boundary), kwargs...) =
    Domain3D{boundaries...}(args...; kwargs...)

Domain3D{BCX, BCY, BCZ}(width, height, depth) where {BCX, BCY, BCZ} =
    Domain3D{BCX, BCY, BCZ}(SA[width, height, depth])

@inline domainsize(w::Domain3D) = w.size


"""
    isperiodic(w::Domain3D)
Checks whether the Domain3D has periodic boundary conditions. Returns a tuple of
booleans that are true if the corresponding boundary parameter is a subtype of
[`PeriodicBoundary`](@ref).
"""
isperiodic(::Domain3D{BX, BY, BZ}) where {BX, BY, BZ} = (BX <: PeriodicBoundary, BY <: PeriodicBoundary, BZ <: PeriodicBoundary)


"""
    getboundaries(w::Domain3D)
Returns the boundary parameters of `w` as a tuple. If you use this in
performance-critical functions, you are probably doing something wrong.
"""
getboundaries(::Domain3D{BX, BY, BZ}) where {BX, BY, BZ} = (BX, BY, BZ)



## Dealing with periodic BCs


"""
    fold_back!(particle::AbstractParticle, d::AbstractDomain)
Fold particles that have crossed a periodic boundary back into the domain by
applying [`fold_back`](@ref) to their `centerpos`.
"""
function fold_back!(particle::AbstractParticle, w::AbstractDomain)
    particle.centerpos = InPartS.fold_back(particle.centerpos, w)
end


# generate loads of utility functions that dispatch on the Boundary parameters

_boundaryfunctions = Expr(:block)

for D ∈ 2:3
    domaintypename = (D == 2) ? :Domain2D : :Domain3D

    for i ∈ 1:(2^D)
        # this is effectively a binary counter thing to get all combinations of boundaries
        periodic = [(i >> j) % 2 != 0 for j ∈ 0:(D-1)]

        domaintype = Expr(:curly, domaintypename, [Expr(:<:, (periodic[j]) ? :PeriodicBoundary : :Boundary) for j ∈ 1:D]...)

        modw_body = Expr(:ref, :SA, [(periodic[j]) ? :(diff[$j] - InPartS.micround(diff[$j], w.size[$j])) : :(diff[$j]) for j ∈ 1:D]...)
        fold_back_body = Expr(:ref, :SA, [(periodic[j]) ? :(mod(pos[$j], w.size[$j])) : :(pos[$j]) for j ∈ 1:D]...)
        isoutside_body = Expr(:call, :any, Expr(:ref, :SA, [(periodic[j]) ? :false : :(!(0 ≤ pos[$j] ≤ w.size[$j])) for j ∈ 1:D]...))

        clampbox_body = Expr(:ref, :SA, [periodic[j] ? :(clamp_periodic(point[$j], lo[$j], hi[$j], w.size[$j])) : :(clamp(point[$j], lo[$j], hi[$j])) for j ∈ 1:D]...)

        push!(_boundaryfunctions.args, quote
            @inline Base.rem(diff::AbstractVector, w::$domaintype) = $modw_body
            @inline fold_back(pos::AbstractVector{<:AbstractFloat}, w::$domaintype) = $fold_back_body
            @inline isoutside(pos::AbstractVector{<:AbstractFloat}, w::$domaintype) = $isoutside_body
            @inline clamptobbox(point::AbstractVector{<:AbstractFloat}, lo::AbstractVector{<:AbstractFloat}, hi::AbstractVector{<:AbstractFloat}, w::$domaintype) = $clampbox_body
        end)
    end
end

eval(_boundaryfunctions)

# manually document all generated methods

@doc """
    rem(diff::AbstractVector{<:AbstractFloat}, d::AbstractDomain)
Folds the difference vector `diff` into the domain using the minimum image
convention where necessary. This allows for the easy calculation of distances
in a domain `d` using the syntax
```julia
Δ = (a - b) % d
```
as `%` automatically maps to `rem`.
""" Base.rem(::AbstractVector{<:AbstractFloat}, ::AbstractDomain)

@doc """
    fold_back(pos::AbstactVector{<:AbstractFloat}, d::AbstractDomain)
Folds the position vector `pos` back into the domain.
""" fold_back(::AbstractVector{<:AbstractFloat}, ::AbstractDomain)

@doc """
    isoutside(pos::AbstractVector{<:AbstractFloat}, d::AbstractDomain)
Returns `true` if `pos` is outside the domain, i.e. if it lies beyond a non-periodic boundary.
""" isoutside(::AbstractVector{<:AbstractFloat}, ::AbstractDomain)

@doc """
    clamptobbox(pos::AbstractVector{<:AbstractFloat}, lo, hi, d::AbstractDomain)
Clamps `pos` into the axis-aligned box ("bbox") defined by the corners `hi` and `lo`.
""" clamptobbox(::AbstractVector{<:AbstractFloat}, ::AbstractVector{<:AbstractFloat}, ::AbstractVector{<:AbstractFloat}, ::AbstractDomain)


## more low-level functions

@inline function micround(Δ::T, s::T) where T
    sh = s*0.5
    # this looks like an Excel formula
    # but it is like 5 ns faster
    return ifelse(
        Δ > sh,
        s,
        ifelse(Δ < -sh, -s, zero(T))
    )
end


"""
    clamp_periodic(x, lo, hi, p)
Like [`Base.clamp`](@extref), but on a periodic space of period `p`.
"""
function clamp_periodic(point::T, lo::T, hi::T, period::T) where {T <: AbstractFloat}
    # deal with stupid convention used e.g. by atan(y,x)
    point < 0 && (point += period)

    # normalise interval (i.e. end > start)
    hi_shift = hi + (hi < lo ? period : 0.0)

    # precompute
    left_of_interval = point < lo

    # 2 cases
    if (!(left_of_interval) && point ≤ hi_shift) || (lo ≤ (point_shift1 = point + period) ≤ hi_shift)
        # point is inside interval
        # everything is fine
        return point
    else
        # point is not in interval
        # we need to find the closest boundary
        if left_of_interval
            return  (lo - point < point_shift1 - hi_shift) ? lo : hi
        else
            return (lo + period - hi < point - hi_shift) ? lo : hi
        end
    end
end
