"""
    obstacleforces!(so, c::AbstractParticle, sim::Simulation)

Computes the forces acting on the particle `c` due to the obstacle `so` in the
simulation `sim`. Models may implement new methods to define interactions between
specific obstacles and particles.
"""
function obstacleforces! end


@forcedef function obstacleforces!(so, particles::AbstractVector{<:AbstractParticle}, sim::Simulation)
    for c in particles
        @forcefun obstacleforces!(so, c, sim)
    end
end

"""
    interactionpolygon(so, w::Domain2D; growby=0.0, kwargs...)
Return a polygon covering the obstacle `so` and additionally `growby` in all
directions beyond the boundaries of the obstacle for computing the relevant area
for interactions with particles. The `domain` may be used for proper behavior of
infinite-size obstacles.
"""
interactionpolygon(so::AbstractObstacle{2}, w; kwargs...) = getpolygon(so; kwargs...)

tostring(so::AbstractObstacle) = string(so)

"""
    getpolygon(p::Union{AbstractParticle, AbstractObstacle}) → Matrix{Float64}
Returns a 2D array containing a polygon representation of the particle / obstacle.
Array rows correspond to indivdual polygon points.
"""
@api_undef getpolygon(p)
