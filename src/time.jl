export TimeSteppingStrategy, FixedStepper, SimpleAdaptiveStepper

"""
    TimeSteppingStrategy[T<:AbstractFloat}
Abstract supertypes for time stepping strategies.
"""
abstract type TimeSteppingStrategy{T<:AbstractFloat} end

"""
    nextdt(ts::TimeSteppingStrategy{T}, ps, w) → dt::T
Returns the next timestep length according to the given time [`TimeSteppingStrategy`](@ref)
"""
nextdt(::TimeSteppingStrategy, sim::Simulation)


"""
    FixedStepper(dt)
Creates a [`TimeSteppingStrategy`](@ref) where [`nextdt`](@ref) always returns `dt`
"""
struct FixedStepper{T<:AbstractFloat} <: TimeSteppingStrategy{T}
    dt::T
    FixedStepper(dt::T) where T = new{T}(dt)
    FixedStepper(;dt::T) where T = new{T}(dt)

end

nextdt(tss::FixedStepper{T}, ::Simulation) where {T} = tss.dt


"""
    SimpleAdaptiveStepper(dx; dtmax = 0.1, dtmin = 1e-10)
Creates a [`TimeSteppingStrategy`](@ref) that uses a simple adaptive algorithm to
compute the next time step
```
     dt = clamp(dx/vmax, dtmin, dtmax)
```
"""
struct SimpleAdaptiveStepper{T<:AbstractFloat} <: TimeSteppingStrategy{T}
    dtmin::T
    dtmax::T
    dx::T
    function SimpleAdaptiveStepper(dx::T1; dtmax::T2 = 0.1, dtmin::T3 = 1e-10, dt = NaN) where {T1, T2, T3}
        if !isnan(dt)
            # TODO: this needs to be removed in the next version
            Base.depwarn("`SimpleAdaptiveStepper(dx; dt)` is deprecated, use `SimpleAdaptiveStepper(dx; dtmax)` instead", :SimpleAdaptiveStepper)
            dtmax = dt
        end
        if dtmax ≤ dtmin
            @warn "dtmax ≤ dtmin"
        end
        T = promote_type(T1, T2, T3)
        new{T}(T(dtmin), T(dtmax), T(dx))
    end
end
SimpleAdaptiveStepper(; dx, kwargs...) = SimpleAdaptiveStepper(dx; kwargs...)


function nextdt(tss::SimpleAdaptiveStepper{T}, sim::Simulation{PTC, P}) where {T<:AbstractFloat, PTC, P}
    vmax = T(maxvelocity(P, sim))
    return  clamp(tss.dx/vmax, tss.dtmin, tss.dtmax)
end


"""
    StickyStepper(events::AbstractVector, tss::TimeSteppingStrategy)
Creates a [`TimeSteppingStrategy`](@ref) which ensures that the time points specified
in `events` are (more or less) exactly hit.

Uses the time step suggested by `tss`; optionally reducing it to hit the target.

!!! warning "Beware floating point errors!"

    At the moment, this stepper simply subtracts the target time from the current
    time to compute the time step. Since floating-point addition is not associative,
    this does **not** guarantee that the target is hit excactly. This might be fixed
    some time in the future.  However, if youre code depends on floating point numbers
    being bit-perfect, you are in deep trouble already and should just give up.
"""
mutable struct StickyStepper{T<:AbstractFloat, V<:AbstractVector{T}, TSS<:TimeSteppingStrategy{T}} <: TimeSteppingStrategy{T}
    events::V
    nevents::Int64
    nextevent::Int64
    stepper::TSS

    function StickyStepper(events::V, tss::TSS) where {T, V<:AbstractVector{T}, TSS<:TimeSteppingStrategy{T}}
        return new{T, V, TSS}(events, length(events), firstindex(events), tss)
    end

    StickyStepper(;events, nevents, nextevent, stepper) =
        new{eltype(events), typeof(events), typeof(stepper)}(events, nevents, nextevent, stepper)

end


function nextdt(s::StickyStepper,  sim::Simulation)
    dt = nextdt(s.stepper, sim)

    if s.nextevent < s.nevents
        nexttime = s.events[s.nextevent]
        if (current_time(sim) + dt) > nexttime
            # FIXME: floating point addition is NOT associative, i.e. w.t[] + dt ≠ nexttime
            dt = nexttime - current_time(sim)
            s.nextevent += 1
        end
    end
    return dt
end



## export

"""
    Dict(ts::TimeSteppingStrategy) → dict::Dict{String, Any}
Converts a [`TimeSteppingStrategy`](@ref) to a dictionary associating the field
names with their values.
The type of the `TimeSteppingStrategy` is saved using the key `"tssType"`.

Dictionaries created using this function can be converted back to a `TimeSteppingStrategy`
using [`TimeSteppingStrategy(::Dict{String, Any})`](@ref).
"""
function Base.Dict(tss::T) where {T <: TimeSteppingStrategy}
    d = Dict{String, Any}(string(fn) => getproperty(tss, fn) for fn ∈ fieldnames(T))
    d["tssType"] = string(T)
    return d
end

"""
    TimeSteppingStrategy(dict::Dict{String, Any}) → tss::TimeSteppingStrategy
Creates a new [`TimeSteppingStrategy`](@ref) from a dictionary. This function can
be used for all subtypes of `TimeSteppingStrategy` that provide a constructor
accepting all fields as keyword arguments.

The type of the `TimeSteppingStrategy` is parsed from the value of `dict["tssType"]`.
 Note that type parameters specified in this string are discarded. They should
therefore be inferrable from the other field values.

The type parsing uses [`reconstruct_subtype`](@ref) to find all the concrete
leaves of the tree below the type `TimeSteppingStrategy`.
"""
function TimeSteppingStrategy(dict::Dict{String, <:Any})
    # make copy of dictionary
    dcopy = Dict(Symbol(k) => v for (k,v) in dict)
    type = reconstruct_subtype(pop!(dcopy, :tssType), TimeSteppingStrategy)
    # get type
    return type(;dcopy...)
end
