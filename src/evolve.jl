#=
 This file contains the main time evolution function, which calls the functions
 specified in the AbstractParticle docs to evolve a system of particles.
 Optionally, the system is divided into boxes to reduce the amount of pair
 interactions that have to be considered.
=#

export evolve!

const SimulationSuccess = 0



## TIME EVOLUTION FUNCTION #####################################################
"""
    DynamicState{S<:Simulation, TSS<:TimeSteppingStrategy, CB<:AbstractCallback}

InPartS wraps all arguments passed of an `evolve!` call into a `DynamicState` struct.
This allows for full introspection needed in the callback system to enable e.g. the
[`BinaryDumpCallback`](@ref).
"""
@kwdef mutable struct DynamicState{S<:Simulation, TSS<:TimeSteppingStrategy, CB<:AbstractCallback}
    sim::S
    tss::TSS = FixedStepper(0.001)
    callback::CB = NoCallback()
    tmax::Float64
    dt::Float64 = NaN
    finalize::Bool = true
end


"""
    evolve!(sim, tmax; kwargs...)
Evolve a `Simulation` for `tmax` time units, using [`computeforces!`](@ref) and
[`propagate!`](@ref).

Returns a named tuple containing none of your business.

## Additional arguments

 - `tss::TimeSteppingStrategy` Set the time stepping strategy to use. Defaults to a [`FixedStepper`](@ref) with `dt = 1e-3`
 - `callback::AbstractCallback` Add a callback to the simulation.
 - `dontfinalize::Bool` If true, blocks execution of the callback finalizer. Use if you want to continue simulating on the same `Simulation`
 - `savedt::Bool` Include a list of timesteps `dt` in the return parameters. Not recommended except for debugging, defaults to `false`

"""
function evolve!(
    sim::Simulation, tmax::Real;
    tss = FixedStepper(0.001),
    callback::AbstractCallback = NoCallback(),
    dontfinalize::Bool = false,
)
    # adjust tmax to account for non-zero initial times
    tmax += current_time(sim)

    state = DynamicState(; sim, tss, callback, tmax, finalize=!dontfinalize, dt=NaN)
    evolve!(state)
end

function evolve!(state::DynamicState)
    rc = SimulationSuccess
    (; sim, tss, callback, tmax) = state

    flushadditions!(sim)
    isempty(sim.params) && error("Simulations without parameter structs cannot be evolved")
    initialize_pc!(sim)

    # reset particle states and apply boundary conditions via fold_back
    # to prepare for first computeforces! (usually done in propagate!)
    for obj in alldynamicobjects(sim)
        fold_back!(obj, sim.domain)
        reset!(obj, sim)
    end
    try
        while current_time(sim) < tmax
            computeforces!(sim)

            # pre-propagate hook
            ret = prepropagate!(callback, state)
            if should_terminate(ret)
                rc = ret
                break
            end

            state.dt = timestep = nextdt(tss, sim)
            propagate!(sim, timestep)

            # this should be the only time in InPartS that `sim.t` and `sim.step` are used without accessor functions
            # (except for import of course…)
            sim.t += timestep
            sim.step += 1

            # post-propagate hook 📬🪝
            ret = postpropagate!(callback, state)
            if should_terminate(ret)
                rc = ret
                break
            end

        end
    finally
        # post-simulation hook
        if rc == SimulationSuccess
            finalize!(callback, state)
        else
            freeze!(callback, state)
        end
    end
    return (rc, state)
end

## PROPAGATION HELPER FUNCTION #################################################

"""
    propagate!(sim, dt)
Advances the system of particles by one time step using the precomputed forces.
"""
function propagate!(sim::Simulation, dt)
    iter  = alldynamicobjects(sim)

    # TODO: make this more elegant
    #=if isthreaded(sim)
        Threads.@threads for n in eachindex(iter)
            particle = iter[n]
            step!(particle, dt, sim)
        end
    else=#
        for n in eachindex(iter)
            particle = iter[n]
            step!(particle, dt, sim)
        end
    #end

    flushdeletions!(sim)
    flushadditions!(sim)


    iter2 = alldynamicobjects(sim)
    if isthreaded(sim)
        Threads.@threads for n in eachindex(iter2)
            particle = iter2[n]
            fold_back!(particle, sim.domain)
            reset!(particle, sim)
        end
    else
        for n in eachindex(iter2)
            particle = iter2[n]
            fold_back!(particle, sim.domain)
            reset!(particle, sim)
        end
    end

    return nothing
end

## FORCE COMPUTATION ###########################################################

"""
    computeforces!(sim)
Computes the external and internal forces in the simulation.
"""
@forcedef computeforces!(sim::Simulation) = @forcefun computeforces!(sim, sim.particles)
