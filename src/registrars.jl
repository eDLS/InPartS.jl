export AbstractSeqRegistrar,
    SimpleRegistrar, LineageRegistrar,
    LineageCoordRegistrar

"""
    set_id!(futureparticle::AbstractParticle, r::AbstractRegistrar{T}, parent::T, sim::Simulation) → updated futureparticle
Updates the ID of `futureparticle` to the one obtained from `get_id!(r, parent, sim, futureparticle)`.
Generally mutates both `futureparticle` and `r`.
"""
function set_id!(futureparticle::AbstractParticle, r::AbstractRegistrar{T}, parent::T, sim::Simulation) where {T<:Integer}
    newid = get_id!(r, parent, sim, futureparticle)
    futureparticle.id = newid
    return futureparticle
end

"""
    _idtype(r::AbstractRegistrar{T}) → T
Returns the type of the IDs the registrar provides.
"""
@inline _idtype(::Type{<:AbstractRegistrar{T}}) where {T<:Integer} = T

"""
    get_id!(reg::AbstractRegistrar{T}, parent::T, sim::Simulation, futureparticle::AbstractParticle) where {T<:Integer} → id::T
Get the next ID of type `T` from the registrar `reg` and trigger side effects
(information recording like parent, etc.) depending on registrar type,
see subtypes of `AbstractRegistrar``.
"""
get_id!(reg::AbstractRegistrar{T}, parent::T, sim::Simulation, futureparticle::AbstractParticle) where {T<:Integer}


## ABSTRACT SEQUENTIAL REGISTRAR ###############################################

"""
    AbstractSeqRegistrar{T <: Integer}
Subtype for [`AbstractRegistrar`](@ref)s that use sequential integer IDs.
Comes with a helper function [`_nextid!`](@ref) that returns the next
available ID and increases the internal counter (called `next_id`) by one.
Other internal state changes should be taken care of by the `get_id!` API
function method for each subtype.

For implementations, see [`SimpleRegistrar`](@ref), [`LineageRegistrar`](@ref)
and [`LineageCoordRegistrar`](@ref).
"""
abstract type AbstractSeqRegistrar{T <: Integer} <: AbstractRegistrar{T} end


"""
    _nextid!(r::AbstractSeqRegistrar{T}) → id::T
Returns the next ID and increments the internal counter. Warns on counter overflow.
"""
@inline function _nextid!(r::AbstractSeqRegistrar{T}) where {T<:Integer}
    ret = r.next_id
    r.next_id += one(T)

    (r.next_id < ret) && @warn "Counter overflow! Previously assigned IDs will be reassigned!"

    return ret
end


## SIMPLE SEQUENTIAL REGISTRAR #################################################

"""
    SimpleRegistrar{T<:Integer}(; next_id = one(T))
    SimpleRegistrar() = SimpleRegistrar{UInt64}()
A simple registrar that returns sequential numbers of type `T`
(defaults to `UInt64`) on calling [`set_id!`](@ref)/[`get_id!`](@ref)
and does nothing else.
"""
mutable struct SimpleRegistrar{T<:Integer} <: AbstractSeqRegistrar{T}
    next_id::T
end

SimpleRegistrar{T}(; next_id = one(T)) where {T<:Integer} = SimpleRegistrar{T}(next_id)
# Skipping next_id inference (already provided by default constructor)
SimpleRegistrar() = SimpleRegistrar{UInt64}()

"""
    get_id!(reg::SimpleRegistrar{T}, parent::T, sim::Simulation, futureparticle::AbstractParticle) → id::T
Returns the next ID and alters the internal state of the
registrar accordincly.
"""
@inline get_id!(reg::SimpleRegistrar{T}, parent::T, sim::Simulation, futureparticle::AbstractParticle) where {T<:Integer} = _nextid!(reg)

## TRACKING SEQUENTIAL REGISTRAR ###############################################

"""
    LineageRegistrar{T<:Integer} <: AbstractSeqRegistrar{T}
In addition to returning sequential numbers of type `T` (defaults to `UInt64`),
this implementation of [`AbstractSeqRegistrar`](@ref) also can keep track of lineages
using a dictionary called `parent`.
Calling [`set_id!`](@ref)/[`get_id!`](@ref) on this registrar automatically
records the parent as the entry `(newid -> parent_id)` in the dictionary.
"""
mutable struct LineageRegistrar{T<:Integer} <: AbstractSeqRegistrar{T}
    next_id::T
    parent::Dict{T, T}
end

LineageRegistrar{T}(; next_id = one(T), parent = Dict{T,T}()) where {T<:Integer} = LineageRegistrar{T}(next_id, parent)
# infer T from next_id
LineageRegistrar(next_id::T; kwargs...) where {T<:Integer} = LineageRegistrar{T}(; next_id, kwargs...)
LineageRegistrar() = LineageRegistrar{UInt64}()

"""
    set_parent!(r::LineageRegistrar, id::T, parent::T)
Create an entry linking `id` to `parent` in the [`LineageRegistrar`](@ref)'s
parent list.
"""
@inline set_parent!(r::LineageRegistrar{T}, id::T, parent::T) where {T<:Integer} =
    r.parent[id] = parent


"""
    get_id!(r::LineageRegistrar{T}, parent::T) → id::T
Returns the next ID and automatically links it to the given parent ID.
"""
@inline function get_id!(r::LineageRegistrar{T}, parent::T, sim::Simulation, futureparticle::AbstractParticle) where {T<:Integer}
    ret = _nextid!(r)
    set_parent!(r, ret, parent)
    return ret
end

## TRACKING INITIALCOORD SEQUENTIAL REGISTRAR ###############################################

"""
    LineageCoordRegistrar{N, T<:Integer} <: AbstractSeqRegistrar{T}

Constructors:

    LineageCoordRegistrar{N, T = UInt64}()
    LineageCoordRegistrar(next_id::T = UInt64(1); ndims::Integer, kwargs...)
    
In addition to returning sequential numbers of type `T` (defaults to `UInt64`),
this implementation of [`AbstractSeqRegistrar`](@ref) also can keep track of lineages
using a dictionary called `parent` and the time and position at which the
particle was added. Particles are assumed to have a `centerpos` property of
type `SVector{N, Float64}`. `ndims` is another way of specifying `N`.
The last two constructors also allow prefilling `parent`, `centerpos` and `times`
properties via keyword args.

Calling [`set_id!`](@ref)/[`get_id!`](@ref) on this registrar automatically
records the above data.

Example:

    # Default-intialized registrar for a two-dimensional simulation
    reg = LineageCoordRegistrar(ndims=2)

    # Same but with type parameter
    reg = LineageCoordRegistrar{2}()
"""
mutable struct LineageCoordRegistrar{N, T<:Integer} <: AbstractSeqRegistrar{T}
    next_id::T
    parent::Dict{T, T}
    positions::Dict{T, SVector{N, Float64}}
    times::Dict{T, Float64}

    # Could do this for validation of the N parameter, but would have to provide two
    # constructors to mimick the default constructor that allows for automatic type inference.
    # Like this:
    # LineageCoordRegistrar(next_id::T, parent::Dict{T,T}, positions::Dict{T, SVector{N, Float64}}, times::Dict{T, Float64}) where {N, T<:Integer} = LineageCoordRegistrar{N,T}(next_id, parent, positions, times)
    # function LineageCoordRegistrar{N,T}(next_id::T, parent::Dict{T,T}, positions::Dict{T, SVector{N, Float64}}, times::Dict{T, Float64}) where {N, T<:Integer}
    #     N isa Int || error("Type parameter N should be an $(Int), received $(typeof(N))")
    #     new{N, T}(next_id, parent, positions, times)
    # end
end

LineageCoordRegistrar{N, T}(; next_id = one(T), parent = Dict{T,T}(), positions = Dict{T, SVector{N, Float64}}(), times = Dict{T, Float64}()) where {N, T<:Integer} = LineageCoordRegistrar{N, T}(next_id, parent, positions, times)
# infer T from next_id
LineageCoordRegistrar(next_id::T = UInt64(1); ndims::Integer, kwargs...) where {T<:Integer} = LineageCoordRegistrar{ndims, T}(; next_id, kwargs...)
LineageCoordRegistrar{N}() where N = LineageCoordRegistrar{N, UInt64}()

"""
    get_id!(r::LineageCoordRegistrar{N, T}, parent::T, sim::Simulation, futureparticle::AbstractParticle{N}) → id::T
Returns the next ID, automatically links it to the given `parent` ID in the parent
dictionary and saves the position of `futureparticle` and simulation time
at the time of ID generation in the `positions`
and `times` dictionaries, respectively.
"""
@inline function get_id!(r::LineageCoordRegistrar{N, T}, parent::T, sim::Simulation, futureparticle::AbstractParticle{N}) where {N, T<:Integer}
    id = _nextid!(r)
    r.parent[id] = parent
    r.positions[id] = futureparticle.centerpos
    r.times[id] = current_time(sim)
    return id
end

#############


function oldestancestor(id::T, reg::Union{LineageRegistrar{T}, LineageCoordRegistrar{N, T} where {N}}; maxdepth = Inf) where {T<:Integer}
    current = id
    depth = 0
    while depth < maxdepth
        nextid = get(reg.parent, current, missing)
        (ismissing(nextid) || iszero(nextid)) && break
        current = nextid
        depth += 1
    end
    return current
end



makesimpletype(x) = x
makesimpletype(x::Dict{<:String,<:Any}) = x
# This will work as long as the ID can be exactly represented
# in the valuetype of the dict.
function makesimpletype(x::Dict{T, T2}) where {T<:Integer, T2<:Number}
    m = zeros(T2, length(x), 2)
    for (n, p) in enumerate(x)
        # ensure that key can be represented in and converted back from type T2
        @assert T(T2(p[1])) == p[1]
        m[n, 1] = p[1]
        m[n, 2] = p[2]
    end
    m
end
function makesimpletype(x::Dict{T, SVector{N, T2}}) where {T<:Integer, N, T2<:Number}
    m = zeros(T2, length(x), 1+N)
    for (n, p) in enumerate(x)
        # ensure that key can be represented in and converted back from type T2
        @assert T(T2(p[1])) == p[1]
        m[n, 1] = p[1]
        for i = 1:N
            m[n, 1+i] = p[2][i]
        end
    end
    m
end


makeoriginaltype(x, idtype::Type) = x
makeoriginaltype(x::Array{T,1}, idtype::Type{T}) where T<:Integer = Dict{T,T}()
makeoriginaltype(x::Array{T,2}, idtype::Type{T}) where T<:Integer = Dict{idtype, idtype}(x[i,1] => x[i,2] for i ∈ 1:size(x)[1])
function makeoriginaltype(x::Array{T2,2}, idtype::Type) where {T2<:AbstractFloat}
    ndims = size(x)[2]-1
    if ndims == 1
        return Dict{idtype, T2}(idtype(x[i,1]) => x[i,2] for i ∈ 1:size(x)[1])
    else
        return Dict{idtype,SVector{ndims, T2}}(idtype(x[i,1]) => SVector{ndims, T2}(x[i,2:end]) for i ∈ 1:size(x)[1])
    end
end

"""
    Dict(reg<:AbstractRegistrar) → dict::Dict{String, Any}
Converts a registrar object to a dictionary associating the field names with
their values.
The type of the registrar is saved using the key `"_type"`.

Dictionaries created using this function can be converted back to a registrar
using [`AbstractRegistrar(::Dict{String, Any})`](@ref InPartS.AbstractRegistrar).
"""
function Base.Dict(reg::R) where {R <: InPartS.AbstractRegistrar}
    d = Dict{String, Any}(string(fn) => makesimpletype(getproperty(reg, fn)) for fn ∈ fieldnames(R))
    d["_type"] = string(R)
    return d
end


"""
    AbstractRegistrar(dict::Dict{String, Any}) → concrete subtype of AbstractRegistrar
Creates a new registrar from a dictionary. This function can be used for all
subtypes of `AbstractRegistrar` that provide a constructor accepting all fields as
keyword arguments.

The type of the registrar is parsed from the value of `dict["_type"]`.
"""
function AbstractRegistrar(dict::Dict{String, <:Any})
    # get type
    _type = "_type"
    if haskey(dict, _type) 
        typestr = dict[_type]
    else
        # legacy
        _type = "registrarType"
        typestr = dict[_type]
        # replace renamed type name for legacy compat.
        typestr = replace(typestr, 
            "SimpleSeqRegistrar" => "SimpleRegistrar",
            "TrackingSeqRegistrar" => "LineageRegistrar",
            )
    end        
    type = reconstruct_subtype(typestr, InPartS.AbstractRegistrar)
    idtype = _idtype(type)
    # make copy of dictionary
    dcopy = Dict(Symbol(k) => makeoriginaltype(v, idtype) for (k,v) in dict if k ≠ _type)

    return type(;dcopy...)
end
