module InPartS

## External Modules
using Reexport
using LinearAlgebra
using Pkg
using Random
using OrderedCollections
using MacroTools
using Requires
@reexport using StaticArrays
using FileIO: FileIO
using Printf

import Base:RefValue

import PkgVersion
const _INPARTS_VERSION = @PkgVersion.Version 0

# macro for undefineded API functions
macro api_undef(f::Expr)
    @assert f.head == :call
    fname = f.args[1]
    fargs = f.args[2:end]

    deleteat!(fargs, findall(x->(x isa Expr && x.head != :(::)), fargs))

    argtypes = Expr(:vect, map(x->(x isa Symbol) ? "Any" : :(string(typeof($(esc(x.args[1]))))), fargs)...)
    estring = "API function \"" * string(fname) * "\" has not been extended for "
    return quote
        $(esc(f)) = throw(ArgumentError($estring * join($argtypes, ", ", " and ")))
    end
end


## Useful definitions

const SV = SVector{2}

@inline function unitvector(φ::T) where {T}
    return SV{T}(reverse(sincos(φ)))
end

@inline cross2D(a, b) = a[1]*b[2] - a[2]*b[1]


"""
       TREE_HASH
Contains the tree hash of the loaded version of the module at precompile time.
Look for git commits with this tree hash by parsing `git log --pretty=raw`.
"""
const TREE_HASH = bytes2hex(Pkg.GitTools.tree_hash(joinpath(splitpath(@__FILE__)[1:end-2]...)))



## Basic types

export AbstractRegistrar, AbstractParticle, AbstractParams, ParamType, AbstractObstacle

"""
    AbstractRegistrar{T}
Abstract type for registrars. Subtypes `XxxxRegistrar{T}` should implement a function
```julia
get_id!(reg::XxxxRegistrar{T}, parent::T, sim::Simulation, futureparticle::AbstractParticle) → T
```
that returns (preferably unique) identifers of type `T` on every call.
Comes with a helper function [`_idtype`](@ref) that returns `T`.
"""
abstract type AbstractRegistrar{T} end
# this very abstract type exists in case non-integer/non-sequential IDs
# turn out to be useful

"""
    AbstractParticle{N}
Abstract supertype for N-dimensional particle models.

Subtypes of AbstractParticle should be mutable structs.
The core functions generally avoid accesing particle properties with one
prominent exception: `centerpos` should be a N-dimensional StaticVector of
floats and contain a canonical position coordinate for the particle, e.g. the
position of the center of mass.

"""
abstract type AbstractParticle{N} end
Base.ndims(::Type{<:AbstractParticle{N}}) where {N} = N
Base.ndims(::AbstractParticle{N}) where {N} = N

"""
    AbstractParams
Supertype for parameter structs.

Parameter structs are used to store static parameters, which can be
shared by multiple particles.
"""
abstract type AbstractParams end

"""
    AbstractObstacle{N}
Supertype for N-dimensional obstacles.
"""
abstract type AbstractObstacle{N} end
Base.ndims(::Type{<:AbstractObstacle{N}}) where {N} = N
Base.ndims(::AbstractObstacle{N}) where {N} = N

"""
    AbstractParticleContainer{N}
Supertype for N-dimensional particle containers
"""
abstract type AbstractParticleContainer{N} end
Base.ndims(::Type{<:AbstractParticleContainer{N}}) where {N} = N
Base.ndims(::AbstractParticleContainer{N}) where {N} = N

"""
    ParamType(PT::Type{<:AbstractParticle}) → PM::Type{<:AbstractParams}
Returns the parameter type for particles of type `PT`.
"""
function ParamType(T::Type{<:AbstractParticle})
    T isa UnionAll && return ParamType(Base.unwrap_unionall(T))
    T isa Union && return Union{ParamType.(Base.uniontypes(T))...}
    i = findfirst(==(:params), fieldnames(T))
    isnothing(i) && error("Type $T has no field params.")
    P = Base.fieldtypes(T)[i]
    P isa DataType || error("Failed to determine parameter type for type $T.")
    return P
end

# Abstract Callback Type
abstract type AbstractCallback end

# Modelling Error Exceptions
struct ModelError <: Exception
    msg::String
end


## Includes
include("extras/forceextraction.jl")

# basics
include("domains.jl")
include("simulation.jl")
include("registrars.jl")
include("particles.jl")

# evolution logic
include("boxgrid.jl")
include("particlecontainers/particlehandling_common.jl")
include("particlecontainers/simpleparticlecontainer.jl")
include("particlecontainers/particleobstaclecontainer.jl")
include("particlecontainers/polocontainer.jl")
include("time.jl")
include("evolve.jl")
include("callbacks.jl")

# IO
include("export/constants.jl")
include("export/generated.jl")
include("export/legacy.jl")
include("export/generic.jl")
include("export/callbacks.jl")
include("export/specialobjects.jl")
include("export/memoryexport.jl")


# extra stuff, convenience syntax, etc.
include("extras/particledef.jl")
include("extras/initialconditions.jl")
include("extras/polygonboxintersections.jl")

include("obstacles.jl")


PLOT_BACKENDS = Set{Symbol}()

export IsPyPlottable, IsPolyPlottable

# Plotting traits
abstract type PlotStyle end
struct IsPolyPlottable <: PlotStyle end
struct IsPyPlottable <: PlotStyle end

# fall back to pyplot
"""
    PlotStyle(::Type{<:AbstractParticle}) → ::PlotStyle
States which kind of plotting functions are provided for a custom model.

Possible return values are

* `InPartS.IsPyPlottable()`: The model provides a custom
  `plot(::ps::AbstractVector{<:AbstractParticle}) function using the
  pyplot API. This is the default option.
* `InPartS.IsPolyPlottable()`: The model provides a function
  [`getpolygon(::AbstractParticle)`](@ref) that returns a list
  of points defining a polygon representing the particle. Plotting the
  polygons is handled automatically
"""
PlotStyle(::Type{<:AbstractParticle}) = IsPyPlottable()

## Initialisation function
function __init__()
    # PLOT BACKENDS

    @require PyPlot="d330b81b-6aea-500a-939a-2ce795aea3ee" begin
        push!(PLOT_BACKENDS, :PyPlot)
        import .PyPlot.plot
        include("extras/plotting_common.jl")
        include("extras/plotting_pyplot.jl")

        @info "InPartS: Plotting enabled (using PyPlot.jl)"
        @warn """
            The PyPlot plotting backend is no longer under active development.
            Support will be dropped in a future version of InPartS.
            Plotting is now provided by the InPartSMakie package, available at
            https://gitlab.gwdg.de/eDLS/InPartSMakie
        """

    end


    # EXPORT BACKENDS
    # fallback if no extensions available
    @static if !isdefined(Base, :get_extension)
        @require HDF5 = "f67ccb44-e63f-5c2f-98bd-6dc0ccc4ba2f" begin
            include("../ext/HDF5Ext.jl")
            @info "InPartS: HDF5 export enabled"
        end

        @require JLD2 = "033835bb-8acc-5ee8-8aae-3f567f8a3819" begin
            include("../ext/JLD2Ext.jl")
            @info "InPartS: JLD2 export enabled"
        end
    end


    # Use Requires infrastructure to warn if legacy version is ever loaded simultaneously
    legacyInPartS = Base.PkgId(Base.UUID("385f2a1c-27df-41b8-9918-2c5d735168af"), "InPartS")
    Requires.listenpkg(legacyInPartS) do
        @warn "The legacy version of InPartS was loaded alongside the current, public version. This will cause unexpected behavior."
    end

    # set IOWARN from environment
    InPartS.IOWARN[] = get(ENV, "INPARTS_IOWARN", "1") != "0"

    return
end


const _NEVEREXPORTNAMES = [:include, :eval, :InPartSDev, :TREE_HASH, :_INPARTS_VERSION]

"""
    InPartSDev
Loading this module will bring all names from InPartS into your current scope, excluding those starting with a `#` or a `_`.
This will inevitably fill your namespace with tons of junk, but it will also make developing InPartS models less verbose.

Other names that will not be exported are $(join(map(x->"`"*string(x)*"`", InPartS._NEVEREXPORTNAMES), ", ", " and "))
"""
module InPartSDev

import InPartS
for n ∈ names(InPartS, all = true)
    if n ∈ InPartS._NEVEREXPORTNAMES || startswith(string(n), r"(#|_)")
        continue
    end
    @eval import InPartS: $n
    @eval export $n
end

end # InPartSDev

function equalparams(p1, p2, sim)
    if length(sim.params) == 1
        return true
    else
        return p1 == p2
    end
end



## finally, load deprecations
include("deprecations.jl")


end # module
