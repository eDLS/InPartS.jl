export SimpleParticleContainer

supportsobstacles(::Type{<:AbstractParticleContainer}) = false

###########################################################################################
#                Simple Particle Container : Use BoxGrid - no obstacles                   #
###########################################################################################

"""
    SimpleParticleContainer{PT<:AbstractParticle}() <: AbstractParticleContainer
Handles simulations with particles of one or more species with open and periodic
boundaries. For performant time evolution the domain is partitioned into
grid boxes with size `≥ interactionrange` where `InPartS.interactionrange` needs to
be extended for new particle types and it has to be ensured that `interactionrange`
is larger or equal to the maximum center-center distance between two interacting
particles.
"""
mutable struct SimpleParticleContainer{N, PT <: AbstractParticle{N}, BG<:AbstractBoxing} <: AbstractParticleContainer{N}
    particles::Vector{PT}
    boxgrid::BG
    SimpleParticleContainer{N, PT, BG}(pv::Vector{PT} = PT[]) where {N, PT, BG}= new{N, PT, BG}(pv)
end

# boxing enabled by default
SimpleParticleContainer{N, PT}(pv::Vector{PT} = PT[]) where {N, PT} = SimpleParticleContainer{N, PT, BoxGrid{N, Float64, PT}}(pv)
SimpleParticleContainer{N}(pv::Vector{PT}, boxing::Bool = true) where {N,PT} = SimpleParticleContainer{N, PT, boxing ? BoxGrid{N, Float64, PT} : NoBoxing}(pv)


function initialize_pc!(sim::Simulation{SimpleParticleContainer{N, PT, BG}}) where {N, PT <: AbstractParticle, BG}
    sim.particles.boxgrid = BG(sim)
    return nothing
end

Base.push!(pc::SimpleParticleContainer, x) = push!(pc.particles, x)
Base.append!(pc::SimpleParticleContainer, xs) = append!(pc.particles, xs)

particles(sim::Simulation{<:SimpleParticleContainer}) = sim.particles.particles
particles(pc::SimpleParticleContainer) = pc.particles
particletype(::SimpleParticleContainer{<:Any, PT}) where {PT} = PT


flushadditions!(pc::SimpleParticleContainer, ab::Vector) = flushadditions!(pc.particles, ab)
flushdeletions!(pc::SimpleParticleContainer, db) = flushdeletions!(pc.particles, db)
findbyid(pc::SimpleParticleContainer, id) = findbyid(pc.particles, id)
setparticles!(pc::SimpleParticleContainer, ps) = (empty!(pc.particles); append!(pc.particles, ps))
alldynamicobjects(sim::Simulation{<:SimpleParticleContainer}) = sim.particles.particles

## FORCE COMPUTATION ###########################################################

@forcedef function computeforces!(sim::Simulation, pc::SimpleParticleContainer)
    @forcefun pairwise_interactions(externalforces!, sim, pc.boxgrid)
    for p in particles(sim)
        internalforces!(p, sim)
    end
end

function Base.show(io::IO, ::MIME"text/plain", pc::SimpleParticleContainer{N, PT, BG}) where {N, PT, BG}
    print(io,
"""SimpleParticleContainer{$N, $PT, $BG}
    \t$(PT): $(length(pc.particles))"""
    )
end
