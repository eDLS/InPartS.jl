export ParticleObstacleContainer

###########################################################################################
#                    Particle Container : Use BoxGrid - With Obstacles                    #
###########################################################################################
"""
    ParticlesObstacleContainer{PT <: AbstractParticle, SO}() <: AbstractParticleContainer
Handler suitable for simulations that contain both particles and obstacles
such as walls. The logic for particles is identical to that of the
`SimpleParticleContainer`. Objects passed as obstacles are static. They cannot
move but may have arbitrary size. To allow for efficient time evolution
obstacles have to implement `InPartS.interactionpolygon` that
allows precomputation of the gridboxes relevant for interactions.
"""
mutable struct ParticleObstacleContainer{N, PT <: AbstractParticle{N}, SO<:AbstractObstacle{N}, BG<:AbstractBoxing} <: AbstractParticleContainer{N}
    pv::Vector{PT}

    sov::Vector{SO} #Static Object
    so_boxes::Vector{Vector{CartesianIndex{N}}}

    boxgrid::BG

    ParticleObstacleContainer{N, PT, SO, BG}(pv::Vector{PT} = PT[], sov::Vector{SO} = SO[]) where {N, PT,SO, BG} = new{N, PT, SO, BG}(pv, sov, Vector{Vector{Int}}())
end

ParticleObstacleContainer{N, PT, SO}(pv::Vector{PT} = PT[], sov::Vector{SO} = SO[]) where {N, PT,SO} = ParticleObstacleContainer{N, PT, SO, BoxGrid{N, Float64, PT}}(pv, sov)
ParticleObstacleContainer{N}(pv::Vector{PT}, sov::Vector{SO}, boxing::Bool = true) where {N, PT, SO} = ParticleObstacleContainer{N, PT, SO, boxing ? BoxGrid{N, Float64, PT} : NoBoxing}(pv, sov)


function initialize_pc!(sim::Simulation{ParticleObstacleContainer{N, PT, SO, BG}}) where {N, PT <: AbstractParticle, SO, BG}
    sim.particles.boxgrid = BG(sim)

    if BG <: BoxGrid
        empty!(sim.particles.so_boxes)
        # Find the relevant boxes for all obstacles
        for n in axes(sim.particles.sov, 1)
            so = sim.particles.sov[n]
            push!(sim.particles.so_boxes, relevant_boxes(so, sim.particles.boxgrid, sim.domain))
        end
    end

    return nothing
end


Base.push!(pc::ParticleObstacleContainer, x) = push!(pc.pv, x)
Base.append!(pc::ParticleObstacleContainer, xs) = append!(pc.pv, xs)

particles(sim::Simulation{<:ParticleObstacleContainer}) = sim.particles.pv
particles(pc::ParticleObstacleContainer) = pc.pv
particletype(::ParticleObstacleContainer{<:Any, PT}) where {PT} = PT

flushadditions!(pc::ParticleObstacleContainer, ab::Vector) = flushadditions!(pc.pv, ab)
flushdeletions!(pc::ParticleObstacleContainer, db) = flushdeletions!(pc.pv, db)
findbyid(pc::ParticleObstacleContainer, id) = findbyid(pc.pv, id)
supportsobstacles(::Type{<:ParticleObstacleContainer}) = true
addobstacles!(ptc::ParticleObstacleContainer, obs) = append!(ptc.sov, obs)
setparticles!(pc::ParticleObstacleContainer, ps) = (empty!(pc.pv); append!(pc.pv, ps))
alldynamicobjects(sim::Simulation{<:AbstractParticleContainer}) = particles(sim)

@forcedef function computeforces!(sim::Simulation, pc::ParticleObstacleContainer)
    bg = pc.boxgrid
    @forcefun pairwise_interactions(externalforces!, sim, bg)
    for p in particles(sim)
        internalforces!(p, sim)
    end
    if pc.boxgrid isa BoxGrid
        for n in axes(pc.sov, 1)
            so = pc.sov[n]
            for idx in pc.so_boxes[n]
                # particles are sorted into boxes, within pairwise_interactions
                @forcefun obstacleforces!(so, bg.boxes[idx], sim)
            end
        end
    else
        for n in axes(pc.sov, 1)
            so = pc.sov[n]
            @forcefun obstacleforces!(so, particles(sim), sim)
        end
    end
end

obstacles(pc::ParticleObstacleContainer) = pc.sov

function Base.show(io::IO, ::MIME"text/plain", pc::ParticleObstacleContainer{N, PT, SO, BG}) where {N, PT, SO, BG}
    print(io,
"""ParticleObstacleContainer{$N, $PT, $SO, $BG}
    \t$(PT): $(length(pc.pv))
"""
    )
    for obstacle in pc.sov
        println(io, "\t"*tostring(obstacle))
    end
end
