export  addparticle!, rmparticle!, addobstacles!
## Particle vector management


"""
addparticle!(sim::Simulation{_, PT}; params, _parent_id = UInt64(0), kwargs...) → id::UInt64
Create a new particle of type `PT` with fixed parameters `params` and add it to the simulation `sim`.
`_parent_id` is used to obtain a new ID from the registrar, all other keyword
arguments are passed on to the `kwarg`-only constructor of `PT`.

Returns the particle id.

# Extended help

New particles are added to a temporary buffer, which is automatically flushed by
[`evolve!`](@ref) on initialization and after every time step.
To manually flush the buffer, use [`flushadditions!(sim)`](@ref).

!!! warning

    Flushing the buffer manually while running a simulation is not recommended and
    may cause the simulation to enter an invalid state!
"""
function addparticle!(
    sim::Simulation{PTC, PT, PM}, T::Type{<:AbstractParticle} = PT, args...;
    params::PM, _parent_id::UInt64 = UInt64(0), kwargs...
    ) where {PTC, PT, PM}

    # assert that particle types are sane
    if !(T <: PT)
        throw(ArgumentError("Cannot add a $T to a simulation of $(PT)!"))
    end

    if T isa Union
        throw(ArgumentError("Please specify which kind of particle to add."))
    end

    lock(sim._buffer_lock) do
        # add parameters if necessary
        addparams!(sim, params)

        # register & add new cell
        newpart = T(args...; id = UInt64(0), params = params, kwargs...)
        InPartS.set_id!(newpart, sim.registrar, _parent_id, sim)
        push!(sim._addition_buffer, newpart)

        return newpart.id
    end
end


"""
    rmparticle!(sim, id) → id
Marks the particle with ID `id` for deletion.

## Extended help

Particle IDs marked for deletion are added to a temporary buffer. Deletions are
automatically committed by [`evolve!`](@ref) after every timestep.
To manually commit the deletions,  use [`flushdeletions!`](@ref).

!!! warning

    Committing the deletions manually is not recommended and may cause the
    simulation to enter an invalid state!
"""
function rmparticle!(sim::Simulation, id::UInt64)
    lock(sim._buffer_lock) do
        push!(sim._deletion_buffer, id)
    end
    return id
end


addobstacles!(sim::Simulation, obs::AbstractVector{<:AbstractObstacle}) = addobstacles!(sim.particles, obs)
addobstacles!(pc::AbstractParticleContainer, obs) = error("$(typeof(pc)) does not support obstacles")


"""
    flushadditions!(sim) → n
Flushes the temporary addition buffer (see [`addparticle!`](@ref)).

Returns the number `n` of particles added to the system.
"""
flushadditions!(sim::Simulation) = flushadditions!(sim.particles, sim._addition_buffer)

function flushadditions!(particles::AbstractVector{<:AbstractParticle}, addition_buffer)
    if (n = length(addition_buffer)) > 0
        append!(particles, addition_buffer)
        empty!(addition_buffer)
    end
    return n
end

"""
flushdeletions!(sim) → n
Flushes the temporary deletion buffer (see [`addparticle!`](@ref)).

Returns the number `n` of particles removed from the system.
"""
flushdeletions!(sim) = flushdeletions!(sim.particles, sim._deletion_buffer)

function flushdeletions!(particles::AbstractVector{<:AbstractParticle}, deletion_buffer)
    if (n = length(deletion_buffer)) > 0
        to_delete = findbyid(particles, deletion_buffer)
        deleteat!(particles, to_delete)
        empty!(deletion_buffer)
    end
    return n
end



"""
findbyid(sim, id::UInt64) → idx::Union{Int, Nothing}
findbyid(sim, id::AbstractVector{UInt64}) → idx::Vector{Int}
Returns the index of the particle with the given `id` in the simulation's particle vector.
"""
function findbyid(particles::AbstractVector{<:AbstractParticle}, id::UInt64)
    idx = searchsortedfirst(particles, (;id=id), by=x->x.id)
    if idx > length(particles) || particles[idx].id != id
        return nothing
    else
        return idx
    end
end

findbyid(particles::AbstractVector{<:AbstractParticle}, ids::AbstractSet{UInt64}) = sort!([findbyid(particles, id) for id ∈ ids])


# Fallback definition so even particle containers without obstacles (namely SimpleParticleContainer) will not error here
obstacles(::AbstractParticleContainer{N}) where {N} = AbstractObstacle{N}[]