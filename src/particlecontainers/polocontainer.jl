export POLOContainer
###########################################################################################
#          Particle Container : Use BoxGrid - With Obstacles and large objects            #
###########################################################################################
"""
    POLOContainer{N, PT <: AbstractParticle, SO, LO}() <: AbstractParticleContainer
A more flexible handler that can work with simulations containing (microscopic)
particles, obstacles and mesoscopic objects. This can be useful when some dynamic
objects are significantly larger than most particles and thus should not
limit the performance sensitive grid box sizes.

Currently no optimization for these large objects is implemented. The interaction
function is computed for every particle - large object pair.

!!! warn

    Despite the fact that it already has a dimensionality parameters, the POLOContainer currently
    only works for 2D simulations. You will find it quite impossible to actually construct a
    POLOContainer with N ≠ 2.
"""
mutable struct POLOContainer{N, PT <: AbstractParticle{2}, SO<:AbstractObstacle{N}, LO} <: AbstractParticleContainer{N}
    pv::Vector{PT}

    sov::Vector{SO} #Static Object (Obstacles)
    so_boxes::Vector{Vector{CartesianIndex{N}}}

    lov::Vector{LO} # Large Dynamic Objects
    lo_boxes
    boxgrid::BoxGrid{N, Float64, PT}

    POLOContainer{N, PT, SO, LO}() where {N, PT, SO, LO} = new{N, PT, SO, LO}(PT[], SO[], Vector{Vector{Int}}(), LO[], [])
end

POLOContainer(PT::Type{<:AbstractParticle{N}}, SO::Type, LO) where N = POLOContainer{N, PT, SO, LO}()

function initialize_pc!(sim::Simulation{POLOContainer{2, PT, SO, LO}}) where {PT <: AbstractParticle, SO, LO}
    sim.particles.boxgrid = BoxGrid{2,Float64, PT}(sim)

    empty!(sim.particles.so_boxes)
    # Find the relevant boxes for all obstacles
    for n in axes(sim.particles.sov, 1)
        so = sim.particles.sov[n]
        push!(sim.particles.so_boxes, relevant_boxes(so, sim.particles.boxgrid, sim.domain))
    end
    return nothing
end

Base.push!(pc::POLOContainer, x) = push!(pc.pv, x)
Base.append!(pc::POLOContainer, xs) = append!(pc.pv, xs)

particles(sim::Simulation{<:POLOContainer}) = sim.particles.pv
particles(pc::POLOContainer) = pc.pv
particletype(::POLOContainer{<:Any, PT}) where PT = PT
flushadditions!(pc::POLOContainer, ab::Vector) = flushadditions!(pc.pv, ab)
flushdeletions!(pc::POLOContainer, db) = flushdeletions!(pc.pv, db)
findbyid(pc::POLOContainer, id) = findbyid(pc.pv, id)
supportsobstacles(::Type{<:POLOContainer}) = true
addobstacles!(ptc::POLOContainer, obs) = append!(ptc.sov, obs)
setparticles!(pc::POLOContainer, ps) = (empty!(pc.pv); append!(pc.pv, ps))
alldynamicobjects(sim::Simulation{<:POLOContainer}) = vcat(sim.particles.pv, sim.particles.lov)


@forcedef function computeforces!(sim::Simulation, pc::POLOContainer)
    @forcefun pairwise_interactions(externalforces!, sim, pc.boxgrid)
    for p in particles(sim)
        internalforces!(p, sim)
    end

    bg = pc.boxgrid
    for n in axes(pc.sov, 1)
        so = pc.sov[n]
        for idx in pc.so_boxes[n]
            # particles are sorted into boxes, within pairwise_interactions
            @forcefun obstacleforces!(so, bg.boxes[idx], sim)
        end
    end

    for n in axes(pc.lov, 1)
        lo = pc.lov[n]
        for cell in particles(sim)
            @forcefun externalforces!(lo, cell, sim)
        end
    end
end

obstacles(pc::POLOContainer) = pc.sov


function Base.show(io::IO, ::MIME"text/plain", pc::POLOContainer{N, PT,SO, LO}) where {N, PT,SO, LO}
    print(io,
"""POLOContainer{$N,$PT, $SO, $LO}
    \t$(PT): $(length(pc.pv))
    \t$(LO): $(length(pc.lov))
"""
    )
    for obstacle in pc.sov
        println(io, "\t"*tostring(obstacle))
    end
end
