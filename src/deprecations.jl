@deprecate CallbackSet(callbacks::Vararg{AbstractCallback})     CallbackList(callbacks)

# there is a kwarg-only deprecation in the SimpleAdaptiveStepper constructor that cannot be moved here

@deprecate latest_full_snap(f) lastfullsnap(f)


# deprecate old registrar constructors
@deprecate SimpleSeqRegistrar(args...; kwargs...) SimpleRegistrar(args...; kwargs...)
@deprecate TrackingSeqRegistrar(args...; kwargs...) LineageRegistrar(args...; kwargs...)

# HACK: this is not nice
"""
    forcetuple(r::SVector{2, T}, f::SVector{2, T}, δ::SVector{2, T}) where {T}
    forcetuple(r::SVector{3, T}, f::SVector{3, T}, δ::SVector{3, T}) where {T}
Returns a named tuple containing the individual components of `r`, `f` and `δ`.

In the 2D case, the names of the tuple entries are `rx`, `ry`, `fx`, `fy` and `δx`, `δy`;
in 3D, the additional entries are called `rz`, `fz` and `δz` respectively

DEPRECATED! Use [`forcecontribution`](@ref) instead.
"""
function forcetuple(r::SVector{2, T}, f::SVector{2, T}, δ::SVector{2, T}) where {T}
    Base.depwarn("forcetuple was used for a semantically suboptimal force extraction. Use forcecontribution instead", :forcetuple)
    return (
        rx = r[1], ry = r[2],
        fx = f[1], fy = f[2],
        δx = δ[1], δy = δ[2]
    )
end

function forcetuple(r::SVector{3, T}, f::SVector{3, T}, δ::SVector{3, T}) where {T}
    Base.depwarn("forcetuple was used for a semantically suboptimal force extraction. Use forcecontribution instead", :forcetuple)
    return (
        rx = r[1], ry = r[2], rz = r[3],
        fx = f[1], fy = f[2], fz = f[3],
        δx = δ[1], δy = δ[2], δz = δ[3]
    )
end

@deprecate hasobstacles(args...; kwargs...) supportsobstacles(args...; kwargs...)
