
const PTTYPENAME = "pttype"
const PMTYPENAME = "pmtype"
const PTCTYPENAME = "ptctype"
const OBSTACLEGROUPNAME = "obstacles"
const PARAMGROUPNAME = "params"
const REGNAME = "registrar"
const RNGNAME = "rng"
const SNAPGROUPNAME = "snapshots"
const SNAPTIMENAME = "time"
const SNAPSTEPNAME = "step"
const SNAPRTSNAME = "writetime"
const SNAPNEXTIDNAME = "next_id"
const STRUCTTYPE = "_type"
const TSSGROUPNAME = "timestepping"
const DOMAINGROUPNAME = "domain"
const DOMAINSIZENAME = "size"
const DOMAINBCNAME = "boundaries"

const SNAPDATANAMES = Set([SNAPTIMENAME, SNAPSTEPNAME, SNAPNEXTIDNAME, RNGNAME, SNAPRTSNAME])



const VERSIONNAME = "InPartS"
const TREEHASHNAME = "treehash"

# Estimated number of snapshots in a file
# JLD2 will allocate a chunk of memory to fit links to approximately EST_NUM_SNAPSHOT
# snapshot groups. Speeds up reading files with many entries.
# Cost is a constant "large" amount of unused space (a few kb) for smaller files.
const EST_NUM_SNAPSHOTS = 2000
