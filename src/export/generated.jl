export ExportUtils
export @genexport


## Type tools

# basic syntax checker for type expressions to prevent 🥗 in error messages
"""
    check_type(expr)
Check whether `expr` is a syntactically valid type specification.

!!! warning "Do NOT assume this is secure!"

    This function is by no means perfect and may occasionally fail!
    Do **NOT NOT NOT** use it in security-critical contexts unless you understand every
    aspect of it internals.

# Extended help

Returns false unless one of the following conditions is true
 1. `expr` is a `Symbol`
 2. `expr` consists only of `:(.)` expressions and `Symbol`s
 3. `expr` is a `:curly` expression, with the first argument matching conditions 1 and 2
    and the remaining arguments are valid type parameters, defined as being either `Symbol`s,
    `isbits` literals or `:(<:)`  expressions, in which `check_type` is true for the last
    argument and all additional arguments are `Symbol`s
 4. `expr` is a `:where` expression with the first argument fulfilling condition 3 and all
    remaining arguments are valid type parameters as defined in condition 3

If a `QuoteNode` is encountered in any of the recursive steps, the condition is applied to
its contents.
"""
function check_type(t)
    if !_check_type(t)
        throw(ArgumentError(("Not a type: $t")))
    end
    return true
end

# reject all expressions except for a.b and a{b}
_check_type(q::QuoteNode) = _check_type(q.value)
function _check_type(t)
    if _check_simple_type(t)
        return true
    elseif t.head == :curly
        return _check_simple_type(t.args[1]) && all(map(_check_params, t.args[2:end]))
    elseif t.head == :where
        return (t.args[1].head == :curly && _check_type(t.args[1])) && all(map(_check_params, t.args[2:end]))
    elseif t.head == :escape
        return _check_type(only(t.args))
    end
    return false
end


_check_simple_type(::Symbol) = true
_check_simple_type(t::Expr) = t.head == :(.) && all(map(_check_simple_type, t.args))
_check_simple_type(q::QuoteNode) = _check_simple_type(q.value)


# _check_params returns true for Symbols, valid subtyping expressions and `isbits` literals
_check_params(::Symbol) = true
_check_params(q::QuoteNode) = _check_params(q.value)
function _check_params(p::Expr)
    if (p.head == :(<:))
        if length(p.args) > 1
            return p.args[1] isa Symbol && _check_type(p.args[2])
        else
            return _check_type(p.args[])
        end
    end
    if (p.head == :tuple)
        return all(map(_check_type, p.args))
    end
    return _check_type(p)
end

_check_params(p) = isbits(p)



"""
    splitstoredtype(t)
Splits an expression representing a type into type name, type parameters and placeholder symbols
for omitted parameters.

## Example
```julia
splitstoredtype(:(Foo{Bar, Baz} where Baz))
```
will return
```
:Foo, Any[:Bar, :Baz], Any[:Baz]
```
"""
function splitstoredtype(t::Expr)
    # catch module names and stuff
    if t.head == :.
        n,p,o = InPartS.splitstoredtype(t.args[2].value)
        return Expr(:., t.args[1], QuoteNode(n)), p, o
    end

    omitted = []
    if t.head == :where
        omitted = t.args[2:end]
        t = t.args[1]
    end
    @assert t.head == :curly
    return t.args[1], t.args[2:end], omitted
end

function splitstoredtype(t::Symbol)
    return t, [], []
end


"""
    freeparams(T::Type)
Returns the number of free type parameters.
"""
function freeparams(T::UnionAll)
    i = 0
    while T isa UnionAll
        T = T.body
        i += 1
    end
    i
end
freeparams(::Type) = 0

"""
    string_inmodule(thing; [mod = Main])
Produces the string representing `thing` in the context of
module `mod`.
The opposite of `reconstruct_type`, with which types can
hopefully be reconstructed in the same module.
"""
function string_inmodule(thing; mod = Main)
    io = IOContext(IOBuffer(), :module => mod)
    print(io, thing)
    return String(take!(io.io))
end

"""
    reconstruct_type(name::String; [mod = Main])
Reconstruct the type represented by `name`, by parsing it and evaluating the resulting
expression in `mod`

Uses [`check_type`](@ref) to prevent the most basic of code injection attacks. Yay security!
"""
function reconstruct_type(name::String; mod::Module = Main)
    expr = Meta.parse(name)
    check_type(expr)
    t = try
        mod.eval(expr)
    catch
        throw(ArgumentError("Could not reconstruct $name in module $(mod)"))
    end
    @assert t isa Type "Not a type: $name"
    t
end


"""
    reconstruct_subtype(name::String, super::Type; [mod = Main])
Same as [`reconstruct_type`](@ref), but complains loudly if the reconstructed type is not a
descendant of `super`.
"""
function reconstruct_subtype(name::String, super::Type; kwargs...)
    T = reconstruct_type(name; kwargs...)
    if !(T<:super)
        error("$name could be reconstructed, but is not a subtype of $super.")
    end
    return T
end


## Export dispatch system


"""
    ExportItem
Valtype for dispatching over export items
"""
abstract type ExportItem{S} end

"""
    FieldExport(S::Symbol)
[`ExportItem`](@ref) representing export of a struct field
"""
struct FieldExport{S} <: ExportItem{S}
    FieldExport(s::Symbol) = new{s}()
end

"""
    AdditionalExport(i::Int)
[`ExportItem`](@ref) representing additional exports (as defined by [`@exportadd`](@ref))
"""
struct AdditionalExport{S} <: ExportItem{S}
    AdditionalExport(i::Integer) = new{i}()
end

#%%

"""
    ExportStyle(T)
Trait which determines how the export system treats instances of `T`.
Defaults to [`VectorExport`](@ref) for subtypes of [`AbstractParticle`]
and [`ScalarExport`](@ref) for everything else.
"""
abstract type ExportStyle  end

"""
    ScalarExport()
[`ExportStyle`](@ref) for types that should be serialized individually, i.e. only one instance
should be written into a data file group.
"""
struct ScalarExport <: ExportStyle end

"""
    VectorExport()
[`ExportStyle`](@ref) for types that should be serialized as vectors, i.e. many instances
of the type should be written into the same group, with fields being collected into vectors.

Note that some backends may not like vectors of complex field types, so be sure to
use [`@exportcollect`](@ref) to pre-digest them with a custom collection function.
"""
struct VectorExport <: ExportStyle end

# Defaults
ExportStyle(::Type) = ScalarExport()
ExportStyle(::Type{<:AbstractParticle}) = VectorExport()


"""
    ExportUtils
This module serves as a namespace for all functions generated by the import/export system.
"""
module ExportUtils
    using InPartS
    import InPartS:SV, STRUCTTYPE, ExportItem, FieldExport, AdditionalExport

    """
        exportfield(::Type, ::ExportItem)
    If false, the given [`ExportItem`] is skipped during export. Used for
    [`AdditionalExport`](@ref) to determine the last additional export.
    """
    exportfield(::Type, ::FieldExport) = true
    exportfield(::Type, ::AdditionalExport) = false


    # default functions for things
    exporttransform(::Type, ::ExportItem) = :(value)
    importtransform(::Type, ::ExportItem) = :(data)

    exportcollect(::Type, ::ExportItem) = :(vector)
    importcollect(::Type, ::ExportItem) = :(blob)

    # defaults for params: use the fact the the simulation will be available in the (de)serializers
    exporttransform(::Type{<:AbstractParticle}, ::FieldExport{:params}) =
        quote
            i = findfirst(x->x==value, sim.params)
            @assert !isnothing(i) "Parameter $(value) not found in simulation. Potentially the result of manually added particles"
            i::Int
        end
    importtransform(::Type{<:AbstractParticle}, ::FieldExport{:params}) = :(sim.params[data])

    # defaults for centerpos: transform  into 2D array for export
    exportcollect(::Type{<:AbstractParticle}, ::FieldExport{:centerpos}) = :(InPartS.collectvectors(vector))
    importcollect(::Type{<:AbstractParticle}, ::FieldExport{:centerpos}) = :(InPartS.tosvectors(blob))
    importcollect(::Type{<:AbstractParticle{2}}, ::FieldExport{:centerpos}) = :(@views InPartS.SVector{2, Float64}.(blob[:, 1], blob[:, 2]))
    importcollect(::Type{<:AbstractParticle{3}}, ::FieldExport{:centerpos}) = :(@views InPartS.SVector{3, Float64}.(blob[:, 1], blob[:, 2], blob[:, 3]))

    # default names (trivial for FieldExport, irrelevant for AdditionalExport)
    exportname(::Type, ::FieldExport{S}) where S = string(S)
    exportname(::Type, ::AdditionalExport) = nothing
    importname(::Type, ::FieldExport{S}) where S = S # if you value your life, do NOT overwrite this method!!!!!
    importname(t::Type, a::AdditionalExport) = Symbol(exportname(t, a))

    # Singleton used as placeholder for fields missing in files while loading data
    struct MissingValue end

    filter_missingvals(fullnt) = NamedTuple((( k=>v for (k,v) ∈ pairs(fullnt) if !isa(v, ExportUtils.MissingValue))...,))

    # function called when instantiating struct upon deserialization (default: call kw-only constructor)
    importconstruct(t::Type; kwargs...) = t(; kwargs...)

    @generated function importconstruct_vector(items, nt::NamedTuple{N, T}) where {N, T}
        inames = Expr(:parameters)
        for iname ∈ N
            push!(inames.args, Expr(:kw, iname, :(nt.$iname[i])))
        end
        return quote
            @inbounds for i ∈ eachindex(items)
                items[i] = ExportUtils.importconstruct($inames, eltype(items))
            end
        end
    end

    """
        isserializable(T::Type) → Bool
        isserializable(item::T) → Bool
    Indicates whether serialize!/deserialize were defined for the type T or item(s) of type T. The type
    for which `true` is returned always corresponds to the type that can also be passed to `serialize!`.
    For `VectorExport`s, this means that the relevant serializable type is `Vector{T}`, not `T` itself.

    For more information on the export system, consult the InPartS manual.
    """
    isserializable(::Type) = false
    isserializable(::T) where {T} = isserializable(T)

    """
        isdeserializabledict(data) → Bool
    Checks whether `data` is a dictionary-like object, which can potentially be
    deserialized with [`deserializefromdict`](@ref).

    For more information on the export system, consult the InPartS manual.
    """
    isdeserializabledict(data) = applicable(haskey, data, STRUCTTYPE) && haskey(data, STRUCTTYPE)


    #modname(::Type) = "Main"
    # more defaults!
    """
        serialize!(group, data::T)
        serialize!(group, data::Vector{T<:AbstractParticle}; sim::Simulation)
    Serialize the `data` of serializable type T into `group`. For types with `ExportStyle(T) == VectorExport()`
    (currently only `AbstractParticle`), the second form applies.

    For more information on the export system, consult the InPartS manual.
    """
    serialize!(g, d::T; kwargs...) where {T} = serialize!(g, d, InPartS.ExportStyle(T); kwargs...)
    serialize!(::Any, @nospecialize(a), @nospecialize(s::InPartS.ExportStyle); kwargs...) = error("InPartS: no $s serializer defined for type $(typeof(a))")

    """
        serializetodict(data::T) → Dict{String, Any}
        serializetodict(data::Vector{T<:AbstractParticle}; sim::Simulation) → Dict{String, Any}
    Serialize the `data` of serializable type `T` into a Dict{String, Any}, including the name of the
    type `T` itself, which is stored at the key "$(STRUCTTYPE)". For types with `ExportStyle(T) == VectorExport()`
    (currently only `AbstractParticle`), the second form applies. The result can be deserialzed using
    [`deserializefromdict`](@ref).

    For more information on the export system, consult the InPartS manual.
    """
    serializetodict(d::T; mod::Module = Main, kwargs...) where {T} = serialize!(Dict{String, Any}(STRUCTTYPE => InPartS.string_inmodule(T; mod)), d; kwargs...)
    serializetodict(d::Vector{T}; mod::Module = Main, kwargs...) where {T} = serialize!(Dict{String, Any}(STRUCTTYPE => InPartS.string_inmodule(T; mod)), d; kwargs...)

    """
        deserialize(group, type::Type{T}) → result::T
        deserialize(group, type::Type{T<:AbstractParticle}; sim::Simulation) → particles::Vector{T}
    Deserializes the `group` into either a single instance of `type` or a vector of `type`. For types with
    `ExportStyle(T) == VectorExport()` (currently only `AbstractParticle`), the latter, and thus, the second form, applies.

    For more information on the export system, consult the InPartS manual.
    """
    deserialize(::Any, @nospecialize(a::Type); kwargs...) = error("InPartS: no deserializer defined for type $a")

    """
        deserializefromdict(dict::AbstractDict, stype = Any; [sim::Simulation, mod::Module]) → Vector{T} where {T<:AbstractParticle}
        deserializefromdict(dict::AbstractDict, stype = Any; [mod::Module]) → T
        deserializefromdict(notadict, stype = Any; kwargs...) → notadict

    The reverse operation of [`serializetodict`](@ref). Deserializes the `dict` into either a vector or
    a single instance of the type T saved in `dict["$(STRUCTTYPE)"]`, depending on whether `T` has
    `ExportStyle(T) == VectorExport()` (currently only `AbstractParticle`) or `ExportStyle(T) == ScalarExport()`,
    respectively.
    The optional `stype` argument is checked to be `T`'s supertype during reconstruction.
    The optional `mod` keyword argument is passed to [`InPartS.reconstruct_subtype`](@ref) and determines the module for
    type reconstruction.

    Includes a fallback dispath for objects which already have the correct type.

    For more information on the export system, consult the InPartS manual.
    """
    deserializefromdict(dict::AbstractDict, stype = Any; mod::Module = Main, kwargs...) = deserialize(dict, InPartS.reconstruct_subtype(dict[STRUCTTYPE], stype; mod); kwargs...)
    deserializefromdict(notadict, stype=Any; kwargs...) = notadict

    # this is for save callbacks but I put it here because I don't want these words to be reserved in the main InPartS namespace
    @enum OnConflict fail skip compare overwrite

end

"""
    @exportoff(type, fieldname1 [, fieldname2, fieldname3, ...])
Disable export of the fields `fieldname1`, `fieldname2` and so on for all instances of `type`.

!!! note
    Be aware that to be able to import data with disable exports, you need to
    provide a kwargs-only constructor where `fieldname` is not a required argument.
"""
macro exportoff(type, fieldnames...)
    fieldnames::NTuple{N, Symbol} where N
    check_type(type)
    block = quote end
    for field in fieldnames
        val = FieldExport{field}
        push!(block.args,
            quote
                ExportUtils.exportfield(::Type{<:$(esc(type))}, ::$val) = false
            end
        )
    end
    return block
end


"""
    @exporttransform(type, fieldname, fwd, back)
Provide expressions to transform the field `fieldname` for all instances of `type`
for (de)serialization.

### Arguments
*   `fwd` A non-quoted expression specifying how to transform the exported value.
    In the context of this expression, the symbol `value` represents field value
    of the instance which is being exported.
*   `back` A non-quoted expression specifying how to reverse-process the
    information during import. The symbol `data` in this expression represents
    the imported data.

!!! note
    This macro only works for fields, not for manually added exports
"""
macro exporttransform(type, field::Symbol, fwd, back)
    check_type(type)
    val = FieldExport{field}
    return quote
        ExportUtils.exporttransform(::Type{<:$(esc(type))}, ::$val) = $(QuoteNode(fwd))
        ExportUtils.importtransform(::Type{<:$(esc(type))}, ::$val) = $(QuoteNode(back))
    end
end

"""
    @importconstruct type func                                       #1
    @importconstruct type function func(t::Type; data...)            #2
      #body constructing type `t` from keyword args `data`
    end
    @importconstruct type func(t::Type; data...) = otherfunc(...)    #3
    @importconstruct type expr                                       #4
    @importconstruct type kw=true/false                            #5

Provide a custom function `func` which is called instead of the keyword-only constructor of type `type` (and its subtypes)
for its deserialization. The first argument passed to the function is the type itself, the keyword arguments
will contain the data corresponding to the exported fields of the type.

The first form registers an existing function `func`, the second and third define and
register a function at the same time.

The fourth form takes an expression `expr` as the second argument
that directly yields the struct (avoiding the definition of an extra function). In the `expr`, the symbol
`type` refers to the type that is being imported (which will be a subtype of the `type` passed as the
first argument to the macro), and the imported fields are available as variables with the corresponding
names.

The fifth form is a convenience shortcut for redirecting the call directly to the
constructor. In the case of `kw=false`, exported fields are passed as positional arguments,
`kw=true` corresponds to the default of calling the keyword-only constructor.

### Example

```julia
struct MyType
    a::Int
end

reconstructmytype(t; a::Int) = t(a)

# First form
@importconstruct MyType reconstructmytype

# Third form
@importconstruct MyType reconstructmytype2(t; a::Int) = t(a)

# Fourth form
@importconstruct MyType type(a)

# Fifth form
@importconstruct MyType kw=false
```

"""
macro importconstruct(type::Union{Symbol,Expr}, expr::Union{Symbol, Expr})
    return _importconstruct(type, expr)
end

function _importconstruct(type::Union{Symbol,Expr}, expr::Union{Symbol, Expr})
    check_type(type)
    e = QuoteNode(expr)
    return quote
        local mod = @__MODULE__
        Base.eval(mod, _importconstruct_function($(esc(type)), $(esc(e))))
    end
end

function _importconstruct_def(type::Type, func::Symbol)
    return quote
        ExportUtils.importconstruct(t::Type{<:$(type)}; kwargs...) = $(func)(t; kwargs...)
    end
end

function _importconstruct_def(type::Type, e::Expr)
    kwargslist = Expr(:parameters)
    for ex ∈ allexports(type)
        push!(kwargslist.args, ExportUtils.importname(type, ex))
    end
    return quote
        ExportUtils.importconstruct($kwargslist, type::Type{<:$(type)}) = $e
    end
end

function _importconstruct_function(type::Type, func::Symbol) # first form
    return _importconstruct_def(type, func)
end

function _importconstruct_function(type::Type, e::Expr)
    if e.head == :function # second form
        func = e.args[1].args[1]
    elseif e.head == :(=) # third form or fifth form
        if isa(e.args[1], Expr) && e.args[1].head == :call # third form
            func = e.args[1].args[1]
        elseif e.args[1] == :kw # fifth form
            if e.args[2] == false
                args = [ExportUtils.importname(type, ex) for ex in allexports(type)]
                return _importconstruct_def(type, Expr(:call, :type, args...))
            else
                return quote end # do nothing, calling keyword-only constructor is the default
            end
        else
            error("Cannot parse the second argument of @importconstruct")
        end
    else # assume fourth form
        return _importconstruct_def(type, e)
    end

    ic = _importconstruct_def(type, func)

    return quote
        $(e)
        $(ic)
    end
end

"""
    @exportrecursive(type, fieldname)
Convenience wrapper for [`@exporttransform`](@ref) indicating that the field `fieldname`
of `type` is itself an InPartS-exportable type (produced, e.g., with @genexport).

This macro produces code
```julia
@exporttransform {type} {fieldname} ExportUtils.serializetodict(value) ExportUtils.deserializefromdict(data, {fieldtype})
```
where `fieldtype` is the automatically determined type of the field.
"""
macro exportrecursive(type, field::Symbol)
    check_type(type)
    local ft = fieldtype(__module__.eval(type), field)
    return quote
        @exporttransform $(esc(type)) $(field) ExportUtils.serializetodict(value; mod = @__MODULE__) ExportUtils.deserializefromdict(data, $(ft); mod = @__MODULE__)
    end
end


"""
    @exportcollect(type,fieldname,expr,backexpr)
Provide expressions for converting the accumulated `fieldname` values into an
exportable object (e.g an array).

### Arguments
*   `expr` A non-quoted expression specifying how to transform the export.
    In the context of this expression, the symbol `vector` represents the vector
    of `field` values for all the instances of type `type` in the collection
    (for example a particle vector).
*   `backexpr` A non-quoted expression specifying how to reverse-process the
    information during import. The symbol `blob` in this expression represents
    the imported data.

!!! note
    This macro only works for [`VectorExport`](@ref) types!
"""
macro exportcollect(type, field::Symbol, fwd, back)
    check_type(type)
    fieldname = String(field)
    return quote
        let f = ($fieldname), T = $(esc(type))
            fs = Symbol(f)
            val = nothing

            if fs ∈ fieldnames(T)
                val = InPartS.FieldExport{fs}
            else

                local i = 1
                while Base.invokelatest(InPartS.ExportUtils.exportfield, T, InPartS.AdditionalExport(i))
                    if Base.invokelatest(InPartS.ExportUtils.exportname, T, InPartS.AdditionalExport(i)) == f
                        val = AdditionalExport{i}
                        break
                    end
                    i = i + 1
                end
                isnothing(val) && error("Export name $f not found!")
           end

           ExportUtils.exportcollect(::Type{<:$(esc(type))}, ::val) = $(Expr(:quote, fwd))
           ExportUtils.importcollect(::Type{<:$(esc(type))}, ::val) = $(QuoteNode(back))
        end
    end
end

"""
    @exportadd type name fwd back
Adds an additional export field of name `name` to `type`.

### Arguments
 * `fwd` A non-quoted expression specifying how to compute the value of the additional
    export from an instance of `type`.
    In the context of this expression, the symbol `item` represents the instance
    of `type`.
*  `back` A non-quoted expression specifying how to process the information during
    import.
    The symbol `data` in this expression represents the imported data.
    The result of this transformation will be passed to the kwarg-only constructor
    of `type` when reconstructing the data.

!!! warning

    If there is no kwargs-only constructor of your type that accepts `name` as a keywoard argument,
    importing your data will fail!
"""
macro exportadd(type, newname, fwd, back)
    check_type(type)

    name = string(newname)
    return quote
        let name = $name, T = $(esc(type))
            if Symbol(name) ∈ fieldnames(T)
                error("Export name $name already exists, use @exporttransform to transform an existing export name")
            else
                local i = 1
                while Base.invokelatest(InPartS.ExportUtils.exportfield, T, InPartS.AdditionalExport(i))
                    if Base.invokelatest(InPartS.ExportUtils.exportname, T, InPartS.AdditionalExport(i)) == name
                        @warn "additional export $name already exists, overwriting"
                        break
                    end
                    i = i + 1
                end

                val = AdditionalExport{i}
                ExportUtils.exporttransform(::Type{<:T}, ::val) = $(QuoteNode(fwd))
                ExportUtils.importtransform(::Type{<:T}, ::val) = $(QuoteNode(back))
                ExportUtils.exportname(::Type{<:T}, ::val) = $(name)
                ExportUtils.exportfield(::Type{<:T}, ::val) = true
            end
        end
    end
end

"""
    @genexport(type [, importconstruct_arg])
Generates the import/export functions for the given type (necessary for `AbstractParticle`s`
and `AbstractParams`).

The second, optional argument is for the convenience of not having to call `@importconstruct`
separately in case it is needed (see [@importconstruct](@ref) for details):

```julia
@genexport MyType kw=false
```

is equivalent to:

```julia
@importconstruct MyType kw=false
@genexport MyType
```

For more information on the export system, consult the InPartS manual.
"""
macro genexport(type::Union{Symbol,Expr})
    return _genexport(type)
end

macro genexport(type::Union{Symbol,Expr}, constructoroption)
    ge = _genexport(type)
    ic = _importconstruct(type, constructoroption)
    return quote
        $ic
        $ge
    end
end

function _genexport(type::Union{Symbol,Expr})
    return quote
        local mod = @__MODULE__
        Base.eval(mod, _isserializable_function($(esc(type))))
        Base.eval(mod, _serialize_function($(esc(type))))
        Base.eval(mod, _deserialize_function($(esc(type))))
        #ExportUtils.modname(::Type{mod.$type}) = "$mod"
    end
end


## isserializable
"""
    _isserializable_function(T)
Returns an expression containing the definition of [`ExportUtils.isserializable()`](@ref)
for the given type `T`.
"""
_isserializable_function(T::Type) = _isserializable_function(T, ExportStyle(T))

_isserializable_function(T::Type, ::ScalarExport) = quote
    ExportUtils.isserializable(::Type{<:$T}) = true
end

_isserializable_function(T::Type, ::VectorExport) = quote
    ExportUtils.isserializable(::Type{Vector{ET}}) where {ET<:$T} = true
end


## Serialization


"""
    _serialize_function(T)
Returns an expression containing the definition of [`ExportUtils.serialize!()`](@ref)
for the given type `T`.

The signature of `serialize!` may vary depending on whether `T` is an
[`AbstractParticle`](@ref) or [`AbstractParams`](@ref). For more information on
the export system, consult the InPartS manual.
"""
function _serialize_function(T::Type, es::ExportStyle = ExportStyle(T))
    body = Expr(:block)
    body.args = map(allexports(T)) do e
        name = ExportUtils.exportname(T, e)
        transform = ExportUtils.exporttransform(T, e)
        _serialization_expr(T, es, e, name, transform)
    end
    push!(body.args, :(return group))

    signature = ifelse( es isa ScalarExport,
        :(ExportUtils.serialize!(group, item::$T)),
        :(ExportUtils.serialize!(group, items::Vector{<:$T}; sim::Union{Simulation, Nothing} = nothing))
    )
    Expr(:function, signature, body)
end

# params, field export —> access & transform
"""
    _serialization_expr(T, es, e, name, transform)
Returns the expression for serializing the [`ExportItem`](@ref) `e` for the type `T`

Optionally specify custom expressions for export name, transformation.
For more information on the export system, consult the InPartS manual.
"""
_serialization_expr(::Type, ::ScalarExport, ::FieldExport{F}, name, transform) where F = quote
    group[$name] = begin
        value = item.$F
        $transform
    end
end

# params, add. export —> transform
_serialization_expr(::Type, ::ScalarExport, ::AdditionalExport, name, transform) = quote
    group[$name] = $transform
end

# particle, field export —> access, transform & collect
_serialization_expr(T::Type, ::VectorExport, e::FieldExport{F}, name, transform) where F = quote
    group[$name] = begin
        vector = map(items) do item::$T
            value = item.$F
            return $transform
        end
        blob = $(ExportUtils.exportcollect(T, e))
    end
end

# particle, add. export —> transform & collect
_serialization_expr(T::Type, ::VectorExport, e::AdditionalExport, name, transform) = quote
    group[$name] = begin
        vector = map(item->$transform, items)
        blob = $(ExportUtils.exportcollect(T, e))
    end
end



## Deserialization
"""
    _deserialize_function(T)
Returns an expression containing the definition of [`ExportUtils.deserialize()`](@ref)
for the given type `T`.

The return type of `deserialize` may vary depending on the type's [`ExportStyle`](@ref). For more information on
the export system, consult the InPartS manual.
"""
_deserialize_function(T::Type) = quote
    function ExportUtils.deserialize(group, type::Type{<:$T}; sim::Union{Simulation, Nothing} = nothing)
        $(_deserialize_block(T))
    end
end

"""
    _deserialize_block(T)
Returns an expression containing the interior of  [`ExportUtils.deserialize()`](@ref)
for the given type `T`.

For more information on the export system, consult the InPartS manual.
"""
_deserialize_block(T::Type) = _deserialize_block(T, ExportStyle(T))

function _deserialize_block(T::Type, ::ScalarExport)
    fullnt = Expr(:parameters)
    assignments = []
    for (n, e) ∈ enumerate(allexports(T))
        iname = ExportUtils.importname(T, e)
        fname = Symbol("f", n)
        push!(fullnt.args, Expr(:kw, iname, fname))
        source = :(get(group, $(ExportUtils.exportname(T, e)), ExportUtils.MissingValue()))
        transform = ExportUtils.importtransform(T, e)
        push!(assignments,
            Expr(Symbol("="),
            fname,
            if transform == :data
                :($source)
            else
                quote
                    data = $source
                    if !isa(data, ExportUtils.MissingValue)
                        $transform
                    end
                end
            end))
        push!(assignments, :($fname isa ExportUtils.MissingValue && @warn("Expected field $($(string(T))).$($(string(iname))) not found in serialized data.", maxlog=1)))
    end
    quote
        $(assignments...)
        fullnt = ($fullnt,)
        filterednt = ExportUtils.filter_missingvals(fullnt)
        ExportUtils.importconstruct(type; pairs(filterednt)...)
    end
end



function _deserialize_block(T::Type, ::VectorExport)
    fullnt = Expr(:parameters)
    assignments = []
    for (n, e) ∈ enumerate(allexports(T))
        iname = ExportUtils.importname(T, e)
        fname = Symbol("f", n)
        push!(fullnt.args, Expr(:kw, iname, fname))
        source = :(get(group, $(ExportUtils.exportname(T, e)), ExportUtils.MissingValue()))
        collect = ExportUtils.importcollect(T, e)
        transform = ExportUtils.importtransform(T, e)

        push!(assignments,
            Expr(Symbol("="), fname,
            if transform == :data
                if collect == :blob
                    source
                else
                    quote
                        blob = $source
                        blob isa ExportUtils.MissingValue ? blob : $collect
                    end
                end
            else
                quote
                    blob = $source
                    if !isa(blob, ExportUtils.MissingValue)
                        datavec = $collect
                        map(data->$transform, datavec)
                    end
                end
            end))
        push!(assignments, :($fname isa ExportUtils.MissingValue && @warn("Expected field $($(string(T))).$($(string(iname))) not found in serialized data.", maxlog=1)))

    end

    return quote
        # all the importing happens here!
        $(assignments...)
        items = Vector{type}(undef, length(f1)::Int)
        fullnt = ($fullnt,)
        filterednt = ExportUtils.filter_missingvals(fullnt)
        ExportUtils.importconstruct_vector(items, filterednt)
        return items
    end
end



## Utility functions

"""
    fieldexports(T)
collects all field exports of a type `T`
"""
function fieldexports(T::Type)
    exports = FieldExport[]
    sizehint!(exports, fieldcount(T))
    for fn ∈ fieldnames(T)
        e = FieldExport(fn)
        if ExportUtils.exportfield(T, e)
            push!(exports, e)
        end
    end
    return exports
end

"""
    additionalexports(T)
collects all addditional exports of a type `T`
"""
function additionalexports(T::Type)
    exports = AdditionalExport[]
    i = 1
    while true
        e = AdditionalExport(i)
        if !ExportUtils.exportfield(T, e)
            break
        end
        push!(exports, e)
        i += 1
    end
    return exports
end

"""
    allexports(T)
collects all exports of a type `T`
"""
allexports(T::Type) = vcat(fieldexports(T), additionalexports(T))

"""
    collectvectors(vs::AbstractVector{SVector{D, T}}) where {D, T}
Collect `N` `D`-dimensional vectors into an `N×D` matrix.
"""
function collectvectors(vs::AbstractVector{SVector{D, T}}) where {D, T}
    out = Matrix{T}(undef, length(vs), D)

    for (i, vs) ∈ enumerate(vs)
        for j ∈ 1:D
            out[i, j] = vs[j]
        end
    end

    return out
end

tosvectors(blob::Vector{<:SVector}) = blob
function tosvectors(blob)
    N = size(blob, 2)
    if N == 2
        return @views SVector{2, Float64}.(blob[:, 1], blob[:, 2])
    elseif N == 3
        return @views SVector{3, Float64}.(blob[:, 1], blob[:, 2], blob[:, 3])
    else
        return @views SVector{N,Float64}.((blob[:,i] for i=1:N)...)
    end
end
