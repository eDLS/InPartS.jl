"""
    get_version(simgroup)
Parse the version of InPartS used to save `simgroup`
"""
function get_version(simgroup)
    if length(simgroup) == 1
        return v"0.1"
    end
    return VersionNumber(simgroup[VERSIONNAME])
end


############################################################################################
## supplementing equivalent type parameters
############################################################################################
# This function adds type parameters to UnionAll types to yield the concrete type
# they are functionally equivalent to. Methods should be added whenever adding type parameters
# retrospectively. 
# Currently, this function is used during particle importing to supplement type
# parameters which were not saved prior to InPartS 0.7.1 or added to the type later.

legacytypeparameters(::Type{T}) where {T} = T


############################################################################################
## v0.2 legacy import
############################################################################################
# these functions try to account for the additional type parameter introduced to some types in v0.3
# if a type doesn't exist in the current session, they'll try to add a `2` as the first parameter


function readstatic_v2(simgroup; time = (0.0, 0), warn::Bool = true,)

    pttype = readtype(simgroup[PTTYPENAME], super = AbstractParticle)
    pmtype = readtype(simgroup[PMTYPENAME], super = AbstractParams)

    ptctype = let
        try
        t = readtype_v2(simgroup[PTCTYPENAME], super = AbstractParticleContainer)
        catch e
            warn && @warn "Could not load ParticleContainer:$e\n Attempting to use SimpleParticleContainer\n"
            SimpleParticleContainer{2, pttype, BoxGrid{2, pttype, Float64}}
        end
    end
    ptc = ptctype()

    params = readparams(simgroup[PARAMGROUPNAME])

    domain = readdomain(simgroup["world"])
    if REGNAME in keys(simgroup)
        reg = AbstractRegistrar(readdict(simgroup[REGNAME]))
    else
        warn && @warn "No registrar was found in the file"
        reg = SimpleRegistrar{UInt64}()
    end

    rng = readrng(simgroup[RNGNAME], complete = true)

    # obstacles & stuff go here
    if supportsobstacles(ptctype)
        obstacles = readobstacles_v2(simgroup[OBSTACLEGROUPNAME])
        addobstacles!(ptc, obstacles)
        #else
    end

    sim = Simulation(ptc, pttype, pmtype, domain, time..., reg, rng)
    sim.params = params

    return sim
end


function readobstacles_v2(group)
    indices = Vector{Int}()
    params = Vector{AbstractObstacle}()
    for n ∈ keys(group)
        paramindex = parse(Int, n)
        group_cached = readdict(group[n])
        paramtype = reconstruct_subtype_v2(group_cached["_type"], AbstractObstacle)
        push!(indices, paramindex)
        # a lot of obstacles have had an "inverted" field added
        if !haskey(group_cached, "inverted")
            group_cached["inverted"] = false
        end
        push!(params, ExportUtils.deserialize(group_cached, paramtype))
    end
    sp = sortperm(indices)
    return params[sp]
end


function readtype_v2(pt; super = Any, mod = Main)
    #pt = simgroup[name]
    if pt isa Vector
        return Union{InPartS.reconstruct_subtype_v2.(pt, super; mod)...}
    else
        return InPartS.reconstruct_subtype_v2(pt, super; mod)
    end
end


function reconstruct_type_v2(s; mod::Module = Main)
    expr = Meta.parse(s)
    check_type(expr)
    expr = fix_typeparams(expr; mod)
    t = try
        mod.eval(expr)
    catch
        throw(ArgumentError("Could not reconstruct $expr in module $(mod)"))
    end
    @assert t isa Type "Not a type: $name"
    return t
end

function reconstruct_subtype_v2(name::String, super::Type; kwargs...)
    T = reconstruct_type_v2(name; kwargs...)
    @assert T<:super "$name could be reconstructed, but is not a subtype of $super found"
    return T
end


#=
 This checks whether the number of type parameters of t has changed
 If it has changed by one, it tries to add a 2 two to account for the
 explicit dimensionality specifications in InPartS v0.3
=#
function fix_typeparams(t::Union{Symbol, Expr}; mod::Module = Main)
    name, params, omitted = splitstoredtype(t)

    # recursively check parameters
    params = map(p->(p ∉ omitted) ? fix_typeparams(p) : p, params)

    basetype = mod.eval(name)
    fp = freeparams(basetype)
    nmissingparams = fp - length(params)

    if nmissingparams == 0
        # everything fine
        return t
    elseif nmissingparams == 1
        # add a 2 two make it 2D or (for older RodCells) a RodMobility
        missingparam = name == :RodCell ? :(ColonyGrowthModels.RodMobility) : 2
        newsig = Expr(:curly, name, missingparam, params...)
        # add a `where` if needed
        isempty(omitted) || (newsig = Expr(:where, newsig, omitted...))
        @debug "Stored type signature $t converted into $newsig"
        return newsig
    elseif nmissingparams == 2
        # add a 2 two make it 2D and a BoxGrid
        newsig = Expr(:curly, name, 2, params..., :(InPartS.BoxGrid{2, Float64, $(params[1])}))
        # add a `where` if needed
        isempty(omitted) || (newsig = Expr(:where, newsig, omitted...))
        @debug "Stored type signature $t converted into $newsig"
        return newsig
    else
        # this shouldn't happen
        error("Parameter mismatch: Saved type $t has $(length(params)) parameters, $name has $fp")
    end
end

# every other AST leaf node goes here
fix_typeparams(s; kwargs...) = s



############################################################################################
## v0.1 legacy import
############################################################################################


function legacy_load(f; time = (0.0, 0))
    simg = f[only(keys(f))]
    pttype = InPartS.reconstruct_subtype(simg["particleType"], AbstractParticle)
    pmtype = ParamType(pttype)
    wsize = SV{Float64}(simg["parameters"]["size"])
    domain = Domain2D{PeriodicBoundary, Boundary}(wsize)
    reg = SimpleRegistrar{UInt64}()
    sim = Simulation(SimpleParticleContainer{pttype}(), pttype, pmtype, domain, time[1], time[2], reg)

    params = readdict(f[only(keys(f))]["parameters"])
    push!(sim.params, pmtype(
                maxlength=get(params, "maxlength", 1.0),
                radius = params["cellRadius"],
                youngsmodulus = 4*params["hertz"],
                hertz_int = params["hertz_int"],
                ))
    sim
end

legacy_readsnap!(sim::Simulation, f, snapid::Integer; kwargs...) = legacy_readsnap!(sim, f, "snap"*string(snapid, pad=5); kwargs...)

function legacy_readsnap!(sim::Simulation, f, snapname::String; warn=warn)
    snapg = f[only(keys(f))]["snapshots"][snapname]
    particles, time, step, next_id, rng = legacy_readsnap(snapg; sim = sim)
    setparticles!(sim.particles, particles)
    sim.t[] = time
    sim.step[] = step
    sim.registrar.next_id = next_id
    if ismissing(rng)
    	warn && @warn "Could not reconstruct rng"
    else
    	(sim.rng = rng)
    end
    return sim
end

function legacy_readsnap(snapgroup; sim::Simulation{PTC, PT}) where {PTC,PT}
    cellID = snapgroup["cellID"]::Vector{UInt64}
    centerpos = snapgroup["centerpos"]::Matrix{Float64}
    growthProg = snapgroup["growthProg"]::Vector{Float64}
    growthRate = snapgroup["growthRate"]::Vector{Float64}
    nodesep = snapgroup["nodeSeparation"]::Vector{Float64}
    phi = snapgroup["phi"]::Vector{Float64}

    particles = map(1:length(cellID)) do n
        PT(;
            id = cellID[n],
            centerpos = SV{Float64}(centerpos[n, 1], centerpos[n, 2]),
            nodeseparation = nodesep[n],
            φ = phi[n],
            growthprog = growthProg[n],
            growthrate = growthRate[n],
            params=only(sim.params)
            )
    end

    return particles,
            snapgroup["t"],
            snapgroup["step"],
            snapgroup["nextID"],
            missing
end
