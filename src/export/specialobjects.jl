## Snap IO definitons for POLOContainer

const SNAPLONAME = "largeobjects"
const POLOSNAPDATANAMES = union(InPartS.SNAPDATANAMES, [SNAPLONAME])

function writesnap(group, sim::Simulation{<:POLOContainer}; kwargs...)
    snapgroup = _writesnap_common(group, sim; kwargs...)

    logroup = gcreate(snapgroup, SNAPLONAME)
    for (i,f) ∈ enumerate(sim.particles.lov)
        fg = gcreate(logroup, string(i))
        # we have to use vector export because los are <:AbstractParticle
        ExportUtils.serialize!(fg, [f])
        fg["_type"] = string(typeof(f))
    end

    return
end

function InPartS.readsnap(snapgroup, sim::Simulation{<:POLOContainer}; kwargs...)
    logroup = snapgroup[SNAPLONAME]
    lov = eltype(sim.particles.lov)[]
    for i ∈ 1:length(logroup)
        fg = logroup[string(i)]
        type = InPartS.reconstruct_type(fg["_type"])
        push!(lov, ExportUtils.deserialize(fg, type)[])
    end

    return _readsnap_common(snapgroup, sim; ignorelist = POLOSNAPDATANAMES, kwargs...)..., lov
end

function InPartS.setstate!(sim::Simulation{<:POLOContainer}, state; kwargs...)
    state = state
    _setstate_common!(sim, state[1:end-1]; kwargs...)
    sim.particles.lov = state[end]
    return sim
end
