"""
    MemoryExport
Export backend for saving snapshots into in-memory `OrderedDict`s.

Since InPartS requires a "file system" for some operations, this provides
a virtual in-memory "file system" (it's just an `OrderedDict`) that can be accessed with
 `MemoryExport.dictfs("/virtual/path/(can be literally any string as long as it is unique)/")`
"""
module MemoryExport
    using ..OrderedCollections
    import InPartS: dfopen, dfclose, dfrename, gcreate, isgroup, readdict

    const DICTFS = Ref(OrderedDict{String, Any}())

    dictfs() = DICTFS[]
    dictfs(key) = dictfs()[key]


    function dfopen(type::Type{<:OrderedDict}, filename::String; kwargs...)
        allflags = Base.open_flags(;kwargs...)
        # create empty dict if allowed to
        if allflags.create && (allflags.truncate || filename ∉ keys(dictfs()))
            dictfs()[filename] = OrderedDict{String, Any}()
        end

        return dictfs(filename)
    end

    dfclose(::OrderedDict) = nothing

    function dfrename(::Type{<:OrderedDict}, oldname, newname; force = false)
        if !force && (newname ∈ keys(dictfs()))
            error(newname * " already exists (use force = true to overwrite)")
        end
        dictfs()[newname] = pop!(dictfs(), oldname)
        return
    end


    function gcreate(group::OrderedDict{String, Any}, key; kwargs...)
        group[key] = OrderedDict{String, Any}()
    end

    isgroup(group::OrderedDict{String, Any}, key) = group[key] isa OrderedDict{String, Any}

    readdict(d::OrderedDict; kwargs...) = d
end

"""
    compare_to_snapshot(s::Union{DynamicState, Simulation}, snapgroup)
Compares the state of `s` to the snapshot stored in snapgroup by serializing `s` into a dictionary and recursively
comparing the data. Returns `true` if no mismatch is found.
"""
compare_to_snapshot(state::DynamicState, snapgroup; kwargs...) = compare_to_snapshot(state.sim, snapgroup; kwargs...)

function compare_to_snapshot(sim::Simulation, snapgroup; dumprng = true)
    # dump sim to dictionary
    currentstate = OrderedDict{String, Any}()
    writesnap(currentstate, sim; name = "test", dumprng)

    # load snap to dictionary (maxdepth isn't honored by most backends but we set it anyway)
    disksnap = readdict(snapgroup; maxdepth = 256)

    # recursive comparison
    return _compare_dicts(currentstate["test"], disksnap)
end


function _compare_dicts(snap1::AbstractDict{String, Any}, snap2::AbstractDict{String, Any})
    # check if keys match first
    k1 = keys(snap1)
    k2 = keys(snap2)
    if k1 == k2 # miraculously this works for KeySets
        for k ∈ k1
            if k == SNAPRTSNAME
                # do not compare realtime timestamps
                continue
            end
            v1 = snap1[k]
            v2 = snap2[k]
            if v1 isa AbstractDict{String, Any} && v2 isa AbstractDict{String, Any}
                _compare_dicts(v1, v2) || (return false)
            elseif v1 != v2
                @debug "MISMATCH:\n\t$k=$v1\n\t$k=$v2"
                return false
            end
            @debug "MATCH:\n\t$k=$v1"
        end
        return true
    end
    return false
end




