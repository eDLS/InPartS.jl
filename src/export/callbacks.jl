export BinaryDumpCallback, SaveCallback, BufferedSaveCallback, BackupCallback

"""
    BinaryDumpCallback(trigger::AbstractCallback, filename)
Callback that uses the Julia serializer to dump the [`DynamicState`](@ref) into
a binary output file at `filename`.
Intermediate files are written in [`prepropagate!`](@ref) whenever the `trigger` callback
is triggered, a final file is written on [`finalize!`](@ref) or [`freeze!`](@ref)

Not intended for long-term storage of data (use [`SaveCallback`](@ref) instead).
"""
mutable struct BinaryDumpCallback{CB<:AbstractCallback} <: AbstractCallback
    trigger::CB
    filename::String
    intermediate::String
    BinaryDumpCallback(trigger::CB, filename::String) where {CB<:AbstractCallback} =
        new{CB}(trigger, filename, filename*"_intermediate")
end

function prepropagate!(cb::BinaryDumpCallback, state::DynamicState)
    if !(prepropagate!(cb.trigger, state) == true)
        return
    end
    Serialization.serialize(cb.intermediate, state)
    mv(cb.intermediate, cb.filename; force=true)
    return
end

function finalize!(cb::BinaryDumpCallback, state::DynamicState)
    Serialization.serialize(cb.intermediate, state)
    mv(cb.intermediate, cb.filename; force=true)
    return
end

freeze!(cb::BinaryDumpCallback, state::DynamicState) = finalize!(cb, state)



"""
    SaveCallback{backend}(trigger::AbstractCallback, filename, dumprng, warn, onconflict)
Create a callback that periodically saves simulation snapshots to `filename`.

# Extended help

A snapshot is saved every time the `trigger` callback is triggered.
On first activation, static simulation parameters are saved.

If the two-argument function `dumprng`, called on the `SaveCallback` and Simulation,
returns `true`, the full RNG state is dumped to the snapshot.
"""
mutable struct SaveCallback{FT, CB<:AbstractCallback, FN<:Function} <: AbstractCallback
    trigger::CB
    filename::String
    nextsnap::UInt64 # as if anyone is going to write more than typemax(Int64) snaps…
    initialized::Bool
    dumprng::FN
    warn::Bool
    onconflict::ExportUtils.OnConflict
    function SaveCallback{FT}(trigger::CB, filename::String, fn::FN, warn::Bool, onconflict::ExportUtils.OnConflict) where {FT, CB, FN}
        if onconflict == ExportUtils.overwrite
            error("Snapshot overwriting is not yet supported, please set onconflict to another option.")
        end
        return new{FT, CB, FN}(trigger, filename, 0, false, fn, warn, onconflict)
    end
end

SaveCallback(trigger, filename, fn, warn, oc) = SaveCallback{backend(filename)}(trigger, filename, fn, warn, oc)

"""
    SaveCallback([backend], filename; [interval = 1.0], [offset = 0.0], [dumprng = (s, sim) -> (s.nextsnap % 10 == 0)], [warn = IOWARN[]], [onconflict = ExportUtils.fail])
Create a [`SaveCallback`](@ref) that periodically saves simulation snapshots to `filename`.

Uses a [`PeriodicCallback`](@ref) with the given `interval` and `offset`.
"""
SaveCallback(backend, filename; interval = 1.0, offset = 0.0,  dumprng = (s, sim) -> _defaultdumprng(sim.rng, s), warn = IOWARN[], onconflict = ExportUtils.fail) =
    SaveCallback(PeriodicCallback(sim -> true, interval; offset = offset), filename, dumprng, warn, onconflict)

SaveCallback(filename::String; kwargs...) = SaveCallback(backend(filename), filename; kwargs...)


function prepropagate!(s::SaveCallback, state::DynamicState)
    if !s.initialized
        _init(s, state.sim)
        s.initialized = true
    end
    if !(prepropagate!(s.trigger, state) == true)
        return
    end

    _savesnap(s, state)
    return
end

# Xoshiros are small and can be dumped every snapshot, MersenneTwisters are huge and shouldn't be written as often
# all other RNGs (including task local/global Xoshiros) are not supported for export
_defaultdumprng(::Random.AbstractRNG, _) = false
_defaultdumprng(::Random.Xoshiro, _) = true
_defaultdumprng(::Random.MersenneTwister, s) = (s.nextsnap % 10 == 0)

function _init(s::SaveCallback{FT}, sim) where FT
    dfopen(FT, s.filename, "w") do f
        _init(f, s, sim)
    end
    return
end

function _init(f, s::SaveCallback{FT}, sim) where FT
    writestatic(f, sim; s.warn)
    # write initial snapshot with RNG dump
    writesnap(f[SNAPGROUPNAME], sim; name = snapname!(s), dumprng = true, s.warn)
    return
end


function _savesnap(s::SaveCallback{FT}, state::DynamicState; dumprng = s.dumprng(s, state.sim)) where FT
    f = dfopen(FT, s.filename, "a")
    try
        newsnapname = snapname!(s)
        # deal with name conflicts
        if haskey(f[SNAPGROUPNAME], newsnapname)
            msg =  "Tried to write snapshot \"$s\" but it already exists in the file"
            if s.onconflict == ExportUtils.skip
                @warn msg*", skipping this snapshot."
                return
            elseif s.onconflict == ExportUtils.compare
                if compare_to_snapshot(state, f[SNAPGROUPNAME][newsnapname]; dumprng)
                    @warn msg*" with the same data, skipping this snapshot."
                    return
                else
                    error(msg*" with different data.")
                end
            # elseif s.onconflict == ExportUtils.overwrite
            #     @warn msg". Unlinking and overwriting existing snapshot"
            #     this isn't implemented in the backend API yet
            #     don't return here so the snapshot is still written below
            else
                # for ExportUtils.fail (and also anything else, this is the default for a reason)
                error(msg*".")
            end
        end
        # write the snapshot
        writesnap(f[SNAPGROUPNAME], state.sim; name = newsnapname, dumprng = dumprng, s.warn)
    finally
        dfclose(f)
    end
    return
end

function finalize!(s::SaveCallback{FT}, state::DynamicState) where FT
    sim = state.sim

    f = dfopen(FT, s.filename, "a")
    try
        if !s.initialized
            @warn "Finalizing an empty data file, consider running longer simulations."
            _init(f, s, state.sim)
            s.initialized = true
        end
        @assert haskey(f, SNAPGROUPNAME) # this should never fail

        # write final snapshot with RNG dump
        writesnap(f[SNAPGROUPNAME], sim; name = snapname!(s), dumprng = true, s.warn)
        writefinal(f, sim; s.warn)
    finally
        dfclose(f)
    end
    return
end

function snapname!(s::SaveCallback)
    sn = string(s.nextsnap)
    s.nextsnap += 1
    return sn
end



"""
    BufferedSaveCallback([backend], filename; [snapinterval = saveinterval], [flushinterval = 10snapinterval], [offset = 0.0], [dumprng = (s, sim) -> (s.nextsnap % 10 == 0)], [warn = IOWARN[]], [onconflict = ExportUtils.fail])
Create a [`SaveCallback`](@ref) that periodically saves simulation snapshots to `filename`.

Uses a [`PeriodicCallback`](@ref) with the given `interval` and `offset`.
"""
mutable struct BufferedSaveCallback{FT, SC<:SaveCallback, CB} <: AbstractCallback
    filename::String
    flushtrigger::CB
    _savecallback::SC

    initialized::Bool

    # we use our own instead of relying on the scb
    warn::Bool
    onconflict::ExportUtils.OnConflict
    function BufferedSaveCallback{FT}(trigger::CB, filename::String, scb::SC, warn::Bool, onconflict::ExportUtils.OnConflict) where {FT, CB, SC}
        if onconflict == ExportUtils.overwrite
            error("Snapshot overwriting is not yet supported, please set onconflict to another option.")
        end

        if warn != scb.warn
            @warn string(
                "Inconsistent export verbosity settings: warnings ",
                _state_name(warn),
                " for disk operations but ",
                _state_name(scb.warn)*" for buffer operations."
            )
        end

        return new{FT, SC, CB}(filename, trigger, scb, false, warn, onconflict)
    end
end

_state_name(b) = b ? "enabled" : "disabled"

function BufferedSaveCallback(savetrigger, flushtrigger, filename, dumprng, warn, oc)
    scb = SaveCallback{OrderedDict{String, Any}}(savetrigger, filename, dumprng, warn, oc)
    BufferedSaveCallback{backend(filename)}(flushtrigger, filename, scb, warn, oc)
end


BufferedSaveCallback(filename; snapinterval = 1.0, flushinterval = 10snapinterval, offset = 0.0,  dumprng = (s, sim) -> _defaultdumprng(sim.rng, s), warn = IOWARN[], onconflict = ExportUtils.fail) =
    BufferedSaveCallback(
        PeriodicCallback(sim -> true, snapinterval; offset = offset),
        PeriodicCallback(sim -> true, flushinterval; offset = offset),
        filename, dumprng, warn, onconflict
    )


function prepropagate!(s::BufferedSaveCallback, state::DynamicState)
    prepropagate!(s._savecallback, state)

    s.initialized || _init(s, state.sim)
    if !(prepropagate!(s.flushtrigger, state) == true)
        return
    end

    _flushmemoryexport(s, state)
    return
end

function _init(s::BufferedSaveCallback{FT}, sim) where FT
    dfopen(FT, s.filename, "w") do f
        _init(f, s, sim)
    end
    s.initialized = true
    return
end

function _init(f, s::BufferedSaveCallback{FT}, sim) where FT
    writestatic(f, sim; s.warn)
    return
end

function _flushmemoryexport(s::BufferedSaveCallback{FT}, state) where FT
    if !s.initialized
        @warn "Flushing buffer into uninitialized file, this shouldn't happen"
        _init(s, state.sim)
    end
    memfile = dfopen(OrderedDict{String, Any}, s.filename)
    memsnaps = memfile[SNAPGROUPNAME]
    dfopen(FT, s.filename, "a") do diskfile
        disksnaps = diskfile[SNAPGROUPNAME]
        _writedict_nocreate!(disksnaps, memsnaps, s.warn)
    end
    # clear memfile after everything is saved
    memfile[SNAPGROUPNAME] = OrderedDict{String, Any}()
    return
end

# like InPartS.writedict but writes into an existing group
function _writedict_nocreate!(group, dict, warn = IOWARN[])
    for (k, v) ∈ pairs(dict)
        if v isa AbstractDict
            writedict(group, v; name = string(k), warn)
        else
            group[string(k)] = v
        end
    end
    return
end

function finalize!(s::BufferedSaveCallback{FT}, state::DynamicState) where FT
    finalize!(s._savecallback, state)
    _flushmemoryexport(s, state)
    sim = state.sim

    f = dfopen(FT, s.filename, "a")
    try
        if !s.initialized
            @warn "Finalizing an empty data file, consider running longer simulations."
            _init(f, s, state.sim)
            s.initialized = true
        end
        @assert haskey(f, SNAPGROUPNAME) # this should never fail

        writefinal(f, sim; s.warn)
    finally
        dfclose(f)
        # “delete” memfile
        delete!(MemoryExport.dictfs(), s.filename)
    end
    return
end



"""
    BackupCallback{backend}(trigger::AbstractCallback, filename; [warn = IOWARN[]])
Creates a callback that dumps the entire simulation state to `filename`.

# Extended help

The entire simulation state, consisting of static parameters and a full snapshot, is saved
every time the `trigger` callback is triggered.
The data file is truncated before writing.
"""
struct BackupCallback{FT, CB} <: AbstractCallback
    trigger::CB
    filename::String
    warn::Bool
    BackupCallback{FT}(trigger::CB, filename, warn) where {FT, CB} = new{FT, CB}(trigger, filename, warn)
end

BackupCallback(trigger, filename, warn) = BackupCallback{backend(filename)}(trigger, filename, warn)

"""
   BackupCallback(backend, filename; [interval = 300.0], [offset = 0.0], [warn = IOWARN[]])
Creates a [`BackupCallback`](@ref) that periodically dumps the entire simulation state to `filename`.

Uses a [`RealTimeCallback`](@ref) with the given `interval` and `offset`.
"""
BackupCallback(backend, filename; interval = 300.0, offset = 0.0, warn = IOWARN[]) =
    BackupCallback(RealTimeCallback(interval, sim -> true; offset = offset), filename, warn)
BackupCallback(filename::String; kwargs...) =
    BackupCallback(backend(filename), filename; kwargs...)


function prepropagate!(cb::BackupCallback{FT}, state::DynamicState) where FT
    if !(prepropagate!(cb.trigger, state) == true)
        return
    end
    sim = state.sim
    tf = joinpath(dirname(cb.filename), "#"*basename(cb.filename))
    f = dfopen(FT, tf, "w")
    try
        writestatic(f, sim)
        # backup files only contain one snapshot
        writesnap(f[SNAPGROUPNAME], sim; name = "0", dumprng = true)
        writefinal(f, sim)
    finally
        dfclose(f)
    end
    # only overwrite potential existing file if saving was successful
    dfrename(FT, tf, cb.filename; force = true)
    return
end

finalize!(cb::BackupCallback, state::DynamicState) = prepropagate!(cb, state)
