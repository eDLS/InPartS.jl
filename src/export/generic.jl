export Snapshots, readsim

# this may be set from ENV by the __init__ function
const IOWARN = Ref(true)
## Datafile interface definitions

"""
    exportbackend(filename)
A bit of sugar around `FileIO.query`.
"""
function backend(filename::String)
    s = FileIO.query(filename)
    modulename = typeof(s).parameters[1].parameters[1]
    # this is a hack to get around the fact that extensions cannot export copied
    # so we can't use the JLD2Wrapper/H5Wrapper types from here
    return _backend(Val(modulename))
end

_backend(v) = error("The backend $v is not implemented or was not correctly loaded.")

"""
    dfopen(backend, filename; kwargs...)
    dfopen(backend, filename, mode::String)
Open the data file at `filename`.

Mode specifications follow [`Base.open`](@extref).
"""
dfopen(type::Type, filename, mode) = dfopen(type, filename; modetoflags(mode)...)
dfopen(@nospecialize(type::Type), filename; kwargs...) = error("Unknown export backend: $type")
dfopen(filename::String, mode="r"; kwargs...) = dfopen(backend(filename), filename, mode; kwargs...)

function dfopen(f::Function, args...; kwargs...)
    file = dfopen(args...; kwargs...)
    try
        f(file)
    finally
        dfclose(file)
    end
end


function dfclose(file) end
function dfrename(type::Type, oldname, newname; force = false) end

function gcreate(group, key) end
function isgroup(group, key) end

# why does this not already exist‽‽‽
function modetoflags(mode::String)
    if mode == "r"
        return Base.open_flags()
    elseif mode == "w"
        return Base.open_flags(write = true)
    elseif mode == "a"
        return Base.open_flags(append = true)
    elseif mode == "r+"
        return Base.open_flags(read = true, write = true)
    elseif mode == "w+"
        return Base.open_flags(truncate = true, read = true)
    elseif mode == "a+"
        return Base.open_flags(append = true, read = true)
    else
        throw(ArgumentError("invalid open mode: $mode"))
    end
end

## Lazy snapshot accessor

"""
    Snapshots(path::String)
A lazy iterator to access snapshots in the InPartS data file at `path`.

Indexing syntax `Snapshots(path)[i]` will open the file and attempt to load the `i`th
snapshot using [`readsim`](@ref).
Indexing with a unit range `Snapshots(path)[i:j]` will return a `Snapshots` struct with
modified `firstindex` and `lastindex`. Note that bounds checking may not be performed here,
as the file isn't opened and therefore the last snapshot index cannot be determined if it isn't
already known from a previous file access. Therefore, the resulting `Snapshots` may be invalid.

Iteration is also supported, but keep in mind that the data file will be opened and closed
in every single iteration.

Requires your data file to use zero indexed sequentially numbered snapshots (as is the case
if you used a [`SaveCallback`](@ref)).
"""
struct Snapshots
    path::String
    _firstsnap::Int # this is not a ref as it must be set on construction (either its 0 or you know what you want)
    _lastsnap::Ref{Int}
    # use -2 as magic value to distinguish empty files (numsnaps = 0)
    Snapshots(path::String; firstsnap = 0, lastsnap = -2) = new(path, firstsnap, Ref(lastsnap))
end

# indexed access
function Base.getindex(sn::Snapshots, i::Integer)
    @boundscheck if !(firstindex(sn) ≤ i ≤ lastindex(sn))
        throw(BoundsError(sn, i))
    end
    readsim(sn.path, snap = i)
end

function Base.getindex(sn::Snapshots, is::UnitRange)
    # don't do a full bounds check in case `lastindex` doesn't work
    @boundscheck if !(_maybe_check_bounds(sn, first(is)) && _maybe_check_bounds(sn, last(is)))
        throw(BoundsError(sn, is))
    end
    return Snapshots(sn.path; firstsnap = first(is), lastsnap = last(is))
end

function _maybe_check_bounds(sn::Snapshots, i)
    if sn._lastsnap[] < -1
        @warn "Index may be out of range for $sn (last snap not known)" maxlog=1
        return firstindex(sn) ≤ i
    else
        return firstindex(sn) ≤ i ≤ lastindex(sn)
    end
end


Base.firstindex(sn::Snapshots) = sn._firstsnap

function Base.lastindex(sn::Snapshots)
    if sn._lastsnap[] < -1
        nsnaps = numsnaps(sn.path)
        sn._lastsnap[] = nsnaps - 1
    end
    return sn._lastsnap[]
end

Base.eachindex(sn::Snapshots) = firstindex(sn):lastindex(sn)

# iteration
Base.iterate(sn::Snapshots, state::Int = 0) = state > lastindex(sn) ? nothing : (sn[state],  state + 1)
Base.length(sn::Snapshots) = lastindex(sn) + 1
Base.size(sn::Snapshots) = (length(sn), )
Base.eltype(::Snapshots) = InPartS.Simulation


Base.show(io::IO, sn::Snapshots) = print(io, "Snapshots($(sn.path))[$(firstindex(sn)):$(sn._lastsnap[] < -1 ? "???" : lastindex(sn))]")



## Generic writing functions
# writestatic
"""
    writestatic(simgroup, sim; [warn = IOWARN[]]) → snapgroup, paramgroup
Writes the static bits of the simulation to the output group `simgroup`.

This includes the particle and parameter types, the domain and some InPartS version
information.
"""
function writestatic(simgroup, sim::Simulation{PTC, PT, PM}; warn = IOWARN[]) where {PTC, PT, PM}
    simgroup[TREEHASHNAME] = InPartS.TREE_HASH
    simgroup[VERSIONNAME] = string(InPartS._INPARTS_VERSION)

    writetype(simgroup, PTC, name = PTCTYPENAME)
    writetype(simgroup, PT; name = PTTYPENAME)
    writetype(simgroup, PM, name = PMTYPENAME)
    writedomain(simgroup, sim.domain; warn)

    writerng(gcreate(simgroup, RNGNAME), sim.rng; complete = true, warn)

    # Keyword argument is only used by JLD2 for performance. HDF5 ignores it.
    gcreate(simgroup, SNAPGROUPNAME; est_num_entries = EST_NUM_SNAPSHOTS)
    gcreate(simgroup, PARAMGROUPNAME)

    # write params already present at the start
    # if new params are added during the simulation, they will be added during finalization
    writeparams(simgroup[PARAMGROUPNAME], sim.params; warn)

    #obstacles & stuff go here
    if supportsobstacles(PTC)
        writeobstacles(simgroup, sim)
    end
    return
end


function writerng(group, rng::T; complete::Bool = false, warn = IOWARN[]) where {T<:AbstractRNG}
    group["_type"] = string(T)
    group["_rng_complete"] = (T == Xoshiro) || complete # Xoshiro is always completely serialized
    if complete
        _serialize_rng_full!(group, rng; warn)
    else
        _serialize_rng_partial!(group, rng; warn)
    end
    return
end



"""
    writetype(group, PT::Type; name, [warn = IOWARN[]])
Creates a dataset of name `name` in `group` and saves the type name of `PT`.
If `PT` is a `Union`, the union types´ names will be written instead as a vector.
"""
function writetype(group, PT::Type; name, prefix = "", warn = IOWARN[])
    if PT isa Union
        typenames = string.(Base.uniontypes(PT))
        group[name] = prefix.*typenames
    else
        group[name] = prefix*string(PT)
    end
    return
end


"""
    writedomain(group, domain; [name = DOMAINGROUPNAME], [warn = IOWARN[]])
Creates a new subgroup called `name` in `DOMAINSIZENAME` and fills it withthe static properties
of the domain (to wit, size & boundaries).
"""
function writedomain(group, domain::Domain2D{BCX, BCY}; name = DOMAINGROUPNAME, warn = IOWARN[]) where {BCX, BCY}
    wgroup = gcreate(group, name)
    wgroup[DOMAINSIZENAME] = domain.size
    wgroup[DOMAINBCNAME] = [string(BCX), string(BCY)]
    return
end

"""
    writedoamin(group, domain; [name = DOMAINGROUPNAME], [warn = IOWARN[]])
Creates a new subgroup called `name` in `group` and fills it withthe static properties
of the domain (to wit, size & boundaries).
"""
function writedomain(group, domain::Domain3D{BCX, BCY, BCZ}; name = DOMAINGROUPNAME, warn = IOWARN[]) where {BCX, BCY, BCZ}
    dgroup = gcreate(group, name)
    dgroup[DOMAINSIZENAME] = domain.size
    dgroup[DOMAINBCNAME] = [string(BCX), string(BCY), string(BCZ)]
    return
end

# writesnap (snapshots)

"""
    writesnap(group, sim; name = nextsnapname(group), [warn = IOWARN[]], kwargs...)
Creates a new subgroup called `name` in `group` and saves a snapshot of the current
state of the dynamical properties of `sim`.

### Optional arguments

  * `dumprng` whether to save (that is, write all fields recursively) the current state
    of the random number generator. Defaults to true if the simulation does not use
    the global RNG.
"""
function writesnap(group, sim::Simulation; kwargs...)
    _writesnap_common(group, sim; kwargs...)
    return
end



function splitbytype(particles::AbstractVector{T}) where {T<:AbstractParticle}
    if isconcretetype(T)
        return (particles,)
    end
    ret = Dict{DataType, Vector{<:AbstractParticle}}()
    for p ∈ particles
        S = typeof(p)
        haskey(ret, S) || (ret[S] = S[])
        push!(ret[S], p)
    end
    return values(ret)
end

# this function deals with particles, times, steps, registrars etc.
# and can be used when overwriting writesnap for custom PTCs
function _writesnap_common(group, sim::Simulation{PTC, PT};
    name = nextsnapname(group),
    dumprng = !(sim.rng isa Random._GLOBAL_RNG),
    warn = IOWARN[]
  ) where {PTC, PT}
    snapgroup = gcreate(group, name)

    snapgroup[SNAPTIMENAME] = sim.t
    snapgroup[SNAPSTEPNAME] = sim.step
    snapgroup[SNAPRTSNAME] = time()
    snapgroup[SNAPNEXTIDNAME] = sim.registrar.next_id

    if dumprng
        writerng(gcreate(snapgroup, RNGNAME), sim.rng; complete = true, warn)
    end

    particlegroups = splitbytype(particles(sim))
    for pg ∈ particlegroups
        # Save with concrete particle type so it can always be correctly reconstructed
        writepgroup(snapgroup, pg, sim; name = string(eltype(pg)), warn)
    end
    return snapgroup
end

"""
    writepgroup(group, particles::Vector{PT}, sim; [name = string(PT)], [warn = IOWARN[]])
Tries to serialize the `particles` into a new subgroup `name` of `group`.

Do not expect this to work if `PT` is a Union.
"""
function writepgroup(group, particles::AbstractVector{PT}, sim::Simulation; name = string(PT), warn = IOWARN[]) where {PT}
    pgroup = gcreate(group, name)
    ExportUtils.serialize!(pgroup, particles; sim = sim)
end

# writefinal (final things)

"""
    writefinal(group, sim; [warn = IOWARN[]])
Write the information that is only available at the end of the simulation.

At the moment, this includes the registrar and the parameter vector
"""
function writefinal(group, sim::Simulation; warn = IOWARN[])
    writedict(group, Dict(sim.registrar); name = REGNAME)
    writeparams(group[PARAMGROUPNAME], sim.params; warn)
end


"""
    writeparams(group, params; [warn = IOWARN[]])
Tries to serialize the `params` into a new subgroup of `group` named i, the index
of the param struct .

In contrast to [`writepgroup`](@ref), this will actually work for abstract/union
eltypes.
"""
function writeparams(group, params::AbstractVector{PT}; warn = IOWARN[]) where {PT<:AbstractParams}
    for (i, p) ∈ enumerate(params)
        if string(i) ∉ keys(group)
            paramgroup = gcreate(group, string(i))
            ExportUtils.serialize!(paramgroup, p)
            writetype(paramgroup, typeof(p); name = "_type", warn)
        end
    end
end

# TODO: circular references? what circular references?
function writedict(group, dict::AbstractDict; name, warn = IOWARN[])
    dgroup = gcreate(group, name)
    for (k, v) ∈ pairs(dict)
        if v isa AbstractDict
            writedict(dgroup, v; name = string(k), warn)
        else
            dgroup[string(k)] = v
        end
    end
end


## generic reading functions

## Convenience function for reading things

"""
readsim(f; snap = InPartS.lastfullsnap(f), [warn = IOWARN[]], [kwargs...])
readsim(filename; snap, [warn = IOWARN[]], [kwargs...])
Read a simulation from file and load the specified snapshot. Defaults to the last full snapshot
(see [`lastfullsnap`](@ref)).

Additional keyword arguments are passed through to [`readstatic`](@ref) and [`readsnap`](@ref).
"""
function readsim(f; snap = lastfullsnap(f), warn = IOWARN[], kwargs...)
    sim = readstatic(f; warn, kwargs...)
    try
        readsnap!(sim, f, snap; warn, kwargs...)
    catch e
        warn && @warn "Could not read snapshot: $e\nSimulation may be invalid."
        throw(e)
    end
    return sim
end

function readsim(filename::AbstractString; kwargs...)
    f = dfopen(filename, "r")
    sim = missing
    try
        sim = readsim(f; kwargs...)
    finally
        dfclose(f)
    end
    return sim
end

# readstatic (simulation)

"""
    readstatic(simgroup; [time], [warn = IOWARN[]]) → sim::Simulation
Reconstructs a [`Simulation`](@ref) from the information saved in `simgroup`.

To load a specific snapshot into the `Simulation`, use [`readsnap!`](@ref).

### Optional arguments
  * `time` specify the simulation time in  time units and  steps. Defaults to `(0.0, 0)`
  * `warn` turns on various warning messages. Defaults to `true`
"""
function readstatic(simgroup; time = (0.0, 0), warn::Bool = IOWARN[], version = get_version(simgroup))

    if version < v"0.2.0-alpha"
        # legacy import for InPartS v1 files (probably only works for CG models)
        @debug "Using legacy import functions for InPartS v0.1"
        return legacy_load(simgroup; time = time)
    elseif version < v"0.3.0-alpha"
        # legacy import for InPartS v2 files (type signature translator)
        @debug "Using legacy import functions for InPartS v0.2"
        return readstatic_v2(simgroup; time, warn)
    end

    dgroup = haskey(simgroup, DOMAINGROUPNAME) ? simgroup[DOMAINGROUPNAME] : simgroup["world"]
    domain = readdomain(dgroup; warn)

    pttype = readtype(simgroup[PTTYPENAME], super = AbstractParticle; warn)

    ptctype = try
        readtype(simgroup[PTCTYPENAME], super = AbstractParticleContainer; warn)
    catch e
        warn && @warn "Could not load ParticleContainer:$e\n Using SimpleParticleContainer\n"
        SimpleParticleContainer{ndims(domain), pttype}
    end

    pmtype = readtype(simgroup[PMTYPENAME], super = AbstractParams; warn)


    params = readparams(simgroup[PARAMGROUPNAME]; warn)
    if REGNAME in keys(simgroup)
        reg = AbstractRegistrar(readdict(simgroup[REGNAME]))
    else
        warn && @warn "No registrar was found in the file"
        reg = SimpleRegistrar{UInt64}()
    end
    rng = readrng(simgroup[RNGNAME]; complete = true, warn)
    ptc = ptctype()
    # obstacles & stuff go here
    if supportsobstacles(ptctype)
        obstacles = readobstacles(simgroup[OBSTACLEGROUPNAME]; warn)
        addobstacles!(ptc, obstacles)
    #else
    end
    sim = Simulation(ptc, pttype, pmtype, domain, time..., reg, rng)
    sim.params = params
    return sim
end


"""
    readdomain(group; [warn = IOWARN[]])
Reconstructs a [`Domain2D`](@ref) or [`Domain3D`](@ref) from `group`.
"""
function readdomain(group; warn = IOWARN[])
    size = group[DOMAINSIZENAME]
    boundarystrings = group[DOMAINBCNAME]
    boundaries = InPartS.reconstruct_subtype.(boundarystrings, Boundary)
    T = length(boundaries) == 3 ? Domain3D : Domain2D
    return T{boundaries...}(size)
end


"""
    readtype(pt; [super], [warn = IOWARN[]]) → T::Type
Reconstructs a type from a name as encoded with [`writetype`](@ref)

### Optional arguments
 * `super` limits the type reconstruction to children of a supertype. Defaults to `Any`
 * `mod` module in which to look for the type name. Defaults to `Main`
"""
function readtype(pt; super = Any, mod = Main, warn = IOWARN[])
    #pt = simgroup[name]
    if pt isa Vector
        return Union{InPartS.reconstruct_subtype.(pt, super; mod)...}
    else
        return InPartS.reconstruct_subtype(pt, super; mod)
    end
end


"""
    readdict(group; [maxdepth], [warn = IOWARN[]]) → d::Union{Missing, Dict{String, Any}}
Recursively reads `group` into a dictionary until depth `maxdepth`. If
`maxdepth` is reached, all data is replaced by `missing`

## Optional arguments
  * `maxdepth` Sets the maximum recursion depth. Defaults to 32.
"""
function readdict(group; maxdepth = 32, warn = IOWARN[])
    if maxdepth == 0
        warn && @warn "readdict: Maximum recursion depth reached"
        return missing
    end

    dict = Dict{String, Any}()
    for gname ∈ keys(group)
        if isgroup(group, gname)
            dict[gname] = readdict(group; maxdepth = maxdepth - 1)
        else
            dict[gname] = group[gname]
        end
    end

    return dict
end




"""
    readrng(group; [complete], [warn = IOWARN[]]) → rng::AbstractRNG
    readrng(group, T ; [complete], [warn = IOWARN[]]) → rng::T
Reconstructs the RNG from `group`. Optionally specifiy the RNG type.

## Optional Arguments
* `complete = true` try to completely reconstruct the RNG. If this is impossible, a warning will be printed.
* `mod` module in which to look for RNG type. Defaults to `InPartS.Random`
"""
readrng(group; mod = InPartS.Random, kwargs...) = readrng(group, InPartS.reconstruct_subtype(group["_type"], AbstractRNG; mod); kwargs...)

function readrng(group, T::Type{<:AbstractRNG}; complete::Bool = false, warn = IOWARN[])
    if complete
        if group["_rng_complete"]
            return _deserialize_rng_full(group, T; warn)
        else
            @warn "Attempted to do a full RNG deserialization without data, falling back to partial deserialization"
        end
    end
    return _deserialize_rng_partial(group, T; warn)
end




"""
    readparams(group; [warn = IOWARN[]]) → params
Reconstructs the parameter structs from `group`
"""
function readparams(group; warn = IOWARN[])
    indices = Vector{Int}()
    params = Vector{AbstractParams}()
    for n ∈ keys(group)
        paramindex = parse(Int, n)
        paramtype = InPartS.reconstruct_subtype(group[n]["_type"], AbstractParams)
        push!(indices, paramindex)
        push!(params, ExportUtils.deserialize(group[n], paramtype))
    end
    sp = sortperm(indices)
    return params[sp]
end


@deprecate readsnap(snapgroup; sim::Simulation) readsnap(snapgroup, sim)

"""
    readsnap(snapgroup, sim::Simulation; [warn = IOWARN[]]) → particles, time, step, next_id, rng[,...]
Loads the snapshot from `snapgroup`, using additional information from `sim`. Returns a tuple of
state information that can be applied to the simulation using [`setstate`]((@ref)
"""
readsnap(snapgroup, sim::Simulation; kwargs...) = _readsnap_common(snapgroup, sim; kwargs...)

# this function deals with particles, times, steps, registrars etc.
# and can be used when overwriting readsnap for custom PTCs
function _readsnap_common(snapgroup, sim::Simulation{PTC, PT}; warn = IOWARN[], ignorelist = SNAPDATANAMES) where {PTC, PT}
    time = snapgroup[SNAPTIMENAME]
    step = snapgroup[SNAPSTEPNAME]
    next_id = snapgroup[SNAPNEXTIDNAME]
    particles = PT[]

    snapnames = keys(snapgroup)
    for subname ∈ snapnames
        # bodge job
        if subname ∈ ignorelist
            continue
        end
        # get type and supplement type parameters from before InPartS 0.7.1 or which were added later
        type = legacytypeparameters(InPartS.reconstruct_subtype(subname, AbstractParticle))
        isconcretetype(type) || @warn "Importing particles of non-concrete type $type can yield wrong results. Perhaps a missing legacytypeparameters definition?"
        append!(particles, ExportUtils.deserialize(snapgroup[subname], type; sim = sim))
    end

    sort!(particles, by=x->x.id)

    rng = missing
    if RNGNAME ∈ snapnames
        rng = readrng(snapgroup[RNGNAME]; complete = true, warn)
    end

    return particles, time, step, next_id, rng
end


"""
    setstate!(sim::Simulation, state)
Sets the simulation state to `state`, which is a tuple of the form
returned by [`readsnap`](@ref)
"""
setstate!(sim::Simulation, state; kwargs...) = _setstate_common!(sim, state; kwargs...)

# this function deals with particles, times, steps, registrars etc.
# and can be used when overwriting setstate! for custom PTCs
function _setstate_common!(sim::Simulation, state; warn = IOWARN[])
    particles, time, step, next_id, rng = state
    setparticles!(sim.particles, particles)
    sim.t = time
    sim.step = step
    sim.registrar.next_id = next_id
    if ismissing(rng)
    	warn && @warn "Could not reconstruct rng"
    else
    	(sim.rng = rng)
    end
    return sim
end


"""
    readsnap!(sim::Simulation, f, snapname::String; [warn = IOWARN[]])
    readsnap!(sim::Simulation, f, snapid::Int; [warn = IOWARN[]])
    readsnap!(sim::Simulation, snapgroup; [warn = IOWARN[]])
Loads the snapshot data from `snapgroup` into `sim`.
"""
readsnap!(sim::Simulation, snapgroup; warn::Bool = IOWARN[]) = setstate!(sim, readsnap(snapgroup, sim; warn); warn)

function readsnap!(sim::Simulation, filename::String, snap; kwargs...)
    dfopen(filename) do f
        readsnap!(sim, f, snap; kwargs...)
    end
end

function readsnap!(sim::Simulation, f, snap; warn::Bool=IOWARN[])
    if length(keys(f)) == 1
        legacy_readsnap!(sim, f, snap; warn=warn)
    else
        readsnap!(sim, f[SNAPGROUPNAME][string(snap)]; warn)
    end
end


function numsnaps(f)
    ks = keys(f)
    if length(ks) == 1
        length(keys(f[only(ks)]["snapshots"]))
    else
        length(keys(f[SNAPGROUPNAME]))
    end
end
numsnaps(filename::String) = dfopen(numsnaps, filename)

## Stuff

snapname(i::Int) = string(i)

function nextsnapname(g)
    @warn "You really shouldn't be using this!"
    snaps = sort!(parse.(Int, keys(g)))
    return snaps[end] + 1
end

"""
    lastfullsnap(f)
Returns the index of the last full snapshot (including RNG) in the file `f`.
"""
function lastfullsnap(f)
    for n = (numsnaps(f)-1):-1:0
        if haskey(f[SNAPGROUPNAME][string(n)], "rng")
            return n
        end
    end
    return -1
end
lastfullsnap(filename::String) = dfopen(lastfullsnap, filename)



## RNG stuff

function _serialize_rng_partial!(group, @nospecialize rng::AbstractRNG; warn = IOWARN[])
    warn && @warn "Partial serialization not implemented for $(typeof(rng))"
    return
end

function _serialize_rng_full!(group, @nospecialize rng::AbstractRNG; warn = IOWARN[])
    warn && @warn "Full serialization not implemented for $(typeof(rng)), falling back to partial serialization"
    return _serialize_rng_partial!(group, rng; warn)
end

function _serialize_rng_partial!(group, rng::MersenneTwister; warn = IOWARN[])
    group["seed"] = rng.seed
    return
end

function _serialize_rng_full!(group, rng::MersenneTwister; warn = IOWARN[])
    _serialize_rng_partial!(group, rng)
    group["vals"] = rng.vals
    group["ints"] = string.(rng.ints)
    group["state"] = rng.state.val
    group["idxF"] = rng.idxF
    group["idxI"] = rng.idxI
    return
end


function _deserialize_rng_partial(group, @nospecialize T::Type{<:AbstractRNG}; warn = IOWARN[])
    warn && @warn "Partial deserialization not implemented for $(T), attemting to create new instance"
    return T()
end

function _deserialize_rng_partial(group, ::Type{Random.TaskLocalRNG})
    @warn "Loading a simulation that used a TaskLocalRNG, results may not be reproducible."
    Random.TaskLocalRNG()
end


function _deserialize_rng_full(group, @nospecialize T::Type{<:AbstractRNG}; warn = IOWARN[])
    warn && @warn "Full deserialization not implemented for $(T), falling back to partial deserialization"
    return _deserialize_rng_partial(group, T)
end

_deserialize_rng_partial(group, ::Type{MersenneTwister}; warn = IOWARN[]) = MersenneTwister(group["seed"])

function _deserialize_rng_full(group, ::Type{MersenneTwister}; warn = IOWARN[])
    rng = _deserialize_rng_partial(group, MersenneTwister)

    rng.vals = group["vals"]
    rng.ints = parse.(UInt128, group["ints"])
    rng.state = Random.DSFMT.DSFMT_state(group["state"])
    rng.idxF = group["idxF"]
    rng.idxI = group["idxI"]
    return rng
end


# Xoshiro

function _serialize_rng_full!(group, rng::Xoshiro; kwargs...)
    group["s0"] = rng.s0
    group["s1"] = rng.s1
    group["s2"] = rng.s2
    group["s3"] = rng.s3
    return
end

# no reason to keep the partial serialization stuff
_serialize_rng_partial!(group, rng::Xoshiro; kwargs...) = _serialize_rng_full!(group, rng; kwargs...)

_deserialize_rng_full(group, ::Type{Xoshiro}; kwargs...) = Xoshiro(
    group["s0"], group["s1"], group["s2"], group["s3"]
)

_deserialize_rng_partial(group, T::Type{Xoshiro}; kwargs...) = _serialize_rng_full!(group, T; kwargs...)



################################################################################
#                           Obstacle Serialization                             #
################################################################################


function writeobstacles(group,  sim::Simulation; warn = IOWARN[])
    sov = obstacles(sim)
    obsgroup = gcreate(group, OBSTACLEGROUPNAME)
    for (i, so) ∈ enumerate(sov)
        sogroup = gcreate(obsgroup, string(i))
        ExportUtils.serialize!(sogroup, so)
        sogroup["_type"] = string(typeof(so))
    end
end

function readobstacles(group; warn = IOWARN[])
    indices = Vector{Int}()
    params = Vector{AbstractObstacle}()
    for n ∈ keys(group)
        paramindex = parse(Int, n)
        paramtype = InPartS.reconstruct_subtype(group[n]["_type"], AbstractObstacle)
        push!(indices, paramindex)
        push!(params, ExportUtils.deserialize(group[n], paramtype))
    end
    sp = sortperm(indices)
    return params[sp]
end

################################################################################
#                           Just Write A Simulation                            #
################################################################################
"""
   write_sim(filename, sim; dumprng = true)

A function for whenever you just need to save the ~~world~~ simulation.

"""
function write_sim(filename, sim::Simulation; dumprng = true)
    FT = backend(filename)
    dfopen(FT, filename, "w") do f
        writestatic(f, sim)
        writesnap(f[SNAPGROUPNAME], sim; name = "0", dumprng)
        writefinal(f, sim)
        dfclose(f)
    end
    return
end
