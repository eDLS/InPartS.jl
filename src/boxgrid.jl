abstract type AbstractBoxing end

struct NoBoxing <: AbstractBoxing end
NoBoxing(::Simulation) = NoBoxing()

"""
    BoxGrid(p, w)
Internal container to pass around precomputed boxes
"""
struct BoxGrid{N, T, P} <: AbstractBoxing
    size::NTuple{N,T}
    boxes::Array{Vector{P}, N}
    neighborlist::Array{Vector{CartesianIndex{N}}, N}
    function BoxGrid(size::NTuple, boxes::Array, neighborlist)
        N = length(size)
        T = Float64
        @assert T == eltype(size)
        P = eltype(eltype(boxes))
        new{N, T, P}(size, boxes, neighborlist)
    end
end

BoxGrid{N, T, P}(sim::Simulation) where {N, T, P} = BoxGrid(sim)::BoxGrid{N, T, P}

function BoxGrid(sim::Simulation)
    P = particletype(sim)
    N = ndims(P)
    nboxes = Tuple(Int.(domainsize(sim) .÷ interactionrange(P, sim)))
    trivial = false
    if any(nboxes .== 0)
        @warn "Domain smaller than interaction range, initializing trivial boxgrid"
        nboxes = Tuple(fill(1, length(domainsize(sim))))
        trivial = true
    end
    lengths = Tuple(domainsize(sim) ./ nboxes)
    boxes = [ P[] for _ in CartesianIndices(nboxes)]

    neighborlist = map(CartesianIndices(boxes)) do idx
        nidx = trivial ? Vector{CartesianIndex{N}}() : compute_neighboring_boxes(idx, sim.domain, nboxes)
        if !(all(nidx .∈ Ref(CartesianIndices(boxes))))
            @warn "$idx has invalid neighbor $nidx"
        end
        nidx
    end
    if any(nboxes .< 3)
        @warn "BoxGrid: small domain, neighborlist may include duplicates. Cleaning up"
        remove_mutual_neighbors!(neighborlist)
    end
    BoxGrid(lengths, boxes, neighborlist)
end

BoxGrid(sim::Simulation, ::Type, ::Type) = BoxGrid(sim)

"""
    remove_mutual_neighbors!(neighborlist::Array{Vector{CartesianIndex{N}}, N})
Removes mutual neighbors from the `neighborlist`. 
Necessary for the `BoxGrid` to work properly when using a small domain with < 3 boxes in at least one dimension.
"""
function remove_mutual_neighbors!(neighborlist::Array{Vector{CartesianIndex{N}}, N}) where N
    for ci in CartesianIndices(neighborlist)
        for nci in neighborlist[ci]
            if ci in neighborlist[nci]
                filter!(!=(ci), neighborlist[nci])
            end
        end
    end
end

"""
    boxparticles!(bg::BoxGrid, partices)
Sorts the particles into boxes of the given `BoxGrid`.
"""
function boxparticles!(bg::BoxGrid{N}, particles) where {N}

    # Clear boxes before refilling
    for box in bg.boxes
        empty!(box)
    end

    # TODO: test whether n-D version is slower
    ci = CartesianIndices(bg.boxes)
    @inbounds for c in particles
        actual_index = CartesianIndex{N}(
            # HACK: this depends on (::SArray).data being a tuple
            @.(unsafe_trunc(Int, c.centerpos.data / (bg.size + 1e-12 ) + 1))
        )
        if actual_index in ci
            box = bg.boxes[actual_index]
            push!(box, c)
        else
            error("No suitable box found for a particle with position $(c.centerpos)")
        end
    end
    return nothing
end



"relative positions of upper right-hand neighbors on a 2D grid"
const relneighbors2D = (
    CartesianIndex( 0, 1),
    CartesianIndex( 1,-1),
    CartesianIndex( 1, 0),
    CartesianIndex( 1, 1),
)

"relative positions of upper right-hand neighbors on a 3D grid"
const relneighbors3D = (
    CartesianIndex( 0, 1, 0),
    CartesianIndex( 0, 0, 1),
    CartesianIndex( 1, 0, 0),
    CartesianIndex( 1, 0, 1),
    CartesianIndex( 1, 1, 0),
    CartesianIndex( 0, 1, 1),
    CartesianIndex( 1, 1, 1),
    CartesianIndex( 1, 0,-1),
    CartesianIndex( 1,-1, 0),
    CartesianIndex( 0,-1, 1),
    CartesianIndex( 1,-1, 1),
    CartesianIndex( 1, 1,-1),
    CartesianIndex( 1,-1,-1),
)


"""
    compute_neighboring_boxes(idx, d::AbstractDomain, nboxes)
Finds all upper right-hand neighbors of the given box.
"""
function compute_neighboring_boxes(idx, ::Domain2D{B1, B2}, nboxes) where {B1,B2}
    nlist = Set{CartesianIndex{2}}()
    for offs in relneighbors2D
        neighbor = idx + offs
        i, j = neighbor.I
        if B1 <: PeriodicBoundary
            i = mod1(i, nboxes[1])
        end
        if B2 <: PeriodicBoundary
            j = mod1(j, nboxes[2])
        end
        neighbor = CartesianIndex(i,j)
        if neighbor != idx && neighbor ∈ CartesianIndices(nboxes)
            push!(nlist, neighbor)
        end
    end
    collect(nlist)
end

function compute_neighboring_boxes(idx, ::Domain3D{B1, B2, B3}, nboxes) where {B1,B2, B3}
    nlist = Set{CartesianIndex{3}}()
    for offs in relneighbors3D
        neighbor = idx + offs
        i, j, k = neighbor.I
        if B1 <: PeriodicBoundary
            i = mod1(i, nboxes[1])
        end
        if B2 <: PeriodicBoundary
            j = mod1(j, nboxes[2])
        end
        if B3 <: PeriodicBoundary
            k = mod1(k, nboxes[3])
        end
        neighbor = CartesianIndex(i,j, k)
        if neighbor != idx && neighbor ∈ CartesianIndices(nboxes)
            push!(nlist, neighbor)
        end
    end
    collect(nlist)
end

"""
    relevant_boxes(so::AbstractObstacle, bg::BoxGrid, d::AbstractDomain)
Compute the boxes of `bg` that contains points less then one interaction radius away from `so`.

Returns a vector of indices.
"""
function relevant_boxes(so::AbstractObstacle, bg::BoxGrid, w::AbstractDomain{2}; kwargs...)
    #the interactionrange by which the polygon should be grown is mininum(bg.size)/2
    ip = interactionpolygon(so, w; growby=minimum(bg.size)/2, kwargs...)

    boxmat = dropdims(intersecting_boxes(ip, w, size(bg.boxes)), dims=3)
    return vec(CartesianIndices(bg.boxes))[vec(boxmat) .> 0]
end


function relevant_boxes(so::AbstractObstacle, bg::BoxGrid, w::AbstractDomain{3}; kwargs...)
    # HACK: this is not how it works.
    # well it is now.
    @warn "Automatic box detection for 3D obstacles is not supported. Override `relevant_boxes` for your obstacle to manually select boxes" maxlog=1
    return vec(CartesianIndices(bg.boxes))
end



## BoxGrid pairwise interaction computation

@forcedef pairwise_interactions(interaction::Function, sim::Simulation, bg::BoxGrid) =
    @forcefun pairwise_interactions(interaction, sim, bg, Val{isthreaded(sim)}())


"""
    pairwise_interactions(interaction::Function, sim::Simulation, bg::BoxGrid, ::Val{false})
Uses a [`BoxGrid`](@ref) to efficiently compute pairwise interactions.
`BoxGrid` structure is mutated in the sense that particles are sorted into their boxes.
"""
@forcedef function pairwise_interactions(interaction::F, sim::Simulation, bg::BoxGrid, ::Val{false}) where {F<:Function}
    # Type parameter F is not needed within the function but apparently causes the compiler to
    # to properly infer the type of the interaction function and hence not use runtime dispatch

    # For execution order preservation compared to multithreaded simulations the
    # boxes are iterated in the same striped manner.
    boxparticles!(bg, particles(sim))

    numstripes, numrows... = size(bg.boxes)

    # odd # stripes have to be dealt with separately to avoid race conditions in periodic systems
    (oddstripes = isodd(numstripes)) && (numstripes -= 1)

    rowit = CartesianIndices(numrows)

    for i = 1:2:numstripes
        @forcefun _process_stripe(i, rowit, interaction, sim, bg)
    end
    for i = 2:2:numstripes
        @forcefun _process_stripe(i, rowit, interaction, sim, bg)
    end

    if oddstripes
        @forcefun _process_stripe(numstripes+1, rowit, interaction, sim, bg)
    end

    return
end

"""
    pairwise_interactions(interaction::Function, sim::Simulation, bg::BoxGrid, ::Val{true})
The `Var(true)` argument requests multi-threaded interaction computation.

## Extended help

Multithreading is implemented by dividing the system into two sets of interleaved stripes (slabs in 3D)
of boxes, each stripe being one box wide.
This division ensures that each stripe is independent of all other stripes in same set.
Therefore, we can first compute interactions for all the stripes in the first set in parallel, synchronise,
then process the second set.

If the number of stripes is odd, the last stripe is processed seperately. This is required for periodic
systems, where the first and last stripe could potentially interact with each other, thus making parallel
force computation unsafe.
"""
@forcedef function pairwise_interactions(interaction::F, sim::Simulation, bg::BoxGrid, ::Val{true}) where {F<:Function}

    boxparticles!(bg, particles(sim))

    numstripes, numrows... = size(bg.boxes)

    # odd # stripes have to be dealt with separately to avoid race conditions in periodic systems
    (oddstripes = isodd(numstripes)) && (numstripes -= 1)

    rowit = CartesianIndices(numrows)

    Threads.@threads for i = 1:2:numstripes
        @forcefun _process_stripe(i, rowit, interaction, sim, bg)
    end
    Threads.@threads for i = 2:2:numstripes
        @forcefun _process_stripe(i, rowit, interaction, sim, bg)
    end

    if oddstripes
        @forcefun _process_stripe(numstripes+1, rowit, interaction, sim, bg)
    end

    return
end

# helper function for boxgrid pairwise_interactions
@forcedef function _process_stripe(i, rowit, interaction::F, sim, bg) where {F<:Function} # <- not sure if needed
    for j = rowit
        I = CartesianIndex(i,j)
        isempty(bg.boxes[I]) && continue
        @forcefun pairwise_interactions(interaction, sim, bg.boxes[I])
        for J ∈ bg.neighborlist[I]
            if !isempty(bg.boxes[J])
                @forcefun pairwise_interactions(interaction, sim, bg.boxes[I], bg.boxes[J])
            end
        end # for J ∈ neighbors
    end
end


@forcedef function pairwise_interactions(interaction::Function, sim::Simulation, ::NoBoxing)
    @forcefun pairwise_interactions(interaction, sim, particles(sim))
end

@forcedef function pairwise_interactions(interaction::Function, sim::Simulation,
                                         particles::AbstractVector{<: AbstractParticle})
    N = length(particles)
    for i = 1:N
        c1 = particles[i]
        for j = i+1:N
            @forcefun interaction(c1, particles[j], sim)
        end
    end
    nothing
end

@forcedef function pairwise_interactions(interaction::Function, sim::Simulation,
                                         particles1::AbstractVector{P},
                                         particles2::AbstractVector{P}) where {P <: AbstractParticle}
    for c1 in particles1, c2 in particles2
        @forcefun interaction(c1, c2, sim)
    end
    nothing
end
