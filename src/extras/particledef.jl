export @particledef

# was this ever a good idea? no. it wasn't.
macro particledef(ptname, fields)
    # 1. Generate all possible variations of the type names
    ptn, params = splitparams(ptname) # split into type name and parameters
    params = escapemap.(params) # escape params because hygiene is bad
    shortparams = simplifyparams(params) # type params without <:

    pmn = pname(ptn) # generate pm name (without params)

    ptn_fullparams = joinparams(ptn, params) # pm name without params
    ptn_shortparams = joinparams(ptn, shortparams) # pt name with params without <:
    pmn_fullparams = joinparams(pmn, params) # pm name without params
    pmn_shortparams = joinparams(pmn, shortparams) # pm name with params without <:


    # 2. process the fields
    ptfields, pmfields = map(x -> x.args, rmlines(fields).args)
    # add additional fields to particle type
    pushfirst!(ptfields, :(id::UInt64))
    push!(ptfields, :(params::$(pmn_shortparams)))

    ptfields = escapemap.(ptfields)
    pmfields = escapemap.(pmfields)


    # 3. generate kwarg-only constructors
    constructors = Expr[]
    #particle with inferred params
    push!(constructors, _kwdef(ptn, ptfields, params))
    # particle with explicit params
    isempty(params) || push!(constructors, _kwdef(ptn_shortparams, ptfields, params, stripfieldtypes = true))

    # param with inferred params
    push!(constructors, _kwdef(pmn, pmfields, params))
    # param with explicit params
    isempty(params) || push!(constructors, _kwdef(pmn_shortparams, pmfields, params, stripfieldtypes = true))


    # 3. create ParamType method
    functions = _ptypefunctions(ptn, pmn, ptn_shortparams, pmn_shortparams, params)


    # 4. create generic docstring for parameter type
    paramdocstring = """
        $pmn
    Automatically generated parameter container for [`$ptn`](@ref).
    """


    # 5. glue everything together
    expr = quote
        struct $(pmn_fullparams) <: AbstractParams
            $(pmfields...)
        end
        @doc $(paramdocstring) $(pmn)


        Base.@__doc__ mutable struct $(ptn_fullparams) <: AbstractParticle{2}
            $(ptfields...)
        end

        $(constructors...)

        $(functions...)
    end
    # debug
    #dump(expr, maxdepth = 20)
    return expr
end





function _ptypefunctions(ptn, pmn, shortptn, shortpmn, params = ())
    # generic ≡ without type params
    # special ≡ with type params

    functions = Expr[]

    # paramtype_generic
    push!(functions, _ptdef(
        :(InPartS.ParamType),
        ptn,
        pmn
    ))

    if params != ()
        # paramtype_special
        push!(functions, _ptdef(
            :(InPartS.ParamType),
            shortptn,
            shortpmn,
            params
        ))
    end

    return functions
end


# split a Name (typedef or function argument) into the actual name & a tuple of parameters
function splitparams(name::Expr)
    @assert name.head == :curly "not a name: $name"
    return name.args[1], Tuple(name.args[2:end])
end
splitparams(name::Symbol) = name, Tuple(())


# inverse function of joinpatams; joins an actual name and a parmeter tuple into a Name
joinparams(name::Symbol, params::Tuple) = :($name{$(params...)})
joinparams(name::Symbol, params::Tuple{}) = :($name)


# remove all <: from parameters (for function arguments)
simplifyparams(params::Tuple) = map(simplifyparams, params)
simplifyparams(param::Expr) = simplifyparams(param.args[1])
simplifyparams(param::Symbol) = param


# append a "Params" to the Name without breaking up the type params
function pname(expr::Expr)
     name, params = splitparams(expr)
     return :($(pname(name)){$(params...)})
end
pname(name::Symbol) = Symbol(string(name)*"Params")


# create ParamType definitions
_ptdef(name, shortname, bodyname, params = ()) = MacroTools.combinedef(Dict(
    :name => name,
    :args => [:(::Type{$(esc(shortname))})],
    :kwargs => [],
    :body => :($(esc(bodyname))),
    :whereparams => params
))


# great big monstrous all-encompassing autoescaper
function escapemap(expr::Expr)
    if expr.head == :(::)
        # type assertion: only escape type
        return Expr(:(::), expr.args[1], Expr(:escape, expr.args[2]))
    #elseif expr.head == :(=)
    #    # assignment: autoescape the assignment location (which could contain
    #    # type assertions in kwdef-style syntax) and escape the assigned expression
    #    return Expr(:(=), escapemap(expr.args[1]), Expr(:escape, expr.args[2]))
    elseif expr.head == :(<:) || expr.head == :curly
        # subtype assertion, curlies : fully escape both bits (else type parameters get all wibbly-wobbly)
        return Expr(expr.head, Expr(:escape, expr.args[1]), Expr(:escape, expr.args[2]))
    else
        return expr
    end
end
# ignore LineNumberNodes; escape stray Symbols (to be on the safe side)
escapemap(x::LineNumberNode) = x
escapemap(x::Symbol) = Expr(:escape, x)



function _kwdef(name, fields, params; stripfieldtypes = false)
    fields = fields[@. !isa(fields, LineNumberNode)]
    fieldnames = map(x->first(x.args), fields)
    stripfieldtypes && (fields = fieldnames)

    head = Expr(:where, Expr(:call, escapemap(name), Expr(:parameters, fields...)), params...)
    tail = Expr(:call, escapemap(name), fieldnames...)

    return :($head = $tail)
end
