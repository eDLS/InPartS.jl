
export addparticles!, addgrid!, autogrid!


"""
    FillVector{T}(entry, size)
Create a scalar that pretends it's a vector.

Behaves like an `AbstractVector{T}` of length `length`, but `getindex` returns `entry` for all
indices (including `i > length` because of acute lazyness.)
Can be used as a memory-efficient replacment of `fill` when horsing around with iterators
"""
struct FillVector{T} <: AbstractArray{T, 1}
    _entry::T
    _size::Int
end

Base.size(f::FillVector) = (f._size, )
Base.IndexStyle(::Type{<:FillVector}) = IndexLinear()
Base.getindex(f::FillVector, ::Int) = f._entry


function _assert_length(v::AbstractVector, n::Int)
    if length(v) ≥ n
        return @view v[1:n]
    else
        error("Array too short: Expected length $n or larger, got $(length(v))")
    end
end

_assert_length(v::T, n::Int) where T = FillVector{T}(v, n)




"""
    addparticles!(sim; centerpos::Vector{SVector}, kwargs...)
Like [`addparticle!`](@ref), but for multiple particles (hence the name).

Creates as many particles as `centerpos`es are specified.
For vector kwargs, the `i`th entries is used for constructing the `i`th particle.
All other kwargs are used for all particles.

## Extended help

### Usage example
```julia
addparticles(sim, centerpos = [SA[0.0, 0.0], SA[1.0, 1.0]], foo = [0.4, -4.5], bar = 5.0, baz = (1.0, 2.0))
```
creates two particles, one at `[0.0, 0.0]`, with `foo = 0.4`, `bar = 5.0` and `baz = (1.0, 2.0)`, and the other at
`[1.0, 1.0]`, with `foo = -4.5`, `bar = 5.0` and `baz = (1.0, 2.0)`.
"""
function addparticles!(sim::Simulation{<:AbstractParticleContainer{D}}; centerpos::Vector{<:SVector{D}}, kwargs...) where {D}

    nparticles = length(centerpos)
    parameters = (k=>_assert_length(v, nparticles) for (k,v) ∈ kwargs)

    for i ∈ eachindex(centerpos)
        addparticle!(sim; centerpos = centerpos[i], (k => v[i] for (k,v) ∈ parameters)...)
    end
    return nparticles
end



"""
    randorientation(::AbstractDomain{N}, [rng])
Draw a random equidistributed orientation.
For `N` = 2, the orientation is returned as an angle, for `N` = 3 it is returned as a unit vector.
"""
@inline randorientation(::AbstractDomain{2}, rng::Random.AbstractRNG = Random.default_rng()) = _randorientation_2D(rng)
@inline randorientation(::AbstractDomain{3}, rng::Random.AbstractRNG = Random.default_rng()) = _randorientation_3D(rng)

@inline _randorientation_2D(rng::Random.AbstractRNG) = 2π*rand(rng)
@inline _randorientation_3D(rng::Random.AbstractRNG) = let φ_over_π = 2rand(rng), θ = acos(2rand(rng) - 1)
    SA[sin(θ).*sincospi(φ_over_π)..., cos(θ)]
end



"""
    addgrid!(sim, lsize; distance, align, jitter, kwargs...)
Add particles on a `D`-dimensional Cartesian lattice.

The lattice extends for `lsize[i]` particles in the `i`th dimension, with a distance of
`distance[i]` in between particle `centerpos`s.
If `lsize[i]` is smaller than 1, the amount of cells will be chosen so that the lattice
fills the entire domain along the `i`th dimension, with a margin of `distance[i]/2`.

Grid points are jittered by adding random numbers drawn from the interval
`[-jitter[i]/2, jitter[i]/2]`.

`align` specifies the position of the grid in the domain. If `align[i]` is a number, the
`i`th coordinate of the grid centroid will be placed at `align[i]`.
If `align[i]` is either `:center`, `:centre`, `:start` or `end`, the grid will be aligned
with the center, start or end of the domain along the `i`th coordinate.

All lattice parameters (`lsize`, `distance`, `align`, `jitter`) can be specified either
as a `D`-vector or as a scalar. In the latter case, they are interpreted as isotropic.

"""
function addgrid!(
    sim::Simulation{<:AbstractParticleContainer{D}}, lsize = -1;
    distance = 1.0, align = :center, jitter = 0.0, kwargs...
) where D

    # convert arguments into standardised form
    distance = SVector{D}(_assert_length(distance, D))
    align = SVector{D}(_assert_length(align, D))
    jitter = SVector{D}(_assert_length(jitter, D))
    lsize = _compute_lsize(domainsize(sim), lsize, distance)

    return _addgrid!(sim, lsize, distance, align, jitter; kwargs...)
end


# helper function to convert the lsize argument into reasonable numbers
function _compute_lsize(dsize::SVector{N}, lsize::AbstractVector, distance::SVector{N}) where N
    max_cells_per_dim = floor.(Int64, dsize./distance)
    lsize = map(enumerate(lsize)) do (i, l)
        (l > 0) ? min(l, max_cells_per_dim[i]) : max_cells_per_dim[i]
    end

    if any(lsize .== 0)
        error("Domain to small for selected spacing, could not create particles.")
    end

    return SVector{N, Int64}(lsize)
end

_compute_lsize(dsize::SVector{D}, lsize, distance::SVector{D}) where D = _compute_lsize(dsize, FillVector(lsize, D), distance)


function _addgrid!(
    sim::Simulation{<:AbstractParticleContainer{D}},
    lsize,
    distance,
    align::SVector{D, <:Any},
    jitter;
    kwargs...
) where D

    # compute lattice bbox (including distance/2 margin on all sides)
    bboxcorner = @. (lsize)*distance/2

    # compute offset vector based on alignment specs
    dsize = domainsize(sim)

    offset = SVector{D, Float64}(
        map(enumerate(align)) do (i, al)
        if al isa Real
            return al
        elseif al ∈ [:centre, :center] # continent-agnostic spelling!
            return dsize[i]/2
        elseif al == :start
            return bboxcorner[i]
        elseif al == :end
            return dsize[i] - bboxcorner[i]
        else
            error("unknown alignment spec: $al")
        end
    end
    )

    return _addgrid!(sim, lsize, distance, offset, jitter; kwargs...)
end


function _addgrid!(
    sim::Simulation{<:AbstractParticleContainer{D}},
    lsize,
    distance,
    offset::SVector{D, <:Real},
    jitter;
    kwargs...
) where D

    # compute lattice points
    dpos = []
    for i ∈ 1:D
        firstpos = -((lsize[i]÷2)*distance[i] - (distance[i]*iseven(lsize[i]))/2) + offset[i]
        push!(dpos, range(firstpos, step = distance[i], length = lsize[i]))
    end
    latticepoints = Iterators.product(dpos...)

    # collect points & apply jitter
    kwargdict = Dict(kwargs)
    jrng = pop!(kwargdict, :_jitter_rng, Random.default_rng())
    points = [SVector(p) + jitter.*(rand(jrng, SVector{D, Float64}) .- 0.5) for p ∈ reshape(collect(latticepoints), :)]

    # add particles
    return addparticles!(sim; centerpos = points, kwargdict...)
end

"""
    minimumgridconstant(::Type{ParticleType}, p)
Defines the minimum grid constant for an ensemble of particles of type
`ParticleType`, i.e., the minimum distance between center positions
at which particles with parameter `p` can be safely placed.

Should be extended by models to enable `autogrid!` functionality.

"""
function minimumgridconstant end

"""
    defaultensembleinitialization!(vardict::Dict{Symbol, Any}, sim::Simulation{<:AbstractParticleContainer, PT}, params) where {PT}
Defines the default constructor arguments of particles for a vector of particle
parameters `params`. Used by `autogrid!`.

Should be extended by models to modify `vardict::Dict{Symbol, Any}` where each Symbol key
corresponds to a constructor argument of the particle type `PT` (except `centerpos` which is
set by `addgrid!`). `vardict[key]` defines the argument's value for all particles, and is
either a scalar (one value for all particles) or a vector of length `length(params)` for
per-particle values. The passed `vardict` already contains the user-defined variables passed
to `autogrid!` (which, by convention, should only be transformed/added to, not be overridden).

The passed `rng` should be used to perform any random initialization to ensure reproducibility.

The default implementation of `defaultensembleinitialization!` does not modify `vardict`.

"""
function defaultensembleinitialization!(vardict, sim, params, rng) return vardict end

"""
    autogrid!(sim, lsize, params, spacing = 1.0, rng = sim.rng, kwargs...)
Add particles arranged on an `N`-dimensional Cartesian lattice to `sim`.

This essentially calls addgrid!, but chooses `distance` as the maximum `minimumgridconstant`
for any of the specified parameters (multiplied by `spacing`) and initializes particles with
the model-defined `defaultgridinitialization` function.

`rng` is used as the `_jitter_rng` for `addgrid!` and passed to `defaultgridinitialization`.


"""
function autogrid!(sim::Simulation{<:AbstractParticleContainer{D}, PT}, lsize = -1;
    params,
    spacing = 1.0,
    rng::AbstractRNG = sim.rng,
    kwargs...
) where {D, PT}

    # HACK: there is a circular dependency here — I need an iterable version of params
    # to compute the maxcellextent, but I can't use _assert_length if I don't know
    # the number of cells (which unfortunately depends on maxcelllength…)
    # therefore, here have hack
    _params_it = params isa AbstractVector ? params : [params]

    # TODO: find some way to do this with interactionrange — this is stupid
    maxcellextent = maximum(_params_it) do p
        minimumgridconstant(PT, p)
    end

    distance = SVector{D}(_assert_length(spacing * maxcellextent, D))
    lsize = _compute_lsize(domainsize(sim), lsize, distance)
    ncells = prod(lsize)

    params = _assert_length(params, ncells)

    # set reasonable default values for undefined parameters
    kwdict = Dict{Symbol, Any}(kwargs)
    defaultensembleinitialization!(kwdict, sim, params, rng)

    addgrid!(sim, lsize; distance, params, _jitter_rng = rng, kwdict...)
end