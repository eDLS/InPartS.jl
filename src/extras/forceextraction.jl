
using MacroTools
export @force, @forcedef, @forcefun, forcecontribution


"""
    @force expr
Decorator for force accumulation.

Inside a method decorated with [`@forcedef`](@ref), decorating an expression with `@force`
indicates that the result of the expression should be pushed to the accumulator.

Outside of `@forcedef`-methods, `expr` is not evaluated.

See [`@forcedef`](@ref) for usage examples.
"""
macro force(ex) end



"""
    @forcefun
Decorator for force accumulation.

Inside a method decorated with [`@forcedef`](@ref), decorating a function
call with `@forcefun` indicates that the accumulator should be passed to this function.

Outside of `@forcedef`-methods, this macro has no effect and the function call is evaluated
as usual.

See [`@forcedef`](@ref) for usage examples.
"""
macro forcefun(ex)
    return esc(ex)
end



"""
    @forcedef [function definition]
Function decorator for force extraction.

Function definitions decorated with `forcedef` are expanded into two methods,
the latter of which contains an additional first positional argument `acc`, which can be used
to accumulate values.

All expressions decorated with [`@force`](@ref) are pushed into the accumulator.
The accumulator is added as the first positional argument to all function calls decorated with `@forcefun`


# Example

The function definition
```
@forcedef function examplefunction(x1; x2 = 0.1)
    dx = @forcefun otherfunction(x1 - x2)
    @force dx
    return dx^2
end
```
is expanded to
```
function examplefunction(x1; x2 = 0.1)
    dx = otherfunction(x1 - x2)
    return dx^2
end

function examplefunction(acc, x1; x2 = 0.1)
    dx = otherfunction(acc, x1 - x2)
    push!(acc, dx)
    return dx^2
end
```


"""
macro forcedef(ex)
    d = splitdef(ex)
    d2 = splitdef(ex)
    acc = gensym(:accumulator)
    pushfirst!(d[:args], acc)

    d[:body] = MacroTools.postwalk(d[:body]) do ex
        # replace all `@forcefun foo(args...[; kwargs...])` with `@forcefun foo(acc, args...[; kwargs...])`
        @capture(ex, @forcefun foo_(args__; kwargs__)) && return :($(foo)($acc, $(args...); $(kwargs...)))
        @capture(ex, @forcefun foo_(args__)) && return :($(foo)($acc, $(args...)))

        # replace all `@force` blocks with `push!(acc, content_of_block)`
        @capture(ex, @force arg__) && return :(push!($(acc), $(arg...)))
        return ex
    end
    d2[:body] = MacroTools.postwalk(d2[:body]) do ex
        # remove all `@force` blocks
        @capture(ex, @force args__) && return nothing
        return ex
    end

    quote
        Base.@__doc__ $(esc(MacroTools.combinedef(d2)))
        $(esc(MacroTools.combinedef(d)))
    end
end

"""
    forcecontribution(c::SVector{2, T}, f::SVector{2, T}, δ::SVector{2, T}) where {T}
    forcecontribution(c::SVector{3, T}, f::SVector{3, T}, δ::SVector{3, T}) where {T}
Returns a named tuple containing the individual components of `c`, `f` and `δ`.

In the 2D case, the names of the tuple entries are `cx`, `cy`, `fx`, `fy` and `δx`, `δy`;
in 3D, the additional entries are called `cz`, `fz` and `δz` respectively

Useful for force accumulation using [`@forcedef`](@ref).
"""
function forcecontribution(c::SVector{2, T}, f::SVector{2, T}, δ::SVector{2, T}) where {T}
    return (
        cx = c[1], cy = c[2],
        fx = f[1], fy = f[2],
        δx = δ[1], δy = δ[2]
    )
end

function forcecontribution(c::SVector{3, T}, f::SVector{3, T}, δ::SVector{3, T}) where {T}
    return (
        cx = c[1], cy = c[2], cz = c[3],
        fx = f[1], fy = f[2], fz = f[3],
        δx = δ[1], δy = δ[2], δz = δ[3]
    )
end
