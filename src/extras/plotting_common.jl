

"""
    prettylims(x1, y1, x2, y2; [div = 9])
Computes pretty axis limits for the bounding box defined by `x1, y1` and `x2, y2`
using [`prettymargin`](@ref).
To get larger limits, make `div` smaller.
"""
function prettylims(x1, y1, x2, y2; kwargs...)
    dl = prettymargin(abs(x2 - x1), abs(y2 - y1); kwargs...)
    return (; xlims = (x1 - dl, x2 + dl), ylims = (y1 - dl, y2 + dl))
end

"""
    prettyfigsize!(ax, maxdim; [div = 9])
Sets a pretty(ish) figure size matching ax so that the figure is at most `maxdim`
wide/tall/whatever using [`prettymargin`](@ref).
"""
function prettyfigsize!(ax, maxdim; kwargs...)
    dx = -1 * -(ax.get_xlim()...)
    dy = -1 * -(ax.get_ylim()...)
    ax.get_figure().set_size_inches(prettyfigsize(maxdim, dx, dy; kwargs...))
    return
end

function prettyfigsize(maxdim, dx, dy; kwargs...)
    dl = prettymargin(dx, dy; kwargs...)
    dims = (dx + 2dl, dy+ 2dl)
    return @. dims/max(dims...) * maxdim
end

"""
    prettymargin(dx, dy; [div = 9]) = min(dx, dy)/div
Computes a pretty margin width for a box of lengths `dx` and `dy`
"""
prettymargin(dx, dy; div = 9) = min(dx, dy)/div


"""
    extend(v::AbstractVector, n) -> vnew::AbstractVector
Copies and extends an `AbstractVector` to length n by repeating its elements.
"""
function extend(v::AbstractVector{T}, n::Integer) where {T}
    m = length(v)
    vnew = Vector{T}(undef, n)
    for i ∈ 1:n
        vnew[i] = v[mod1(i, m)]
    end
    return vnew
end

extend(v, n::Integer) = v

maskcolor(v::AbstractVector, mask::BitArray) = v[mask]
maskcolor(v, mask::BitArray) = v

"""
    get_offsets(w::Domain)
Helper function to determine the offset vectors used to plot periodic copies of
the system.
"""
get_offsets(w::Domain2D{<:Boundary,<:Boundary}) = []
get_offsets(w::Domain2D{<:PeriodicBoundary,<:Boundary}) = [SA[-w.size[1], 0], SA[w.size[1], 0]]
get_offsets(w::Domain2D{<:Boundary,<:PeriodicBoundary}) = [SA[0, -w.size[2]], SA[0, w.size[2]]]

get_offsets(w::Domain2D{<:PeriodicBoundary, <:PeriodicBoundary}) = [
    SA[-w.size[1], -w.size[2]],
    SA[-w.size[1], 0],
    SA[-w.size[1], w.size[2]],
    SA[0, -w.size[2]],
    #SA[0, 0],
    SA[0, w.size[2]],
    SA[w.size[1], -w.size[2]],
    SA[w.size[1], 0],
    SA[w.size[1], w.size[2]]
]


"""
    color_by_lineage(reg::LineageRegistrar, maxdepth = 5)
Returns a function to color particles by lineage.

Traces back particle lineage to `maxdepth` generations, chooses a distinct hue
for every "family"
"""
function color_by_lineage(reg::LineageRegistrar{T}, maxdepth=5) where {T<:Integer}

    ancestorcolors = Dict{T, NTuple{3, Float64}}()
    hue = 0

    function color(cell::AbstractParticle)
        # find oldest known ancestor
        id = oldestancestor(cell.id, reg; maxdepth = maxdepth)
        col = get!(ancestorcolors, id) do
            hue += Base.MathConstants.φ
            hue %= 1
            return Tuple(m_color.hsv_to_rgb((hue, 0.8, 0.95)))
        end

        return col
    end

    return color
end
