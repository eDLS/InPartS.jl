# TODO: unfortunate type name
struct PolyDomain2D{T}
    ncols::Int
    nrows::Int
    margin::Tuple{Int,Int}
    xlim::Tuple{T, T}
    ylim::Tuple{T, T}
    xinc::T
    yinc::T
    igrid::Matrix{Bool}
    modcols::Vector{Bool}
    function PolyDomain2D(ncols, nrows, margin, xlim::Tuple{T,T}, ylim::Tuple{T,T}) where T
        new{T}(ncols, nrows, tuple(margin...), xlim, ylim, (xlim[2]-xlim[1])/ncols, (ylim[2]-ylim[1])/nrows,
                zeros(Bool, ncols, nrows), zeros(Bool, ncols))
    end
end
box_area(dm::PolyDomain2D) = dm.xinc * dm. yinc

"""
    intersecting_boxes(poly::AbstractMatrix, w::Domain, samples)
Rasterises the polygon with a resolution of `samples` using an algorithm derived
from B. J. Larkin, Computers & Geosciences 14, 1-14 (1988).

"""
function intersecting_boxes(
    poly,#Matrix
    w::Domain2D{BCX, BCY},
    samples,
    ) where {BCX, BCY}
    wsize = w.size
    inc = w.size ./ samples
    # Check if polygon lies outside the domain
    exts = extrema(poly, dims=1)
    xmin, xmax = exts[1]
    ymin, ymax = exts[2]

    imin = floor(Int, xmin / nextfloat(w.size[1])*samples[1])
    imax = floor(Int, xmax / prevfloat(w.size[1])*samples[1])
    jmin = floor(Int, ymin / nextfloat(w.size[2])*samples[2])
    jmax = floor(Int, ymax / prevfloat(w.size[2])*samples[2])

    margin = (max(1 - imin + 1, imax - samples[1] + 1, 1),
              max(1 - jmin + 1, jmax - samples[2] + 1, 1))

    xlim = (-margin[1]*inc[1], wsize[1] + margin[1]*inc[1])
    ylim = (-margin[2]*inc[2], wsize[2] + margin[2]*inc[2])
    ncols, nrows = samples .+ 2 .* margin

    dm = PolyDomain2D(ncols, nrows, margin, xlim, ylim)

    overlap = zeros(UInt8, ncols, nrows)

    verts = map(axes(poly, 1)) do n
        SVector{2, Float64}(poly[n, 1], poly[n, 2])
    end
    #verts = convert(Vector{SVector{2,Float64}}, poly)
    # Caculate partial grid cells for the including polygon
    part(overlap, verts, dm)

    # fill in the nonpartial gridcells
    #area = box_area(dm)
    #TODO: Check iteration order in whole algorithm
    @inbounds for icol = 1:dm.ncols
        dm.modcols[icol] || continue
        in = false
        for irow = 1:dm.nrows
            in = xor(dm.igrid[icol, irow] ,in)
            if in
                overlap[icol, irow] = true
            end
        end
    end
    if BCX <: PeriodicBoundary && BCY <: PeriodicBoundary
        return fold_x_cut_y_margin(overlap, dm)
    elseif BCX <: Boundary && BCY <: Boundary
        return cut_x_cut_y_margin(overlap, dm)
    else
        throw(ArgumentError("intersecting_boxes not implemented fo $BCX, $BCY"))
    end
end

function cut_x_cut_y_margin(field, dm)
    margin = dm.margin
    return field[(margin[1]+1):(end-margin[1]), (margin[2]+1):(end-margin[2]), :]
end

function fold_x_cut_y_margin(field, dm)
    margin = dm.margin
    figsize = size(dm.igrid) .- 2 .*margin
    #Copy left margin to the right
    margincols = 1:margin[1]
    targetcols = figsize[1] .+ margincols
    @views field[targetcols, :, :] .+= field[margincols, :, :]

    # copy right margin to the left
    targetcols = (margin[1]+1):2margin[1]
    margincols = figsize[1] .+ targetcols
    @views field[targetcols, :, :] .+= field[margincols, :, :]

    #selection
    return field[(margin[1]+1):(end-margin[1]), (margin[2]+1):(end-margin[2]), :]
end



"""
    part(xmin, xinc, ymin, yinc, grid, igrid, mcol, mrow, xvert, yvert, nvert, okdir)
Calculate grid cell areas partially contained within a given polygon
xmin, xinc  easting minimum and grid increment
ymin, yinc northing minimum and grid increment
mcol, mrow maximum no. of allocated grid columns and rows
xvert, yvert co-coordinates of the polygon
nvert   number of vertices in the polygon
okdir polygon vertices are in normal order (true/false)

returns
    grid     areas of grid cells partially in the polygon
    igrid    flag of grid cells partially in the polygon
"""
function part(overlap, vert, dm)
    xmin = dm.xlim[1]
    ymin = dm.ylim[1]
    xinc = dm.xinc
    yinc = dm.yinc
    igrid = dm.igrid
    nvert = length(vert)

    # Initialize end point of preceeding polygon edge
    #ist = 1
    #iend = nvert
    #idir = 1
    ist = nvert
    iend = 1
    idir = -1


    x2, y2 = vert[iend]
    ix2 = round(Int, (x2 - xmin) / xinc, RoundToZero) + 1
    iy2 = round(Int, (y2 - ymin) / yinc, RoundToZero) + 1

    # Calculate areas of partial cells along each of the polygon edges

    for ivert = ist:idir:iend
        x1 = x2
        y1 = y2
        ix1 = ix2
        iy1 = iy2
        x2, y2 = vert[ivert]
        ix2 = round(Int, (x2 - xmin) / xinc, RoundToZero) + 1
        iy2 = round(Int, (y2 - ymin) / yinc, RoundToZero) + 1

        # Calculate inverse gradient, x-intercept and flag to determine if included
        # area is to the left (iright =-1) or to the right (iright = 1) of the
        # edge, these items are only required if y2 != y1

        if y2 != y1
            xgrad = (x2 - x1) / (y2 - y1)
            xint = x1 - xgrad * y1
            if y2 > y1
                iright = 1
            else
                iright = -1
            end
        else
            # Have to define the variables even though they will not be needed
            # (multiplied by zero)
            xgrad = 0.0
            xint = 0.0
            iright = 1
        end

        # Calculate gradient, y-intercep and flag to determine if included area
        # is below (ibelow = 1) or above (ibelow = -1) the edge

        if x2 != x1
            ygrad = (y2 - y1) / (x2 - x1)
            yint = y1 - ygrad * x1
            if x2 > x1
                ibelow = 1
            else
                ibelow = -1
            end
        else
            ibelow = 1
        end

        # Initialize end points of preceeding column
        yend = y1
        iyend = iy1
        # for each column crossed by the polygon edge calculate the areas of the
        # partial cells

        for ix = ix1:ibelow:ix2
            yst = yend
            iyst = iyend
            if ix == ix2
                yend = y2
                iyend = iy2
            else
                xend = xmin + (ix + (ibelow -1)/2) *xinc
                yend = ygrad * xend + yint
                iyend = round(Int, (yend - ymin) / yinc, RoundToZero) + 1
            end

            # indicate that this grid cell has an inside / outside change
            # and calculate the area of the rectangle below the intersection of
            # the polygon edge and the right hand edge of the cell
            if x2 > x1
                if ix != ix2
                    dm.modcols[ix] = true
                    igrid[ix, iyend] = !(igrid[ix, iyend])
                    #yrec = yend - ymin - (iyend -1) * yinc
                    #field[ix, iyend] += ibelow * xinc * yrec * val
                    overlap[ix, iyend] += 0x01#ibelow * xinc * yrec
                end
            elseif x2 < x1
                if ix != ix1
                    dm.modcols[ix] = true
                    igrid[ix, iyst] = !(igrid[ix, iyst])
                    #yrec = yst - ymin - (iyst -1) * yinc
                    #field[ix, iyst] += ibelow * xinc * yrec * val
                    overlap[ix, iyst] += 0x01#ibelow * xinc * yrec

                end
            end

            # For each row within this column, calculate area of the trapezium to
            # the left of the polygon edge

            yy = yst
            #xx = xgrad * yy + xint
            #xleft = xmin + (ix-1) * xinc

            for iy = iyst:iright:iyend
                #sy = yy
                # if iy == iyend
                #     yy = yend
                # else
                #     yy = ymin + (iy + (iright -1) / 2) * yinc
                # end
                #sx = xx
                #xx = xgrad * yy + xint
                # the expression (y - sy) determines if an addition, subtraction
                # or no operation is required
                #field[ix,iy] -= (yy - sy) * (0.5 * (xx + sx) - xleft) * val
                overlap[ix,iy] += 0x01# (yy - sy) * (0.5 * (xx + sx) - xleft)
            end
        end
    end
    return nothing
end
