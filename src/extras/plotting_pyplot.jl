#=
WARNING: declaring PyObjects as global constants is very volatile and only works
in this case because this file is loaded dynamically using Requires.
If this code ever gets moved to another location (say, a dedicated plotting module),
this MUST be changed.
=#
const m_patch = PyPlot.matplotlib.patches
const m_colls = PyPlot.matplotlib.collections
const m_color = PyPlot.matplotlib.colors
const cconv = m_color.colorConverter


"""
    plot(w::Vector{<:AbstractParticle}, w::Domain2D; ax = gca(), kwargs...)
Plots a system of particles and a domain2d on the given axis.

Uses [`plot(::AbstractVector{<:AbstractParticle})`](@ref) and
[`plot(::Domain2D)`](@ref).

## Optional parameters
 * `color: a color specification either as a single color or a vector of
   colors. If an vector is given, the particle colors are chosen cyclically from
   the vector. Note that all opacity information is overridden by
  `alpha`/`periodicalpha`. Defaults to `"C0"`.
 * `alpha`: the opacity of the particles. Defaults to `0.7`.
 * `periodic`: if `true`, plots periodic copies of the system with different
   opacity and line style. Defaults to `true`.
 * `periodicalpha`: the opacities of particles in the periodic copies. Defaults
   to `true`.
 * `periodicls`: the line style of particles in the periodic copies. Defaults to
   `":"`.
"""
function plot(
    sim::Simulation;
    ax = PyPlot.gca(),
    color = "C0",
    alpha = 0.7,
    periodic = true,
    periodicalpha = 0.5*alpha,
    periodicls = ":",
    autolims = true,
    kwargs...
)
    # Axis setup
    ax.set_aspect("equal")
    if autolims
        xlim, ylim = prettylims(0.0, 0.0, domainsize(sim)...)
        ax.set_xlim(xlim...)
        ax.set_ylim(ylim...)
    end

    # extend color vector to match particle vector
    color = extend(color, length(particles(sim)))
    plot(particles(sim); ax = ax, color = cconv.to_rgba.(color; alpha = alpha), kwargs...)
    if supportsobstacles(typeof(sim.particles))
        plot(obstacles(sim); ax=ax)
    end
    plot(sim.domain; ax = ax)

    if periodic
        # create color list
        faded_color = cconv.to_rgba.(color; alpha = periodicalpha)
        for offset ∈ get_offsets(sim.domain)
            plot(particles(sim); ax = ax, color = faded_color, ls = periodicls,
                 offset = offset, kwargs...)
        end
    end
end


"""
    plot(w::Vector{<:AbstractParticle}; ax = gca(), kwargs...)
Plots a vector of `AbstractParticle` onto the given axis, respecting their
[`PlotStyle`](@ref) if necessary.

This fallback implementation plots the particle center positions.

## Optional parameters
 * `world`: specify a world to extract addtional information about the particle.
   Not used in fallback implementation
 * `offset`: a 2D offset vector to apply to each plotting coordinate.

Other parameters are passed to [`PyPlot.scatter`](@ref).
"""
function plot(ps::AbstractVector{PT}; color = "C0", kwargs...) where {PT<:AbstractParticle}
    if !(PT isa Union)
        plot(PlotStyle(PT), ps; color = color, kwargs...)
        return
    end

    types, masks = maskbytype(ps)

    for (T, mask) ∈ zip(types, masks)
        plot(PlotStyle(T), Vector{T}(@view(ps[mask])); color = maskcolor(color, mask), kwargs...)
    end
    return
end


function plot(::IsPyPlottable, ps::AbstractVector{<:AbstractParticle};
              ax = PyPlot.gca(), offset = SA[0.0, 0.0], ec = "k", kwargs...)
    positions = vec(map(p->p.centerpos + offset, ps))
    ax.scatter(first.(positions), last.(positions); ec = ec, kwargs...)
end


function plot(pt::IsPolyPlottable, ps::AbstractVector{<:AbstractParticle};
              ax = PyPlot.gca(), offset = SA[0, 0], ec = "k", color = "C0",
              polykwargs = NamedTuple(), kwargs...)

    polygons = map(p->getpolygon(p; polykwargs...) .+ (offset'), ps)
    system = m_colls.PolyCollection(polygons, closed=true, facecolors = color,
                                    zorder=100, ec = ec; kwargs...)
    ax.add_collection(system)
end




"""
    plot(w::Domain2D; ax = gca(), kwargs...)
Plots the boundaries of a `Domain2D` on a given axis. Solid walls are
drawn with solid lines, periodic boundary conditions are drawn with dashed
lines.

Keyword arguments:
 * `c` color of the boundary lines. Defaults to "C2".
 * `lw` linedwidth of the boundary lines. Defaults to 1.
"""
function plot(w::Domain2D; ax = PyPlot.gca(), c = "C2", lw = 1)

    dashedlines = []
    solidlines = []
    bcs = getboundaries(w)

    # y is periodic -> x-lines dashed
    linetypex = (bcs[2] == PeriodicBoundary ? dashedlines : solidlines)
    push!(linetypex, [(0.0, 0.0), (w.size[1], 0.0)])
    push!(linetypex, [(0.0, w.size[2]), (w.size[1], w.size[2])])
    linetypey = (bcs[1] == PeriodicBoundary ? dashedlines : solidlines)
    push!(linetypey, [(0.0, 0.0), (0.0, w.size[2])])
    push!(linetypey, [(w.size[1], 0.0), (w.size[1], w.size[2])])

    line_dashed = m_colls.LineCollection(dashedlines, colors = c, linestyles="--",
                                         linewidths = lw)
    line_solid = m_colls.LineCollection(solidlines, colors = c, linestyles="-",
                                       linewidths = lw)

    ax.add_collection(line_dashed)
    ax.add_collection(line_solid)
end


"""
    callback_plot(;ax = gca(), kwargs...)
Returns a callback to plot the system onto the given axis.

## Optional parameters
 * `colorfunction`: This function can be used to map particles to their colors.
   The default implementation maps every particle to `"C0"`.
 * `savedir`: If set, frames are saved to the specified directory in the
   specified format.
 * `format`: Image format for export. Defaults to `"png"`.
 * `sleeptime`: Time to sleep after drawing a frame, can be necessary to ensure
   that `PyPlot` redraws the GUI. Defaults to `0.01`. A `NaN` value removes the
   call to `sleep` altogether.

Other parameters are passed to
 [`plot(::AbstractVector{<:AbstractParticle}, ::Domain2D)`](@ref).
"""
function callback_plot(;colorfunction::Function = p -> "C0",
                       savedir=nothing, format = "png", savedpi = 100,
                       sleeptime = 0.01, ax = PyPlot.gca(), kwargs...)

    f = ax.get_figure()
    idx = 0

    function callback(sim)
        ax.clear()
        color = colorfunction.(particles(sim))
        plot(sim; ax = ax, color = color, kwargs...)
        isnothing(savedir) || f.savefig(joinpath(savedir, @sprintf("frame%05d.", idx)*format), dpi = savedpi)
        idx += 1
        !isnan(sleeptime) && sleep(sleeptime)
        return nothing
    end

    return callback
end

_stdobstaclecolor(o::OT) where OT<:AbstractObstacle = (hasfield(OT, :inverted) && o.inverted == true) ? "white" : "tab:gray"

function plot(sov::AbstractVector{<:AbstractObstacle}; ax=gca(), polykwargs=NamedTuple(),
    offset = SA[0.0, 0.0], color=_stdobstaclecolor.(sov), kwargs... )
    #for so in sim.particles.sov
    polygons = map(p->getpolygon(p; polykwargs...) .+ (offset'), sov)
    facecolors = _assert_length(color, length(sov))
    system = m_colls.PolyCollection(polygons; closed=true, facecolors = facecolors,
                              zorder=-10 , kwargs...)
    ax.add_collection(system)
end


# function PyPlot.plot(
#     sim::Simulation{<:InPartS.POLOContainer};
#     ax = PyPlot.gca(),
#     color = "C0",
#     alpha = 0.7,
#     periodic = true,
#     periodicalpha = 0.5*alpha,
#     periodicls = ":",
#     autolims = true,
#     kwargs...
# )
#     # Axis setup
#     ax.set_aspect("equal")
#     if autolims
#         xlim, ylim = prettylims(0.0, 0.0, domainsize(sim)...)
#         ax.set_xlim(xlim...)
#         ax.set_ylim(ylim...)
#     end
#     # extend color vector to match particle vector
#     color = extend(color, length(particles(sim)))
#     plot(particles(sim); ax = ax, color = cconv.to_rgba.(color; alpha = alpha), kwargs...)
#     if supportsobstacles(typeof(sim.particles))
#         plot(obstacles(sim); ax=ax)
#     end
#     for lo in sim.particles.lov
#         plot([lo]; ax = ax, color="gray", kwargs...)
#     end
#     plot(sim.domain; ax = ax)

#     if periodic
#         # create color list
#         faded_color = cconv.to_rgba.(color; alpha = periodicalpha)
#         for offset ∈ get_offsets(sim.domain)
#             plot(particles(sim); ax = ax, color = faded_color, ls = periodicls,
#                     offset = offset, kwargs...)
#         end
#     end
# end
