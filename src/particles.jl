
# Particle API docstrings —
@nospecialize
"""
    step!(p::AbstractParticle, dt::AbstractFloat, sim::Simulation)
Advances the state of the particle by `dt` time units.

In general, this function should use the precomputed forces (see
[`internalforces!`](@ref) and [`externalforces!`](@ref)) to advance the
particle state.
"""
@api_undef step!(p::AbstractParticle, dt, s::Simulation)


"""
    reset!(p::AbstractParticle, sim::Simulation)
Clears the precomputed forces and prepares the particle for the next time step.

This function is generally called after every time step.
"""
@api_undef reset!(p::AbstractParticle, sim::Simulation)


# """
#     issane(p::AbstractParticle, w::World) → Boolean
# Returns true if the particle is in a "sane" state.
#
# This function has a fallback method definition that always returns false to
# prevent the usage of incompatible particle and world models.
# """
# issane(p::AbstractParticle, sim::Simulation) = false


"""
    maxvelocity(::Type{<:AbstractParticle}, sim::Simulation) → AbstractFloat
Returns an estimate of the maximum velocity for the particle vector.

Required for using adaptive timesteps (see [`evolve!`](@ref)). The return value
 is used to compute the length of the next time step.
"""
@api_undef maxvelocity(t::Type{AbstractParticle}, s::Simulation)


"""
    interactionrange(::Type{<:AbstractParticle}, sim::Simulation) → AbstractFloat
Returns the maximum range of pairwise interactions.

Required for using interaction boxing. If the maximum length is only known
heuristically, make sure to notify users whenever the heuristic is broken, as
this could potentially affect the dynamics.
"""
@api_undef interactionrange(t::Type{AbstractParticle}, sim::Simulation)

@specialize

"""
    internalforces!(p::AbstractParticle, sim::Simulation)
Computes the forces due to particle-internal interactions.

Can be used to compute forces that do not depend on the state of other particles
 (e.g. cellular growth or self-propulsion). In general, one should not use this
 function for time-dependent forces, as the length of the next time step is not
 known at the time the functions is called. Things like random forces are
 easier integrated in  [`step!`](@ref). For pairwise interactions,
 use [`externalforces!`](@ref)

!!! note "Keep track of forces"
    Forces should be stored within the particle struct. Any values returned by this
    function are discarded.
"""
@forcedef function internalforces!(p::AbstractParticle, s::Simulation) end



"""
    externalforces!(p1::AbstractParticle, p2::AbstractParticle, sim::Simulation)
Computes the forces due to pairwise particle interactions.

Can be used to compute pairwise interactions between particles, e.g. steric
 repulsion or electrostatic attraction. For implementing explicitely
time-dependent interactions, the same caveats as specified in [`internalforces!`](@ref)
apply.

!!! note "Keep track of forces"
    Forces should be stored within the particle struct. Any values returned by this
    function are discarded.
    The function is called only once for every pair of particles, so make sure to
    store its return values in both structs!
"""
function externalforces!(p1::AbstractParticle, p2::AbstractParticle, s::Simulation) end
