using Printf
import Base.∘
using Serialization: Serialization

export CallbackList, EveryStepCallback, TimedCallback, PeriodicCallback, RealTimeCallback,
    BurstModeCallback, ExponentialBackoffCallback, SignalHandlerCallback

"""
    prepropagate!(cb::AbstractCallback, state::DynamicState)
Execute the pre-`propagate`-hook for callback `cb`.

`prepropagate!` is the first callback hook in the main loop, called after force computation,
but before `dt` is set and [`propagate!`](@ref) is called.
The simulation state at this point is likely to be consistent (i.e. the particle configuration
is "physical" and the forces match the configuration), but your mileage may vary depending
on the model.
Use this hook e.g. to add additional forces, to save/plot/… your simulation.

Like all callback hooks, the return value of `prepropagate!` can interrupt integration
(see [`should_terminate`](@ref) and [`CallbackList`](@ref) for return value semantics)
"""
function prepropagate!(::AbstractCallback, ::DynamicState) end

"""
    postpropagate!(cb::AbstractCallback, state::DynamicState)
Executes the post-`propagate`-hook for callback `cb`

`postpropagate!` is the second callback hook in the main loop, called after [`propagate!`](@ref) returns
and the simulation time is updated.
This is the last step in the main loop, so this hook can also be used to access the system
pre-force-computation — the next timestep's [`computeforces!`](@ref) is called immediately after
`postpropagate!`.
Most applications will probably prefer [`prepropagate!`](@ref), but this hook has its applications e.g.
in controlling particle numbers.

Like all callback hooks, the return value of `postpropagate!` can interrupt integration
(see [`should_terminate`](@ref) and [`CallbackList`](@ref) for return value semantics).
"""
function postpropagate!(::AbstractCallback, ::DynamicState) end

"""
    finalize!(cb <: AbstractCallback, state::DynamicState)
Performs the last rites after the simulation has termianted successfully.

Define for a callback if it needs to do something after the end of the simulation.
Default implementation does nothing.
This hook is called only if the simulation exit code is 0.
For handling "unsuccessful" termination, use [`freeze!`](@ref).
"""
function finalize!(::AbstractCallback, ::DynamicState) end


"""
    freeze!(cb <: AbstractCallback)
Freezes a callback for later [`defrost!`](@ref)ing.

This function is called on a callback when a simulation exits with an non-zero exit code,
e.g. when it is interrupted due to a timeout before reaching `tmax`
The semantic difference to `finalize!`
The default implementation does nothing, wh
"""
function freeze!(::AbstractCallback, ::DynamicState) end

"""
    defrost!(cb <: AbstractCallback)
Define for custom callback if it needs modification when restarting an interrupted simulation.
Counterpart to [`freeze!`](@ref).

The primary intended use case is to update timestamps in e.g. RealTimeCallback.
"""
function defrost!(::AbstractCallback) end


"""
    should_terminate(rc)
Parses callback return code and returns `true`` if the simulation should be interrupted.
Will return `false` if `rc` is either a zero-value integer or `nothing`.
"""
should_terminate(::Nothing)  = false
should_terminate(i::Integer) = i != 0



## CALLBACK LIBARY
# There are three kinds of callbacks in the following section:
#    i. utility: NoCallback, CallbackList. These have some sort of "structural" role
#       (representing the absence of callbacks/chaining together callbacks) and don't
#       do anything by themselves
#   ii. generic: PeriodicCallback etc. These also don't do anything but have specialized
#       stateful timing logic and execute user-defined functions
#  iii. specialized: these are specialized to a certain task and can only be used for that
#       task. Some specialized callbacks related to data export can be found in /src/export/callbacks.jl


## I. UTILITY CALLBACKS

"""
    NoCallback() <: AbstractCallback
Dummy callback that is used when no real callback was passed.
"""
struct NoCallback <: AbstractCallback end



"""
    CallbackList(args...) <: AbstractCallback
Returns a callback that combines all callbacks passed as arguments and executes them in that
order. Note that CallbackList uses short-circuiting and will immediately stop execution on
encountering any unsuccessful exit codes, without calling the remaining callbacks.

See [`should_terminate`](@ref) for callback exit code semantics.

CallbackLists can also be constructed using the ∘ operator, where `cb1 ∘ cb2` means `cb1` is
executed *after* `cb2`.

Nested CallbackLists will automatically be flattened on construction, all [`NoCallback`](@ref)s
will be trimmed to save weight.
"""
struct CallbackList{T} <: AbstractCallback
    callbacks::T
end

CallbackList(callbacks::Vararg{AbstractCallback}) = CallbackList((flatten_callbacks(callbacks)...,))

function flatten_callbacks(callbacks)
    v = Vector{AbstractCallback}()
    for cb in callbacks
        if cb isa CallbackList
            append!(v, flatten_callbacks(cb.callbacks))
        elseif !(cb isa NoCallback) # cut out the NoCallbacks
            push!(v, cb)
        end
    end
    return v
end

# syntactic sugar — make sure order of execution is correct (i.e. last callback is executed last)
∘(cb1::AbstractCallback, cb2::AbstractCallback, cbs::Vararg{AbstractCallback}) = CallbackList(reverse(cbs)..., cb2, cb1)

function prepropagate!(cbs::CallbackList, state::DynamicState)
    for cb in cbs.callbacks
        ret = prepropagate!(cb, state)
        if should_terminate(ret)
            return ret
        end
    end
    return nothing
end

function postpropagate!(cbs::CallbackList, state::DynamicState)
    for cb in cbs.callbacks
        ret = postpropagate!(cb, state)
        if should_terminate(ret)
            return ret
        end
    end
    return nothing
end

function finalize!(cbs::CallbackList, state::DynamicState)
    for cb in cbs.callbacks
        finalize!(cb, state)
    end
    return
end

function freeze!(cbs::CallbackList, state::DynamicState)
    for cb in cbs.callbacks
        freeze!(cb, state)
    end
    return
end

function defrost!(cbs::CallbackList, state::DynamicState)
    for cb in cbs.callbacks
        defrost!(cb, state)
    end
    return
end



## II. GENERIC CALLBACKS

# these need a special logic to pass through either the Simulation or the DynamicState
# to the user-provided affect functions (in order to avoid breaking existing scripts that
# don't know about simulations but also allowing pro users to get access to EVERYTHING)
const StateOrSimType = Union{Type{<:DynamicState}, Type{<:Simulation}}
_state_or_sim(::Type{<:DynamicState}, state::DynamicState) = state
_state_or_sim(::Type{<:Simulation}, state::DynamicState) = state.sim

# this could be used to implement more clever like stuff (e.g. method table lookups)
_default_sost(fallback::StateOrSimType, functions...) = fallback


"""
    EveryStepCallback(affect, finalize)
A generic callback that calls the function `affect` on the simulation state in every
[`prepropagate!`](@ref) hook. Calls `finalize` on successfull termination of the simulation
(see [`finalize!`](@ref)).
"""
struct EveryStepCallback{S, F1, F2} <: AbstractCallback
    affect::F1
    finalize::F2

    EveryStepCallback(
        f::F1; finalize::F2 = Returns(nothing), argtype::StateOrSimType = _default_sost(Simulation, f, finalize)
    ) where {F1, F2} = new{argtype, F1, F2}(f, finalize)
end

prepropagate!(cb::EveryStepCallback{S}, state::DynamicState) where S= cb.affect(_state_or_sim(S, state))

finalize!(cb::EveryStepCallback{S}, state::DynamicState) where S = cb.finalize(_state_or_sim(S, state))



"""
    TimedCallback(affect, executiontimes; offset=0.0, initialize, finalize)
A generic callback that calls the function `affect` on the simulation state in a
[`prepropagate!`](@ref) hook whenever the simulation time is larger than one of
the points specified in `executiontimes`.
`executiontimes` must be indexable and sorted in ascending order (multiple identical
 elements are fine).
 The kwarg `offset` optionally shifts all execution times by the specified amount,
 `initialize` and `finalize` functions are called when the callback is first executed
  and on successfull termination of the simulation respectively.
"""
mutable struct TimedCallback{T, U, S, F1, F2, F3} <: AbstractCallback
    offset::T
    executiontimes::U
    counter::Int
    initialize::F1
    affect::F2
    finalize::F3

    function TimedCallback(
        f::F2,
        executiontimes::U;
        offset::T=zero(eltype(U)),
        initialize::F1 = Returns(nothing),
        finalize::F3 = Returns(nothing),
        argtype::StateOrSimType = _default_sost(Simulation, f, initialize, finalize)
    ) where {T <:Number, U, F1, F2, F3}

        new{T, U, argtype, F1, F2, F3}(offset, executiontimes, -1, initialize, f, finalize)
    end
end

function prepropagate!(cb::TimedCallback{T, U, S}, state::DynamicState) where {T, U, S}
    if cb.counter < 0
        cb.initialize(_state_or_sim(S, state))
        cb.counter = 1
    end

    retcode::Int = 0
    while retcode == 0 && cb.counter ≤ length(cb.executiontimes) && (current_time(state.sim) - cb.offset) > cb.executiontimes[cb.counter]
        retcode += should_terminate(cb.affect(_state_or_sim(S, state)))
        cb.counter += 1
    end
    return retcode
end

finalize!(cb::TimedCallback{T, U, S}, state::DynamicState) where {T, U, S} = cb.finalize(_state_or_sim(S, state))



"""
    PeriodicCallback(affect, interval; [offset=0.0, initialize, finalize])
Returns a callback that executes `affect` on the [`prepropagate!`](@ref) hook
every `interval` simulation time units, **starting at `offset + interval`**.
`initialize` and `finalize` functions are called when the callback is first executed
and on successfull termination of the simulation respectively.
"""
mutable struct PeriodicCallback{T, S, F1, F2, F3} <: AbstractCallback
    offset::T
    interval::T
    counter::Int
    initialize::F1
    affect::F2
    finalize::F3

    function PeriodicCallback(
        f::F2,  interval::T;
        offset=zero(T),
        initialize::F1 = Returns(nothing),
        finalize::F3 = Returns(nothing),
        argtype::StateOrSimType = _default_sost(Simulation, f, initialize, finalize),
    ) where {T <:Number, F1, F2, F3}

        new{T, argtype, F1, F2, F3}(offset, interval, -1, initialize, f, finalize)
    end
end

function prepropagate!(cb::PeriodicCallback{T, S}, state::DynamicState) where {T, S}
    if cb.counter < 0
        cb.initialize(_state_or_sim(S, state))
        cb.counter = max(floor((current_time(state.sim) - cb.offset)/cb.interval), 0)
    end

    if floor((current_time(state.sim) - cb.offset)/cb.interval) ≤ cb.counter
        return
    end
    cb.counter += 1
    cb.affect(_state_or_sim(S, state))
end

finalize!(cb::PeriodicCallback{T, S}, state::DynamicState) where {T, S} = cb.finalize(_state_or_sim(S, state))



"""
        RealTimeCallback(interval, affect!; [offset=0.0, reset_on_freeze = false, initialize, finalize])
Returns a callback that executes `affect` on the [`prepropagate!`](@ref) hook
every `interval` seconds system time, **starting at `offset` seconds after frst execution**.
`initialize` and `finalize` functions are called when the callback is first executed
and on successfull termination of the simulation respectively.

The behaviour of the timer on [`freeze!`](@ref) and subsequent [`defrost!`](@ref) is set by the
`reset_on_freeze` kwarg: If set to `false` (the default option), the callback will remember how
many seconds were left until the next `affect` call and resume where it left off after defrosting.
If set to `true`, the timer is completely reset and will again wait for `offset` seconds after
defrosting.
"""
mutable struct RealTimeCallback{S, F1, F2, F3} <: AbstractCallback
    offset::Float64
    interval::Float64
    _next::Float64
    reset_on_freeze::Bool
    initialize::F1
    affect::F2
    finalize::F3

    function RealTimeCallback(
        f::F2, interval::Real;
        offset=0.0,
        reset_on_freeze = true,
        initialize::F1 = Returns(nothing),
        finalize::F3 = Returns(nothing),
        argtype::StateOrSimType = _default_sost(Simulation, f, initialize, finalize)
    ) where {F1, F2, F3}

        new{argtype, F1, F2, F3}(offset, Float64(interval), NaN, reset_on_freeze, initialize, f, finalize)
    end
end

function prepropagate!(cb::RealTimeCallback{S}, state::DynamicState) where S
    triggertime = time()
    if isnan(cb._next)
        cb.initialize(_state_or_sim(S, state))
        cb._next = time() + cb.offset
    end

    if triggertime < cb._next
        return
    end

    cb._next += cb.interval
    cb.affect(_state_or_sim(S, state))
end

finalize!(cb::RealTimeCallback{S}, state::DynamicState) where S = cb.finalize(_state_or_sim(S, state))

function freeze!(cb::RealTimeCallback)
    if !cb.reset_on_freeze
        # We save the relative offset as a negative number so it's easy for defrost to identify that we did it
        cb._next -= time()
        cb._next *= -1
    end
    return
end

function defrost!(cb::RealTimeCallback)
    # if cb._next is positive that means the callback wasn't properly frozen so we have to reset
    if !cb.reset_on_freeze && cb._next < 0
        cb._next = time() - cb._next
    else
        # reset timer
        cb._next = NaN
    end
    return nothing;
end



"""
    BurstModeCallback(; interval, affect, maxcount=10, trigger_offset=0.0, trigger_interval=10.0) <: AbstractCallback
Returns a callback that executes `affect` every interval time units for a total of
`maxcount` iterations after it has been triggered by a `PeriodicCallback` that is
initialized with `trigger_offset` and `trigger_interval`.

## Example
A callback created with

    BurstModeCallback(; interval=0.1,
                            affect=printcurrenttime,
                            maxcount=3,)

where `printcurrenttime` does what you think it does, will, during a simulation,
print `0.0, 0.1, 0.2, 10.0, 10.1, 10.2, 20.0, ...`
"""
mutable struct BurstModeCallback{T, S, F1, F2, F3, CB} <: AbstractCallback
    counter::Int
    maxcount::Int
    interval::T
    offset::T
    trigger::CB
    initialize::F1
    affect::F2
    finalize::F3
end


function BurstModeCallback(
    affect::F2,
    interval::T,
    trigger::CB;
    maxcount::Int = 10,
    initialize::F1 = Returns(nothing),
    finalize::F3 = Returns(nothing),
    argtype::StateOrSimType = _default_sost(Simulation, affect, initialize, finalize)
) where {T, F1, F2, F3, CB<:AbstractCallback}

    BurstModeCallback{T, argtype, F1, F2, F3, CB}(
        -2, maxcount, interval, zero(T), trigger, initialize, affect, finalize)
end


function BurstModeCallback(;
    interval,
    affect,
    maxcount = 10,
    initialize = Returns(nothing),
    finalize = Returns(nothing),
    trigger_offset = 0.0,
    trigger_interval = 10.0,
    argtype::StateOrSimType = _default_sost(Simulation, affect, initialize, finalize)
)

    BurstModeCallback(
        affect,
        interval,
        PeriodicCallback(trigger_interval, Returns(true); offset=trigger_offset);
        maxcount,
        initialize,
        finalize,
        argtype
    )
end



function prepropagate!(cb::BurstModeCallback{T, S}, state::DynamicState) where {T, S}
    if cb.counter == -2
        cb.initialize(_state_or_sim(S, state))
        cb.counter = -1
    end

    if cb.counter == -1
        if prepropagate!(cb.trigger, state)==true
            cb.counter = 0
            cb.offset = current_time(state.sim)
        end
    end

    ret = nothing
    if cb.counter > -1
        if floor((current_time(state.sim) - cb.offset)/cb.interval) ≤ cb.counter
            return ret
        end
        cb.counter += 1
        ret = cb.affect(_state_or_sim(S, state))
    end

    if cb.counter == cb.maxcount
        cb.counter = -1
    end
    return ret
end

finalize!(cb::BurstModeCallback{T, S}, state::DynamicState; kwargs...) where {T, S} = cb.finalize(_state_or_sim(S, state); kwargs...)



"""
    ExponentialBackoffCallback(; kwargs...) <: AbstractCallback
Returns a callback that triggers on first execution and then every `interval` seconds.
Where `interval` is multiplied by `factor` after each execution, but never exceeds `max_interval`.

Starts back at `start_interval` after `defrost!` is called.

## keyword arguments
- `start_interval`: initial interval in seconds
- `max_interval=Inf`: maximum interval in seconds
- `factor=2`: factor by which the interval is multiplied after each execution
- `affect`: function to execute
- `interrupt=Returns(nothing)`: function to execute when simulation is interrupted
- `finalize=Returns(nothing)`: function to execute when simulation is finalized

`affect`, `interrupt`, and `finalize` must accept an `InPartS.DynamicState` as argument.
"""
mutable struct ExponentialBackoffCallback{S, F1, F2, F3} <: AbstractCallback
    start_interval::Float64
    max_interval::Float64
    factor::Float64

    affect::F1
    interrupt::F2
    finalize::F3

    next_trigger::Float64
    interval::Float64

    function ExponentialBackoffCallback(
        start_interval::Real,
        max_interval::Real,
        factor::Real,
        affect::F1,
        interrupt::F2,
        finalize::F3,
        argtype::StateOrSimType
    ) where {F1, F2, F3}

        @assert factor ≥ 1.0 "Backoff factor must be ≥ 1"
        @assert start_interval ≥ 0.0 "Start interval must be ≥ 0"
        @assert max_interval ≥ 0.0 "Max interval must be ≥ 0"
        @assert hasmethod(affect, Tuple{argtype}) "affect must accept a $argtype as argument"
        @assert hasmethod(interrupt, Tuple{argtype}) "interrupt must accept a $argtype  as argument"
        @assert hasmethod(finalize, Tuple{argtype}) "finalize must accept a $argtype as argument"
        new{argtype, F1, F2, F3}(start_interval, max_interval, factor, affect, interrupt, finalize, 0.0, start_interval)
    end
end

function ExponentialBackoffCallback(;
    start_interval,
    affect,
    max_interval = Inf,
    factor = 2.0,
    interrupt = Returns(nothing),
    finalize = Returns(nothing),
    argtype =  _default_sost(Simulation, affect, interrupt, finalize)
)

    ExponentialBackoffCallback(
        start_interval,
        max_interval,
        factor,
        affect,
        interrupt,
        finalize,
        argtype
    )
end

ExponentialBackoffCallback(affect; kwargs...) = ExponentialBackoffCallback(; affect, kwargs...)

function prepropagate!(cb::ExponentialBackoffCallback{S}, state::DynamicState) where S
    triggertime = time()
    (triggertime < cb.next_trigger) && return nothing

    cb.next_trigger = triggertime + cb.interval
    cb.interval = min(cb.interval * cb.factor, cb.max_interval)
    return cb.affect(_state_or_sim(S, state))
end

finalize!(cb::ExponentialBackoffCallback{S}, state::DynamicState) where S = cb.finalize(_state_or_sim(S, state))
freeze!(  cb::ExponentialBackoffCallback{S}, state::DynamicState) where S = cb.interrupt(_state_or_sim(S, state))

function defrost!(cb::ExponentialBackoffCallback)
    cb.next_trigger = 0.0 # Trigger on first execution
    cb.interval = cb.start_interval
    return nothing
end



## SPECIALIZED CALLBACKS



"""
    SignalHandlerCallback(affect, signal::Int) <: AbstractCallback
Returns a callback that executes `affect` when the specified operating system
signal is received. Can be used for graceful termination of simulations when interrupted.
Requires `using InterProcessCommunication.jl` to be available. Most likely only works on
POSIX systems. See [InterProcessCommunication.jl](https://github.com/emmt/InterProcessCommunication.jl)

## Example
    cb = InPartS.SignalHandlerCallback(Returns(99), 2)
This callback will end the simulation with return code `99` when the process receives a
`SIGINT` (i.e. when you press `Ctrl-C`, note that `2` refers to `SIGINT`).
"""
function SignalHandlerCallback(affect, signal)
    ext = Base.get_extension(InPartS, :InterProcessCommunicationExt)
    if isnothing(ext)
        error("InterProcessCommunicationExt.jl is not loaded. Please load it before using SignalHandlerCallback.")
    end
    ext.SignalHandlerCallback(affect, signal)
end
