module JLD2Ext

using InPartS
@static isdefined(Base, :get_extension) ? (using JLD2) : (using ..JLD2)

const JLD2Obj = Union{JLD2.JLDFile, JLD2.Group}

struct JLD2Wrapper{T<:JLD2Obj} <: AbstractDict{String, Any}
    _handle::T
end

function Base.getindex(j2w::JLD2Wrapper, key)
    item = j2w._handle[key]
    return item isa JLD2Obj ? JLD2Wrapper(item) : item
end

Base.get(j2w::JLD2Wrapper, key, default) = haskey(j2w, key) ? j2w[key] : default

Base.setindex!(j2w::JLD2Wrapper, val, key) = setindex!(j2w._handle, val, key)
# For backwards compatibility with HDF5 write all SArrays as Arrays
Base.setindex!(j2w::JLD2Wrapper, value::SArray, key::String) = setindex!(j2w, collect(value), key)
# use writedict for recursive dicts
Base.setindex!(j2w::JLD2Wrapper, value::Dict{String, Any}, key::String) = InPartS.writedict(j2w, value; name = key)

Base.keys(j2w::JLD2Wrapper) = keys(j2w._handle)
Base.haskey(j2w::JLD2Wrapper, k) = haskey(j2w._handle, k)

# InPartS IO Backend API
InPartS._backend(::Val{:JLD2}) = JLD2Wrapper

function InPartS.dfopen(::Type{<:JLD2Wrapper}, filename::String, mode::String = "r"; disable_commit=true, kwargs...)
    f = JLD2Wrapper(JLD2.jldopen(filename, mode; kwargs...))
    f._handle.disable_commit = disable_commit
    f
end
InPartS.dfclose(j2w::JLD2Wrapper) = close(j2w._handle)
Base.close(j2w::JLD2Wrapper{<:JLD2.JLDFile}) = close(j2w._handle) # for lazy people

function InPartS.dfrename(::Type{JLD2Wrapper}, oldname, newname; force = false)
    cp(oldname, newname; force = force)
    rm(oldname)
end

InPartS.gcreate(g::JLD2Wrapper, name::String; kwargs...) = JLD2Wrapper(JLD2.Group(g._handle, name; kwargs...))

InPartS.isgroup(g::JLD2Wrapper, key) = (g[key] isa JLD2Wrapper{<:JLD2.Group})
Base.length(g::JLD2Wrapper) = length(keys(g._handle))

Base.show(io::IO, mime::MIME{Symbol("text/plain")}, g::JLD2Wrapper) = (print(io, "JLD2Wrapper: "); show(io, mime,g._handle))

# more efficient readdict
# copied and modified from previous JLD2.loadtodict!(Dict{String,Any}(), g)
# to preserve nestedness
# is faster than generic version in generic.jl because reading via `g[k]`
# only happens once
function InPartS.readdict(g::JLD2Wrapper; kwargs...)
    d = Dict{String, Any}()
    for k in keys(g)
        v = g[k]
        if v isa JLD2Wrapper
            d[k] = InPartS.readdict(v)
        else
            d[k] = v
        end
    end
    return d
end



# legacy stuff
InPartS.dfopen(::Type{<:JLD2.JLDFile}, args...; kwargs...) = InPartS.dfopen(JLD2Wrapper, args...; kwargs...)
InPartS.dfclose(df::JLD2.JLDFile) = InPartS.dfclose(JLD2Wrapper(df))
InPartS.dfrename(df::Type{<:JLD2.JLDFile}, args...; kwargs...) = InPartS.dfrename(JLD2Wrapper, args...; kwargs...)
 # fully automatic wrapping
for fun ∈ [:readdict, :readdomain, :readobstacles, :readparams, :readrng, :readsim, :readsnap, :readstatic, :readtype, :numsnaps, :lastfullsnap]
    @eval InPartS.$(fun)(d::JLD2Obj, args...; kwargs...) = InPartS.$(fun)(JLD2Wrapper(d), args...; kwargs...)
end

InPartS.readsnap!(sim::Simulation, df::JLD2.JLDFile, args...; kwargs...) = InPartS.readsnap!(sim::Simulation, JLD2Wrapper(df), args...; kwargs...)

end
