module HDF5Ext

using InPartS
@static isdefined(Base, :get_extension) ? (using HDF5) : (using ..HDF5)



struct H5Wrapper{T<:Union{HDF5.File, HDF5.Group}} <: AbstractDict{String, Any}
    handle::T
end

InPartS._backend(::Val{:HDF5}) = H5Wrapper


"""
    H5Wrapper(filename; [kwargs...]) → H5Wrapper{HDF5.File}
    H5Wrapper(filename, mode::String) → H5Wrapper{HDF5.File}
Creates a new H5Wrapper from the given path. The file mode can be specified as
kwargs or using a modes string (compare [`Base.open`](@extref))
"""
H5Wrapper(filename::String; kwargs...) = H5Wrapper{HDF5.File}(h5open(filename, h5mode(; kwargs...)))
H5Wrapper(filename::String, mode::String) = H5Wrapper{HDF5.File}(h5open(filename, h5mode(; modetoflags(mode)...)))


function h5mode(;kwargs...)
    # some Base flags aren't meaningful for HDF5, so warn if they are explicitely set
    #haskey(kwargs, :append) && @warn "Ignoring append flag for HDF5"
    #get(kwargs, :read, true) || @warn "Ignoring read = false for HDF5"

    flags = Base.open_flags(;kwargs...)

    if flags.write && flags.create && flags.truncate
        return "w"
    elseif flags.write && flags.create
        return "cw"
    elseif flags.write
        return "r+"
    else
        return "r"
    end
end

# dictionary-like access
function Base.getindex(h5w::H5Wrapper, key::String)
    item = h5w.handle[key]
    return InPartS.isgroup(h5w, key) ? H5Wrapper(item) : read(item)
end
Base.setindex!(h5w::H5Wrapper, value, key::String) = setindex!(h5w.handle, value, key)
Base.setindex!(h5w::H5Wrapper, value::Dict{String, Any}, key::String) = InPartS.writedict(h5w, value; name = key)
# workaround because StaticArrays doesn't implement Base.strides…
Base.setindex!(h5w::H5Wrapper, value::SArray, key::String) = setindex!(h5w.handle, collect(value), key)

Base.get(h5w::H5Wrapper, key::String, default) = haskey(h5w.handle, key) ? h5w[key] : default

# InPartS IO Backend API
InPartS.dfopen(::Type{H5Wrapper}, name::String; flags...) = H5Wrapper(name; flags...)
InPartS.dfclose(h5w::H5Wrapper{HDF5.File}) = close(h5w.handle)
function InPartS.dfrename(::Type{H5Wrapper}, oldname, newname; force = false)
    cp(oldname, newname; force = force)
    rm(oldname)
end

InPartS.gcreate(h5w::H5Wrapper, name::String; kwargs...) = H5Wrapper(HDF5.create_group(h5w.handle, name))
Base.keys(h5w::H5Wrapper) = keys(h5w.handle)
Base.haskey(h5w::H5Wrapper, k::AbstractString) = haskey(h5w.handle, k)

InPartS.isgroup(h5w::H5Wrapper, key) = !(h5w.handle[key] isa HDF5.Dataset)
Base.length(h5w::H5Wrapper) = length(h5w.handle)



# more efficient readdict
InPartS.readdict(h5w::H5Wrapper; kwargs...) = read(h5w.handle)

Base.show(io::IO, mime::MIME{Symbol("text/plain")}, g::H5Wrapper) = (print(io, "H5Wrapper: "); show(io, mime, g.handle))

end
