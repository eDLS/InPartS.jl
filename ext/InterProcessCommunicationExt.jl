module InterProcessCommunicationExt

using InPartS: InPartS
using InterProcessCommunication

mutable struct SignalHandlerCallback{F} <: InPartS.AbstractCallback
    const signal::Int
    received::Bool
    isinitialized::Bool
    const affect::F
    handler::Base.CFunction # needed to keep it from being garbage collected
    sigact_old::SigAction # needed to restore old signal handler in finalizer
    function SignalHandlerCallback(affect::F, signal::Int) where {F}
        new{F}(signal, 0, false, affect)
    end
end

Base.show(io::IO, cb::SignalHandlerCallback) =
    print(io, "SignalHandlerCallback($(cb.signal), $(cb.received), $(cb.isinitialized), $(cb.affect))")

function InPartS.prepropagate!(cb::SignalHandlerCallback, state::InPartS.DynamicState)
    if !cb.isinitialized

        # Julia blocks many signals by default. need to unblock them
        spm = SigSet()
        spm[cb.signal] = true
        sigprocmask(IPC.SIG_UNBLOCK, spm)

        cb.handler = @cfunction $(signum->(cb.received=true; nothing)) Cvoid (Cint,)
        sigact = SigAction(Base.unsafe_convert(Ptr{Cvoid}, cb.handler), SigSet(), 0)
        cb.sigact_old=sigaction(cb.signal)
        sigaction(cb.signal, sigact)
        cb.isinitialized = true
    end
    if cb.received
        cb.received = false
        return cb.affect(state)
    end
    return nothing
end

function InPartS.finalize!(cb::SignalHandlerCallback, ::InPartS.DynamicState)
    # always finalize. ignore state.finalize flag
    sigaction(cb.signal, cb.sigact_old)

    if cb.signal in (2,10,15)
        spm = SigSet()
        spm[cb.signal] = true
        sigprocmask(IPC.SIG_BLOCK, spm)
    end

    cb.isinitialized = false
    return nothing
end

InPartS.freeze!(cb::SignalHandlerCallback, state::InPartS.DynamicState) =
    InPartS.finalize!(cb, state)

function InPartS.defrost!(cb::SignalHandlerCallback)
    cb.isinitialized = false
    cb.received = false
end


end
