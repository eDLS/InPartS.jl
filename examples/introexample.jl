using Pkg
Pkg.activate(@__DIR__)

# this loads the basic InPartS package…
using InPartS
# …and this exports all the internal names to make model development easier
using InPartS.InPartSDev

using StaticArrays

# a bunch of useful Julia stdlib packages
using LinearAlgebra, Random



## 1. Model definition
#=
  In InPartS, a model is represented by objects of two types:
      - particles (<:AbstractParticle) contain the dynamic variables, forces, and particle-specific properties
      - parameters (<:AbstractParams) contain static, shared parameters
  Every particle has an associated parameters struct. Multiple particles can share the same parameters.
=#

# parameters
struct MyParticleParams <: AbstractParams
    mass::Float64
    radius::Float64
    propulsion::Float64
    rotdiffusion::Float64
    fluidviscosity::Float64
    temperature::Float64
end

# all InPartS objects need to have a kwarg-only constructor
MyParticleParams(;mass = 1.0, radius = 0.5, propulsion = 1.0, rotdiffusion = 1.0, fluidviscosity = 1.0, temperature = 1.0) = MyParticleParams(mass, radius, propulsion, rotdiffusion, fluidviscosity, temperature)


# particles
# we can automatically create the kwarg-only constructor using Base.@kwdef
Base.@kwdef mutable struct MyParticle <: AbstractParticle{2}
    # these three fields are mandatory
    id::UInt64
    centerpos::SVector{2, Float64}
    params::MyParticleParams

    velocity::SVector{2, Float64} = SA[0.0, 0.0] # default values for kwarg-constructor
    force::SVector{2, Float64} = SA[0.0, 0.0]
    orientation::Float64 = 0.0

    opacity::Float64 = 0.1
end

# this tells InPartS the maximum range of interactions caused by this particle
InPartS.interactionrange(::Type{MyParticle}, sim::Simulation) = 2maximum(p->p.radius, sim.params)

## 2. Interactions

# Lennard-Jones force shifted and rescaled to get F = 0 at r = 1
ljf(r) = -12*(2^(1/6) * r)^-13 + 6*(2^(1/6) * r)^-7

# particle-particle interaction
function InPartS.externalforces!(p1::MyParticle , p2::MyParticle, s::Simulation)
    # taking a difference vector modulo the `Domain` will automatically take periodic boundaries into account
    δ = (p1.centerpos - p2.centerpos)%s.domain
    nδ2 = dot(δ, δ)
    Θ = p1.params.radius + p2.params.radius
    if nδ2 < Θ*Θ
        nδ = √nδ2
        force = 50*ljf(nδ/Θ) * δ/nδ
        p1.force -= force
        p2.force += force
    end
end

# active propulsion and Stokesian drag
function InPartS.internalforces!(p::MyParticle , ::Simulation)
    activeforce = p.params.propulsion * SVector(reverse(sincos(p.orientation)))
    dragforce = 6π*p.params.fluidviscosity*p.params.radius*p.velocity
    p.force += activeforce - dragforce
end



## 3. Integration

function InPartS.step!(p::MyParticle , dt, sim::Simulation)
    sqrdt = √(dt)
    # rotational diffusion
    p.orientation += p.params.temperature*p.params.rotdiffusion * sqrdt * randn(sim.rng, Float64)

    # forces and velocity diffusion
    D = p.params.temperature/(6π*p.params.fluidviscosity*p.params.radius)
    p.velocity += dt .* (p.force/p.params.mass) +  D*sqrdt*randn(sim.rng, SVector{2, Float64})

    # position change
    p.centerpos += dt .* p.velocity
end


function InPartS.reset!(p::MyParticle, ::Simulation)
    p.force = SA[0.0, 0.0]
end


## 3. Export functions
#=
  One of the key features of InPartS are the semi-automatically generated export functions, which
  can squeeze a struct into a hierarchical dictionary-like structure.
  This is great for portable output files (e.g. using JLD2)

  Export functions for a Type T are generated with the command `@genexport T`.
  We can use the `@export…` macros (see relevant docstrings) to modify the behaviour of these functions.
  Note that InPartS expects `@genexport` and the `@export…` macros to be define in the same scope
  as the type T, which is important when trying to reconstruct the data.
=#

# load JLD2 to provide an export backend
using JLD2

# disable export for the `force` field
@exportoff MyParticle force
# transform the `opacity` field for export (for no particular reason)
@exporttransform MyParticle opacity value data #((1 + value)^2) (sqrt(data) - 1)

# generate export functions
@genexport MyParticleParams
@genexport MyParticle

## 4. Visualisation
#=
  Here, we use GLMakie.jl to create a simple live visualisation for our system.
  The InPartSMakie.jl package provides a mesh-based visualisation pipeline for
  InPartS simulations,
  For our simple circular particles, implementing the mesh generation API may
  seem a little overkill, but for particles with complex shapes (e.g. simulations
   of growing and dividing cells, the original application of InPartS), the
   flexibility outweighs the initial effort.
=#

Pkg.add(url = "https://gitlab.gwdg.de/eDLS/InPartSMakie.jl.git")

using GLMakie, InPartSMakie

function InPartSMakie.to_mesh(p::MyParticle; quality = 10, offset = Vec3f(0), kwargs...)
    nhalf = quality÷2
    nvertices = 2nhalf
    # generate outline points
    rad32 = Float32(p.params.radius)
    realoffset = Point2f(p.centerpos + offset)
    points = [rad32*Point2f(sincospi(i/nhalf + p.orientation)) + realoffset for i ∈ 1:nvertices]

    # manual triangulation is much faster than algorithmic
    faces = Vector{Makie.GLTriangleFace}(undef, nvertices - 2)
    # use quads connecting opposite vertices in parallel stripes
    for i ∈ 1:nhalf-1
        # with two triangles per quad
        quadpattern = SA[i, i+1, nvertices - i, nvertices - i + 1]
        faces[2i - 1] = Makie.GLTriangleFace(@view quadpattern[[1,2,3]])
        faces[2i]     = Makie.GLTriangleFace(@view quadpattern[[3,4,1]])
    end

    return Makie.GeometryBasics.Mesh(points, faces)
end
## 5. Simulation


sim = Simulation(MyParticle,
    domainsize = (50,50),
    boundaries = (PeriodicBoundary, PeriodicBoundary),
    rng = Random.Xoshiro(42)
)

# We now add a grid of particles to the simulation. Instead of doing this by hand, we can also use the `autogrid!` function
params = MyParticleParams(propulsion = 5.0, temperature = 1.0, rotdiffusion = 0.5)
[addparticle!(sim, MyParticle, centerpos = SA[i, j], orientation = 2π*rand(), params = params) for i ∈ 2.0:1.5:48.0, j ∈ 1.0:1.5:49.0]

# plotting the simulation
fig, ax, plt = plot(sim;
    axis = (;aspect = DataAspect()),
    particles = (;
        quality = 8,
        colorfunction = x->mod2pi(x.orientation),
        colorrange = (0,2π),
        colormap = :cyclic_mygbm_30_95_c78_n256_s25
    ),
)
display(fig)

# In order to observe the simulation while it is running, we add a callback for live visualisation.
# This produces a plot of the system every 0.1 simulation time units
# (and unfortunately slows it down a bit)
plotcb = PeriodicCallback(0.1) do sim
    # update simulation observable
    plt[1][] = sim
    yield()
end
# We also add an additional callback for exporting simulation snapshots to JLD2
savecb = InPartS.SaveCallback(JLD2.JLDFile, joinpath(@__DIR__, "demo.jld2"), interval = 1.0)


# We now evolve the system for 40 time units, using a fixed time step of 0.001 time units and the previously defined callbacks
@time evolve!(sim, 100.0, tss = FixedStepper(0.001), callback = plotcb ∘ savecb)
