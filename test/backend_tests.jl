@testitem "Disabled Commmitting in JLD2 backend" begin
    # Define dummy particles and test error messages until the export is properly defined
    using JLD2

    @kwdef struct DummyParams <: AbstractParams
        z::Complex{Float64} = 1.0 + 1.0im
    end

    @kwdef mutable struct DummyParticle <: AbstractParticle{2}
        id::UInt64 = 0
        centerpos::SVector{2, Float64} = SA[0.0, 0.0]
        params::DummyParams = DummyParams(1.0 + 1.0im)
        customfield::@NamedTuple{a::Int, b::Float64} = (a = 1, b = 1.0)
    end

    sim = Simulation(DummyParticle; domainsize=(1,1))
    addparticle!(sim, DummyParticle; params=DummyParams())
    InPartS.flushadditions!(sim)

    fn = mktempdir()*"test.jld2"
    f = InPartS.dfopen(fn, "w")
    try
        # Serializer is not defined yet
        g = InPartS.gcreate(f, "test1")
        @test_throws(
            "no InPartS.ScalarExport() serializer defined",
            InPartS.ExportUtils.serialize!(g, particles(sim); sim)
        )

        @genexport DummyParticle

        g = InPartS.gcreate(f, "test2")
        @test_throws(
            "Attempted to commit DataType",
            InPartS.ExportUtils.serialize!(g, particles(sim); sim)
        )

        InPartS.@exportoff DummyParticle customfield
        @genexport DummyParticle
        InPartS.@exporttransform DummyParams z [real(value), imag(value)] [data[1] + im*data[2]]
        @genexport DummyParams

        # This should work but there is @test_nothrow
        g = InPartS.gcreate(f, "test3")
        InPartS.ExportUtils.serialize!(g, particles(sim); sim)

        # this brings the simulation into an inconsistent state
        # it should trigger an error during serialization as the parameters are not "registered"
        push!(particles(sim), DummyParticle(; params=DummyParams(1.0 + 2.0im)))
        g = InPartS.gcreate(f, "test4")
        @test_throws(
            AssertionError,
            InPartS.ExportUtils.serialize!(g, particles(sim); sim)
        )
    finally
        close(f)
    end
end
