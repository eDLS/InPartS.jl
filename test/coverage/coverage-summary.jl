####
#### Coverage summary, printed as "(percentage) covered".
####
#### The detailed coverage report is deployed to GitLab Pages (see yml file).
####

using Pkg
Pkg.activate(@__DIR__)
Pkg.instantiate()

using Coverage
using LocalCoverage

# change to package directory
cd(joinpath(@__DIR__, "..", ".."))
Pkg.activate(; temp=true)
Pkg.add(PackageSpec(name="TestReports"))
Pkg.develop(path=".")

using TestReports
# Always test with reduced optimizations and not using native processor instructions
# (otherwise, floating point differences across different CPUs have been seen to occur)
# Coverage only for current project (not for all user code) speeds up tests
TestReports.test("InPartS"; julia_args = ["--optimize=1", "-Cgeneric", "--sysimage-native-code=no", "--pkgimages=no", "--code-coverage=@"], logfilename="report.xml")
# also generates coverage/lcov.info (needed to generate cobertura.xml)
cov = generate_coverage("InPartS"; run_test = false)
display(cov)
html_coverage(cov; open = false, dir = joinpath(pwd(), "coverage"))

percentage = round(cov.lines_hit / cov.lines_tracked * 100, digits=2)
println("Code coverage: $(percentage)% of lines covered")

write(
    ENV["COVERAGE_STASHFILE"],
    "Code coverage: $(percentage)% of lines covered"
)
println("Also stashed away the coverage in $(ENV["COVERAGE_STASHFILE"])")