@testitem "Aqua Tests" begin
    using Aqua

    Aqua.test_all(
        InPartS;
        deps_compat=(
            ignore=[
                :LinearAlgebra,
                :Pkg,
                :Printf,
                :Random,
                :Serialization],
            ),
      )
end
