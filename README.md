# InPartS — Interacting particle simulations in Julia

[![Documentation Link](https://img.shields.io/badge/Documentation-dev-blue)](https://inparts.org)
[![Latest Release](https://gitlab.gwdg.de/eDLS/InPartS.jl/-/badges/release.svg)](https://gitlab.gwdg.de/eDLS/InPartS.jl/-/releases)
[![Aqua QA](https://raw.githubusercontent.com/JuliaTesting/Aqua.jl/master/badge.svg)](https://github.com/JuliaTesting/Aqua.jl)
[![Testing](https://gitlab.gwdg.de/eDLS/InPartS.jl/badges/main/pipeline.svg)](https://gitlab.gwdg.de/eDLS/InPartS.jl/pipelines)
[![Coverage](https://gitlab.gwdg.de/eDLS/InPartS.jl/badges/dev/coverage.svg?job=coveragereport)](https://dontknowwheretolinkto.com)

InPartS is a framework for running agent-based simulations of systems of interacting particles with pairwise interactions, developed in and used by the [Emergent Dynamics in Living Systems](https://www.ds.mpg.de/lmp/dyn-sys) group at [MPI-DS](https://www.ds.mpg.de/) in Göttingen.

## Installation

InPartS is a registered Julia package and can be installed using the Julia package manager.

Most functions provided by the package are documented, but the standards of documentation may vary, and some information may be outdated.
Feel free to contact the developers (e.g. by opening an issue on GitLab) to ask questions or raise complaints.



## Abstract structures for model description

In InPartS, particles are represented by `AbstractParticle` structs that contain all dynamic variables and particle-specific parameters.
Static parameters are stored in immutable `AbstractParam` structs that can be shared by multiple particles.
This feature can be used to simulate systems with multiple different families of particles sharing a similar model, but different parameter values, e.g. cells of different radii or growth rates.

The interaction laws and equations of motion for the particles are represented by three core functions which act on the particle structs.
 - `externalforces!`, which, given a pair of particles, computes their pairwise interactions
 - `internalforces!`, which computes forces that only depend on the state of the single particle they act on. Physically, these forces can be internal to the particle, but the function can also be used for global potential forces.
 - `step!`, which applies the discretised equations of motion to a single particle using the precomputed forces.
These functions have to be extended by the user when developing a new model.

The core functions are called by the main simulation logic in two steps:
First, the `…forces!` functions are called for all particles.
Only after all interactions are computed, InPartS proceeds to call the `step!` function sequentially on all particles.
This separation allows for simulations with adaptive time step sizes, using the computed interactions to set a small enough time step to prevent numerical instability.

Within this cycle, InPartS can optionally execute a set of user-defined functions (callbacks), which can be used to inspect, modify, or export the state of the simulation.

## Simulation domains

By default, InPartS simulates particles on rectangular domains, with the origin of the internal coordinate system in the lower left-hand corner of the system.
Optionally, periodic boundaries can be enabled independently for both dimensions and are stored as part of the type signature of the domain struct.

To easily switch between simulating the same model on domains with and without periodic boundary conditions, InPartS provides functions that apply the periodicity to position and difference vectors only in the relevant coordinates, at negligible cost for nonperiodic systems.

Note that the current implementation of periodic boundaries does not natively support multiple interactions between the same pair of particles across periodic boundaries.

## Force computation

InPartS computes forces by calling the `…forces!` functions on all (pairs of) particles sequentially.
Particle structs have to be designed to accumulate the forces computed by these functions themselves.
Optionally, the force computation functions can also be used to extract and save all individual forces and attack points.
This feature can be used to measure stresses and pressures within systems.

When simulating a system of $N$ particles with pairwise interactions, there are $N^2$ possible interacting pairs.
If the interaction is limited to finite spatial range much smaller than the system size, most of the pairs will not interact.
Unfortunately, due to the overhead of computing particle distances, the run time of the simulations will still scale quadratically with the amount of particles.
To prevent this quadratic scaling from making larger simulations unfeasible, InPartS provides the option to bin the particles into boxes with lengths equal to the interaction radius.
Now, to find all possible interaction partners for a particle inside a given box, we can limit our search to other particles in its 8-neighbourhood.

As extracting the interaction radius from the model interaction functions is nontrivial, InPartS requires models to explicitly provide a value of the interaction radius using the `interactionradius`
Note that the box grid is constructed statically at the beginning of the simulation (although it can be reinitialized manually to deal with changing length scales), thus its size should always be an upper bound to the true interaction radius.


## Obstacles

Some simulations require particles to interact with static objects and walls.
While very simple cases could be handled by manually adding additional terms to the `internalforces!` method, this approach is not very flexible and limits the reuseability and composability of the models.

To solve this problem, InPartS offers a separate interface to add large, static objects to simulations.
Internally, these obstacles are in many ways similar to particles.
However, unlike particles, they cannot be added to or removed from a simulation dynamically and cannot change state during a simulation.

## Automated export

To save simulation snapshots, InPartS provides powerful functions for converting its internal representation of simulation states into a hierarchical data layout, which can be easily serialised using common portable data formats
This module uses metaprogramming and code generation to create performant export functions for custom data types at compile time, which can be further customised by the model developer to exclude certain fields from export, or to convert them into a more convenient format.

Together with the callback interface, the export functions can be used to write system snapshots directly to disk during simulations, thus preventing data loss if a simulation terminates unexpectedly.

## High-level API
Aside from a powerful API for model development, InPartS also provides an easy-to-use interface for setting up and running simulations.
This interface is desiged around the `Simulation` container, which manages the particles and obstacles of the simulation.
A simple simulation can be set up in very few lines of code:

```julia
    # load InPartS and the bacteria model definitions
    # (from https://gitlab.gwdg.de/LMP-pub/StressAnisotropyGrowingRods/)
    using InPartS, StressAnisotropyPaperCode
    # load StaticArrays and JLD2 (Julian HDF5-based file format)
    using StaticArrays, JLD2

    # Create a simulation container for rod cells,
    # with an x-periodic 50×50 domain
    sim = Simulation(RodCell;
        domainsize = (50, 50),
        boundaries = (PeriodicBoundary, Boundary),
    )

    # add a rod cell with random orientation and a division length of 3
    # (max. backbone length bᵐᵃˣ = 2) to the system centre
    addparticle!(sim;
        centerpos = SA[25.0, 25.0],
        φ = π*rand(),
        params = RodCellParams(maxlength = 2)
    )

    # set up live visualisation using the matplotlib plotting library
    using PyPlot
    plotcb = PeriodicCallback(0.05, InPartS.callback_plot())

    # set up snapshot export
    savecb = InPartS.SaveCallback(JLD2.JLDFile, joinpath(@__DIR__, "demo.jld2"), interval = 0.05)

    # evolve system for 5 generations using adaptive time steps
    evolve!(sim, 5.0;
        tss = SimpleAdaptiveStepper(0.05, dt = 0.1),
        callback = plotcb ∘ savecb
    )
```
